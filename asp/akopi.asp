<!--#include file="headers.asp"-->
<!--#include file="functions.asp"-->

<% 	'--akopi.asp
	'--Sp�r om dato for visning av journalistkopier. Katalogen vises av katalog.asp.
	
	Dim i, dateStr, yy, mm, dd

''	Set auth = Server.CreateObject("AUTHXOCX.AuthXOCXCtrl.1")
''	Set fso = CreateObject("Scripting.FileSystemObject")
''	gSign = GetSign
''	gAvdeling = Request.QueryString("avd")

	InitGlobals

	If gAvdeling = "" Then
		gAvdeling = GetAvdeling
	End If

	response.expires = 0

%>
<html>
<head>
<title>Journalistkopier</title>
</head>
<body bgcolor="#ffeedd">
	<h2>Journalistkopier <%=gAvdeling%></h2>
	<h4>Velg dag:</h4>
<%
	dateStr = FormatDateTime(Now(), vbShortDate)
%>
	<a href="katalog.asp?dir=<%=gAvdeling%>/kopier/<%=dateStr%>">I dag</a>
<%
	yy = Year(dateStr)
	mm = Month(dateStr)
	dd = Day(dateStr)

	For i = 1 to 5
		dateStr = FormatDateTime(DateSerial(yy, mm, dd - i), vbShortDate)
%>
	<p><a href="katalog.asp?dir=<%=gAvdeling%>/kopier/<%=dateStr%>"><%=i%> dag(er) siden</a>
<%
	Next
%>
</body>
</html>

