<%
Sub GetDBUserHeaders
    Dim objConn, objRec, strSQL

	Set objConn = Server.CreateObject("ADODB.Connection")
   	Set objRec = Server.CreateObject("ADODB.Recordset")

   	objConn.open "DSN=Fretex"

    'Finn hvilken avdeling brukeren tilh�rer
    strSQL  = "SELECT Avdeling FROM Brukere WHERE sign='" & GetSign & "'"
	set objRec = objConn.Execute(strSQL)

	If Not objRec.EOF Then
        strSQL  = "SELECT Gruppe, Undergruppe, Desk, Distkode, Kanal FROM Avdelinger WHERE avdeling='" & objRec("Avdeling") & "'"
        set objRec = objConn.Execute(strSQL)

        SetFieldValue "GR", objRec("Gruppe")
        SetFieldValue "UG", objRec("Undergruppe")
        SetFieldValue "DI", objRec("Distkode")
        SetFieldValue "KA", objRec("Kanal")
    End If

    Set objConn = Nothing
    Set objRec = Nothing
End Sub

Function GetMyDesk
    Dim objConn, objRec, strSQL, signSQL

	Set objConn = Server.CreateObject("ADODB.Connection")
   	Set objRec = Server.CreateObject("ADODB.Recordset")

   	objConn.open "DSN=Fretex"

    'Finn hvilken avdeling brukeren tilh�rer
    strSQL  = "SELECT Avdeling FROM Brukere WHERE sign='" & GetSign & "'"
	set objRec = objConn.Execute(strSQL)

	If Not objRec.EOF Then
        strSQL  = "SELECT Gruppe, Undergruppe, Desk, Distkode FROM Avdelinger WHERE avdeling='" & objRec("Avdeling") & "'"
	    set objRec = objConn.Execute(strSQL)
        GetMyDesk = objRec("Desk")
    End If

    Set objConn = Nothing
    Set objRec = Nothing
    
End Function

Function SetLock(fil)
    Dim objConn, objRec, strSQL, retval

    retval = ""

	Set objConn = Server.CreateObject("ADODB.Connection")
   	Set objRec = Server.CreateObject("ADODB.Recordset")

   	objConn.open "DSN=Fretex"

    If forceOpen Then
        RemoveLock fil
    End If

    'Sjekk om meldingen allerede er l�st
    strSQL= "SELECT Count(Filnavn) AS ANTALL, Filnavn, Sign, Tid FROM Locks GROUP BY Filnavn, Sign, Tid HAVING Filnavn='" & fil & "'"
    set objRec = objConn.Execute(strSQL)     

    If objRec.EOF Then
        Response.Write "<!-- Hei -->"
        'Meldingen er ikke l�st
        strSQL ="INSERT INTO Locks (Filnavn, Sign, Tid) VALUES ('" & fil & "', '" & GetSign & "', '" & Now & "')"
        set objRec = objConn.Execute(strSQL)
    Else
        retval = "Meldingen ble l�st av " & objRec("Sign") & " " & objRec("Tid")
    End If

    Response.Write "<!-- Retval: " & retval & "-->"

    Set objConn = Nothing
    Set objRec = Nothing

    SetLock = retval

End Function

Function RemoveLock(fil)
    Dim objConn, objRec, strSQL

	Set objConn = Server.CreateObject("ADODB.Connection")
   	Set objRec = Server.CreateObject("ADODB.Recordset")

   	objConn.open "DSN=Fretex"

    'Sjekk om meldingen allerede er l�st
    'strSQL  = "SELECT Sign, Tid FROM Locks WHERE Filnavn='" & fil & "'"
	'set objRec = objConn.Execute(strSQL)
    strSQL ="DELETE FROM Locks WHERE Filnavn='" & fil & "'"
    set objRec = objConn.Execute(strSQL)

    Set objConn = Nothing
    Set objRec = Nothing

End Function

%>
