<%
	response.expires = 0
%>
<html>
<head>
    <title>Velg destinasjon</title>

    <script language="javascript">
    <!--
    function SettKatalog(katalog) {
        opener.document.redform.sendtil.value = katalog;
        self.close();
    }

    function SettSign () {
        opener.document.redform.sendtil.value = "brukere/" + document.brukerform.sign.value;
        self.close();
    }
    // -->
    </script>
</head>
<body>
<p><b>Skriv inn en signatur:</b>
<form name="brukerform" action="popup.htm" onSubmit="return SettSign()">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;<input type="textbox" name="sign">
</form>
</p>
<p><b>eller velg fra listen:</b>
<ul>
<li><a href="javascript:SettKatalog('nyhetsdesken')">Nyhetsdesken</a>
<li><a href="javascript:SettKatalog('reportasjedesken')">Reportasjedesken</a>
<li><a href="javascript:SettKatalog('sport/desk')">Sportsdesken</a>
<li><a href="javascript:SettKatalog('direkte/desk')">Direktedesken</a>
<li><a href="javascript:SettKatalog('kultur/desk')">K&U-desken</a>
<li><a href="javascript:SettKatalog('media/desk')">Media-avdelingen</a>
<li><a href="javascript:SettKatalog('feature/desk')">Featuredesken</a>
</ul>
<ul>
<li><a href="javascript:SettKatalog('ut')">Ut til satellitt</a>
<li><a href="javascript:SettKatalog('hold')">Hold (p� vent)</a>
</ul>
<ul>
<li><a href="javascript:SettKatalog('nyhet/logger')">Nyhetsdesk logger</a>
<li><a href="javascript:SettKatalog('reportasje/logger')">Reportasjedesk logger</a>
<li><a href="javascript:SettKatalog('sport/logger')">Sport logger</a>
</ul>
<ul>
<li><a href="javascript:SettKatalog('sport/fakta')">Sport fakta-katalog</a>
</ul>
<ul>
<li><a href="javascript:SettKatalog('direkte/ut')">Direkte-kunder</a> <font color=red>(automatisk publ.)</font>
<li><a href="javascript:SettKatalog('feature/ut')">Feature-web</a>
</ul>
</p>
</body>
</html>

