<%
'--
'--

Sub WriteFormJScript
%>
	<SCRIPT language="javascript">
	<!--
		//-- KALENDER --
		function y2k(number)    { return (number < 1000) ? number + 1900 : number; }
		var today = new Date();
		var day   = today.getDate();
		var month = today.getMonth();
		var year  = y2k(today.getYear());

		function padout(number) { return (number < 10) ? '0' + number : number; }

		function restart() {
		    document.redform.TI.value = '' + padout(day) + '/' + padout(month - 0 + 1) + '/' + year + ' 12:00';
			kalender.close();
		}
		function VisKalender() {
			kalender = window.open('kalender.htm','kalender','resizable=no,width=350,height=270,top=50,left=150');
		}

		// -- ANNET --
		function VisDestListe() {
			window.open("destliste.asp","popup",'toolbar=0,location=0,directories=0,status=0,menubar=0,scrollbars=0,resizable=0,width=300,height=550');
		}
	
		function SettInnByline(byline) {
			var navn, org;
			navn = "<%=GetFullName%>";
			org = "<%=gOrg%>";
			if (byline) {
				document.redform.Byline.value = "Av " + navn + ", " + org;
			}
			else {
				document.redform.Ingress.value = "Oslo (" + org + "-" + navn + "): " + document.redform.Ingress.value;
			}
		}
		
		function TellLinjer() {
			var cnt, str, lin;
			str = document.redform.Ingress.value;
			cnt = str.length;
			str = document.redform.BrodTekst.value;
			cnt = cnt + str.length;
			lin = Math.round(cnt/65) + 1
			str = "Ingress + br�dtekst er p� " + cnt + " tegn.\n"
			str = str + "Dette tilsvarer " + lin + " linjer � 65 anslag."
			alert(str)
		}
		
		function SjekkFelt() {
			var str;	
			//Sjekker stikkordet
			str = document.redform.ST.value;
			if (str.length == 0) {
				alert("Meldingen m� ha et stikkord");
				return false;
			}
			else if (str.length < 3) {
				alert("Stikkordet er for kort");
				return false;
			}
			//Sjekker infolinje
			str = document.redform.Tittel.value;
			if (str.length == 0) {
				alert("Meldingen m� ha infolinje");
				return false;
			}
			//Sjekker undergruppe
			str = document.redform.UG.value;
			if (str.length != 3) {
				alert("Undergruppen m� v�re p� tre tegn");
				return false;
			}
			//Sjekker prioritet
			str = document.redform.PR.value;
			if ((str.length != 1) || (str <= "0") || (str >= "9")) {
				alert("Prioritet m� v�re et tall mellom 1 og 8");
				return false;
			}
			//Sjekker distkode
			str = document.redform.DI.value;
			if (str.length != 3) {
				alert("Dist-koden m� v�re p� tre tegn");
				return false;
			}
			//Sjekker tittel
			//str = document.redform.Tittel.value;
			//if (str.length == 0) {
			//	alert("Meldingen m� ha en infolinje");
			//	return false;
			//}
			return true;
		}
	// -->
	</SCRIPT>
<%
End Sub

%>
