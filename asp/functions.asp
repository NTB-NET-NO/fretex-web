<%	
'---------------------------------------------------------------------
'functions.asp
'Diverse funksjoner.
'---------------------------------------------------------------------

'---------------------------------------------------------------------
'-- Sub InitGlobals
'-- Initierer globale variabler
'-- Inn: n/a
'-- Ut:	 n/a
'---------------------------------------------------------------------
Sub InitGlobals()
	''Set auth = Server.CreateObject("AUTHXOCX.AuthXOCXCtrl.1")
	Set fso = CreateObject("Scripting.FileSystemObject")

	gSign = GetSign()
	'Set objUser = GetObject("WinNT://" & gDomain & "/" & gSign)
	Set objUser = GetObject("WinNT://portal/rov")
	gAvdeling = GetAvdeling()
End Sub

'---------------------------------------------------------------------
'-- Function GetFieldValue
'-- Returnerer verdien p� et felt i formen.
'-- Inn:	Feltnavn
'-- Ut:	Verdi
'---------------------------------------------------------------------
Function GetFieldValue(fldName)

	Dim i, fld
       
	fldName = LCase(fldName)
	i = 0
	For Each fld in gFelt
	   	If LCase(fld) = fldName Then
       			GetFieldValue = gVerdi(i)
			Exit For
		End If
		
		i = i + 1        
	Next

End Function


'---------------------------------------------------------------------
'-- Sub SetFieldValue
'-- Setter verdien p� et felt fra formen.
'-- Inn:	Feltnavn, Verdi
'-- Ut:	n/a
'---------------------------------------------------------------------
Sub SetFieldValue(fldName, value)

	Dim i, fld

	fldName = LCase(fldName)
	i = 0
	For Each fld in gFelt
	
		If LCase(fld) = fldName Then
			gVerdi(i) = value
			Exit For
		End If

		i = i + 1        
	Next

End Sub

'---------------------------------------------------------------------
'-- Sub ParseFile
'-- Leser en HTML-fil og setter de forskjellige feltverdiene.
'-- Inn:	Filnavn (full path i filsystemet)
'-- Ut:	n/a
'---------------------------------------------------------------------
Sub ParseFile (fileName)

	Dim myText, fld, myVal, strBef, strAft, myStart, myEnd

	Set fs = fso.OpenTextFile(fileName, ForReading)
	myText = fs.ReadAll

	For Each fld in gFelt

		myVal = ""
		strBef = "<!--pre-" & fld & "-->"
		strAft = "<!--post-" & fld & "-->"

		myStart = InStr(1, myText, strBef)
		myEnd = InStr(1, myText, strAft)

		If (myStart > 0 And myEnd > 0) Then
			myVal = Mid(MyText, myStart + Len(strBef), myEnd - myStart - Len(StrBef))
		End If

		SetFieldValue fld, myVal
		''response.write "##" & fld & "##" & myval & "##<br>"

	Next
	fs.Close
End Sub

'---------------------------------------------------------------------
'-- Sub GetPostVars
'-- Leser variable som sende med POST og setter de tilsvarende feltverdiene.
'-- Inn:	n/a
'-- Ut:	n/a
'---------------------------------------------------------------------
Sub GetPostVars

	Dim fld, myVal

	'Sett alle headere (samme navn i form som i arrayet gFelt)
	For Each fld in gFelt
		SetFieldValue fld, Request.Form(fld)
	Next

	myVal = Request.Form("Go")
	
	Select Case myVal
	Case "Lagre"
		gAction = "save"
		gDestDir = Request.Form("sendtil")
	Case "Lagre og lukk"
		gAction = "saveclose"
	''Case "Send til desk"
	''	gAction = "send"
	''	gDestDir = GetMyDesk
	''Case "Send ut"
	''	gAction = "send"
	''	gDestDir = "ut/" & gAvdeling
	Case "Send til:", "Send"
		gAction = "send"
		gDestDir = Request.Form("sendtil")
	Case "Slett"
		gAction = "delete"
	Case "Ny versjon"
		gAction = "nyver"
	Case "Rediger"
		gAction = "edit"
	Case "Rediger likevel"
		gAction = "edit"
		gForceOpen = True
	Case "Kopier melding"
		gAction = "copy"
	End Select

	gFileName = Request.Form("filename")
	gSrvFile = Server.MapPath(AddBaseDir(gFileName))

End Sub

'---------------------------------------------------------------------
'-- Sub LagNyVersjon
'-- Endrer stikkord og til red. ved ny versjon
'-- Inn:	n/a
'-- Ut:	n/a
'---------------------------------------------------------------------
Sub LagNyVersjon

	Dim myST, newST, newRed, myNum, i, lpart
	
	'Endre stikkord
	myST = GetFieldValue("ST")

	i = InStr(1, myST, "-")
	If i > 0 Then
		lpart = LCase(Left(myST, i-1))
		
		If lpart = "il" Then
			newST = "HAST-" & Right(myST, Len(myST) - 3)
			SetFieldValue "PR", "3"

		ElseIf lpart = "hast" Then
			newST = Right(myST, Len(myST) - 5)
			SetFieldValue "PR", "5"

		ElseIf Left(lpart, 2) = "ny" Then
			mynum = Mid(lpart, 3, i-3)
			If IsNumeric(mynum) Then
				mynum = mynum + 1
				newST = "ny" & mynum & Right(myST, Len(myST) - i + 1)
			Else
				newST = myST
			End If

			'Fjerne infolinje for meldinger med INN og UTE
			mldgrp = GetFieldValue("GR")
			If mldgrp = "INN" Or mldgrp = "UTE" Then
				SetFieldValue "Tittel", ""
			End If

		Else
			newST = "ny1-" & myST
			'Fjerne infolinje for meldinger med INN og UTE
			mldgrp = GetFieldValue("GR")
			If mldgrp = "INN" Or mldgrp = "UTE" Then
				SetFieldValue "Tittel", ""
			End If
		End If

	Else
		newST = "ny1-" & myST
	End If
		
	SetFieldValue "ST", newST

	'---newRed = "(Til red: Oppdatert versjon.)"
	'---SetFieldValue "TilRed", newRed

End Sub

'---------------------------------------------------------------------
'-- Sub AddBaseDir
'-- Legger til full path p� en relativ URL (fra server_root)
'-- Inn:	Filnavn
'-- Ut:	Modifisert Filnavn
'---------------------------------------------------------------------
Function AddBaseDir (fileName)
	Dim theDir

	If LCase(Left(filename, Len(gBaseDir))) <> LCase(gBaseDir) Then
		If Left(fileName, 1) = "/" Then
			theDir = gBaseDir & fileName
		Else
			theDir = gBaseDir & "/" & fileName
		End If
		'''AddBaseDir = fso.BuildPath(gBaseDir, fileName)
	Else
		theDir = fileName
	End If
	
	AddBaseDir = Replace(theDir, "//", "/")

End Function

'---------------------------------------------------------------------
'-- Sub RemoveBaseDir
'-- Returnerer et brukervennlig fil/katalognavn.
'-- Inn:	Filnavn
'-- Ut:	Modifisert Filnavn
'---------------------------------------------------------------------
Function RemoveBaseDir (fileName)

	Dim spath

	i = InStr(1, fileName, gBaseDir)
	If i > 0 Then
		RemoveBaseDir = Right(fileName, Len(fileName) - Len(gBaseDir) - i)
		Exit Function
	End If

	spath = Server.MapPath(gBaseDir)
	i = InStr(1, fileName, spath)
	If i > 0 Then
		RemoveBaseDir = Right(fileName, Len(fileName) - Len(spath) - i)
	End If

End Function


'---------------------------------------------------------------------
'-- Function GetSign
'-- Returnerer hvilken bruker som er innlogget.
'-- Inn:	n/a
'-- Ut:	Signaturen
'---------------------------------------------------------------------
Function GetSign
	Dim sign
	Dim currentUser, currentUserText
	Dim protectedDomain, protectedAbsPath, cookieName

	'Hent variabelen som inneholder innlogget bruker
	sign = Request.ServerVariables("LOGON_USER")
	
	'Stripp evt. domenenavn
	GetSign = Right(sign, Len(sign) - InStr(1, sign, "\"))

	'currentUserText = Request.Cookies("AXCOOKIELOGIN")
	'If currentUserText <> "" Then
	'	currentUser = Right(currentUserText, Len(currentUserText) -1)
	'	currentUser = Left(currentUser, Instr(currentUser, ">") - 1)
	'End If

	'GetSign = currentUser
	'Exit Function
End Function

'---------------------------------------------------------------------
'-- Function GetAvdeling
'-- Returnerer avdeling for innlogget bruker. 
'-- Sjekker mot WinNTs brukerdatabase vha. ADSI
'-- Inn:	n/a
'-- Ut:	Avdeling
'---------------------------------------------------------------------
Function GetAvdeling
	Dim objGruppe		'As Object
	Dim strAvdPrefiks	'As String
	Dim varAvdelinger	'As Array
	Dim strAvdeling		'As String
	Dim strGruppe		'As String

	'Sjekk selskap
	If InGroup("Org_Pluss") Then
		gOrg = "NTB Pluss"
		strAvdPrefiks = "Pluss"
		varAvdelinger = Array("Admin", "IT", "Produksjon", "Redaksjon")
	ElseIf InGroup("Org_NTB") Then
		gOrg = "NTB"
		strAvdPrefiks = "NTB"
		varAvdelinger = Array("Admin", "Andre", "DirekteRed", "Ekspedisjon", _
			"Innenriks", "IT", "Marked", "Regnskap", _
			"Sentralbord", "Sport", "Tekstarkiv", "Utenriks")
	End If

	strAvdPrefiks = LCase(strAvdPrefiks)

	'Sjekk brukerens grupper
	For Each objGruppe In objUser.Groups
		strGruppe = LCase(objGruppe.Name)
		'Sjekk om dette er en avdelingsgruppe
		If Left(strGruppe, Len(strAvdPrefiks)+5) = "avd_" & strAvdPrefiks & "_" Then
			'Sjekk etter en gyldig avdelingsgruppe
			For Each strAvdeling In varAvdelinger
				If InStr(1, strGruppe, LCase(strAvdeling)) Then
					GetAvdeling = strAvdeling
					Exit For
				End If
			Next
		End If
	Next
End Function

'===============================================================
' Function InGroup
' Sjekker om brukeren er med i angitt gruppe.
' Parametere: gruppenavn
' Returnerer: TRUE/FALSE
'===============================================================
Function InGroup(strGroupName)

'start debug
if strGroupName = "Org_NTB" then
	InGroup = True
else
	InGroup = False
end if
'stlutt debug

exit function

	Dim objGrp

	Set objGrp = GetObject("WinNT://" & gDomain & "/" & strGroupName)
	If (Not objGrp Is Nothing) And (objGrp.IsMember(objUser.ADsPath)) Then
		InGroup = True
	Else
		InGroup = False
	End If
End Function

'---------------------------------------------------------------------
'-- Function GetFullName
'-- Returnerer fullt navn for innlogget bruker
'-- Sjekker mot WinNTs brukerdatabase vha. ADSI
'-- Inn:	n/a
'-- Ut:	navn
'---------------------------------------------------------------------
''Function GetFullName
'
'	GetFullName = auth.UserNotes(gSign)
'
'End Function


'---------------------------------------------------------------------
'-- Function FinnDefaultDestinasjon
'-- Finner default destinasjon p� nye meldinger.
'-- Sjekker mot Autehntix
'-- Inn:	n/a
'-- Ut:		evt. destinasjon
'---------------------------------------------------------------------
''Function FinnDefaultDestinasjon
''	Dim entry, i, member
''
''	For Each entry in gDestList
''		'Sjekk om brukeren er medlem (O = medlem!!)
''		member = auth.GroupHasUser(entry, gSign)
''		If member = 0 Then
''			i = InStr(1, entry, "_")
''			If i > 0 Then
''				FinnDefaultDestinasjon = Right(entry, Len(entry) - i)
''				Exit For
''			End If
''		End If
''	Next
''End Function

'---------------------------------------------------------------------
'-- Function GruppeTilAvdeling
'-- Returnerer avdeling ut fra gruppe-feltet.
'-- Brukes til kopi n�r meldinger sendes ut.
'-- Inn:	n/a
'-- Ut:	avdeling
'---------------------------------------------------------------------
Function GruppeTilAvdeling
	Dim grp	

	grp = GetFieldValue("GR")

	Select Case UCase(grp)
	Case "INN"
		GruppeTilAvdeling = "Innenriks"
	Case "UTE"
		GruppeTilAvdeling = "Utenriks"
	Case "SPO"
		GruppeTilAvdeling = "Sport"
	Case "ART"
		GruppeTilAvdeling = "Kultur"
	Case "PRS"
		GruppeTilAvdeling = "Personalia"
	Case "FEA"
		GruppeTilAvdeling = "Feature"
	Case "PRM", "RTV"
		GruppeTilAvdeling = "Media"
	Case "VLG"
		GruppeTilAvdeling = "Valg"
	Case Else
		GruppeTilAvdeling = gAvdeling
	End Select

End Function



'---------------------------------------------------------------------
'-- Function GetUniqueName
'-- Returnerer et unikt filnavn. Brukes n�r nye filer opprettes.
'-- Filnavn er p� formen YYYY-MM-DD-HH-MM-U.htm, der U er et unikt l�penummer.
'-- Inn:	n/a
'-- Ut:	Filnavn (uten path)
'---------------------------------------------------------------------
Function GetUniqueName

	Dim tid, myName, myStr

	tid = Now()
	myName = Year(tid) & "-"

	myStr = Month(tid)
	If myStr < 10 Then
		myName = myName & "0"
	End If
	myName = myName & myStr & "-"

	myStr = Day(tid)
	If myStr < 10 Then
   		myName = myName & "0"
	End If
	myName = myName & myStr & "-"

	myStr = Hour(tid)
	If myStr < 10 Then
   		myName = myName & "0"
	End If
	myName = myName & myStr & "-"
	
	myStr = Minute(tid)
	If myStr < 10 Then
		myName = myName & "0"
	End If
	myName = myName & myStr
  
	'Henter et unikt l�penummer	
	Application.Lock
	Application("Count") = Application("Count") + 1
	If Application("Count") > 9999 Then
		Application("Count") = 1
	End If
	myName = myName & "-" & Application("Count") & ".htm"
	Application.Unlock
	
	GetUniqueName = myName

End Function


'---------------------------------------------------------------------
'-- Sub RedigerFil
'-- Kalles n�r en HTML-fil skal redigeres
'-- Inn:	n/a
'-- Ut:	n/a
'---------------------------------------------------------------------
Sub RedigerFil

	'Sjekk om noe filnavn er angitt
	If gFileName <> "" Then

		'Setter global variabel gSrvFile til full path
		'gSrvFile = Server.MapPath(AddBaseDir(gFileName))

		If Not fso.FileExists(gSrvFile) Then
			gMsg = "Filen finnes ikke (kan den v�re sendt fra katalogen?).<BR>Oppdater evt. visningen og pr�v igjen."
			gDoWrite = False
			Exit Sub
		End If

		'Setter global melding (gMsg) hvis ikke tilgang
		gMsg = CheckEditPermission(gSrvFile)
		
		If gMsg <> "" Then
			'Ikke tilgang til filen
			gDoWrite = False
			gNoPerm = True

		Else
			'Setter global melding (gMsg) hvis meldingen er l�st
			gMsg = SetLock(gSrvFile)

			If gMsg <> "" Then
				gDoWrite = False
				gIsLocked = True
			Else
				'Hvis ikke l�st skal filen leses og form skrives
				ParseFile(gSrvFile)
				
				'Setter default destinasjon
				If LCase(gAvdeling) = "direkte" Then
					gSendTil = "direkte/ut"
				End If

			End If 'melding l�st
			
		End If  'tilgang til filen

	'Filnavn er ikke angitt
	Else
		
		gMsg = "Finner ikke filen.."
		gDoWrite = False
		
	End If
	
End Sub


'---------------------------------------------------------------------
'-- Function CheckEditPermission
'-- Sjekker om en bruker har rettigheter til � redigere en fil.
'-- Inn:	Filnavn (full path i filsystemet)
'-- Ut:	Evt. feilmelding
'---------------------------------------------------------------------
Function CheckEditPermission(eFile)

	Dim grp, usrStr, dirType, dirName, dirParts, perm
	
	perm = False
	
	'Del opp full path i deler
	dirParts = Split(eFile, "\")

	'Finn type katalog (tredje siste del)
	dirType = LCase(dirParts(UBound(dirParts) - 2))
	
	'Sjekk om det er en brukerkatalog. Kun brukeren selv har tilgang til denne.
	If dirType = "brukere" Then
		'Finn katalognavn (nest siste del)
		dirName = dirParts(UBound(dirParts) - 1)

		If LCase(dirName) = LCase(gSign) Then
			perm = True
		End If
	
	'Kan ikke redigere kopier
	ElseIf dirType = "kopier" Then
		perm = False
		
	'Sjekker om dette er en avdelingskatalog. Brukeren m� da v�re medlem av avdelingen.
	ElseIf InArray(gAvdList, dirType) Then
		If LCase(gAvdeling) = dirType Then
			perm = True
		End If
	
	'Default er tilgang
	Else
		perm = True
	End If
	
	If Not perm Then
		CheckEditPermission = "Du har ikke redigeringsadgang til denne katalogen"
	End If
	
End Function


'---------------------------------------------------------------------
'-- Function IsDeskUser
'-- Sjekker om en bruker er desk-bruker p� en gitt avdeling.
'-- Inn:	Navn p� avdeling
'-- Ut:	True/false
'---------------------------------------------------------------------
Function IsDeskUser(desk)

	Dim grpStr, usrStr, retVal, grp
	
	retVal = False
	
	'Gruppe som skal sjekkes
	grpStr = "WinNT://" & gDomain & "/Desk_" & desk & ",group"
	usrStr = "WinNT://" & gDomain & "/" & gSign
	
	For Each entry in gAvdList
		'Hent aktuell gruppe
		On Error Resume Next	
		Set grp = GetObject(grpStr)

		If Err = 0 Then
			'Sjekk om brukeren er medlem av gruppen
			If (grp.IsMember(usrStr)) Then
				retVal = True
				Exit For
			End If
		End If
	Next
	
	IsDeskUser = retVal

End Function

'---------------------------------------------------------------------
'-- Function GetMyDesk
'-- Returnerer denne brukerens deskkatalog.
'-- Inn:	n/a
'-- Ut:	Desk-katalog
'---------------------------------------------------------------------
Function GetMyDesk

	Dim myAvd
	
	myAvd = gAvdeling
	
	If myAvd <> "" Then
		GetMyDesk =  myAvd & "/desk"
	Else
		GetMyDesk =  ""
	End If

End Function

'---------------------------------------------------------------------
'-- Function SetLock
'-- L�ser en melding ved � opprette en l�se-fil i samme katalog.
'-- L�sefilen har samme navn, men avsluttes med '.lck'.
'-- Inn:	Filnavn (full path i filsystemet)
'-- Ut:	Evt. melding som forteller om meldingen allerede er l�st
'---------------------------------------------------------------------
Function SetLock(lFile)

	Dim lockFile, msg
	
	lockFile = lFile & ".lck"
	
	'Sjekk om meldingen allerede er l�st (og om vi skal overstyre)
	If (fso.FileExists(lockFile)) And Not (gForceOpen) Then
	
		Set fs = fso.OpenTextFile(lockFile, ForReading)
		
		msg = "Meldingen ble l�st av "
		msg = msg & fs.ReadLine & " "				'signatur
		msg = msg & fs.ReadLine & " "				'tid
		'''msg = msg & "fra maskin " & fs.ReadLine	'ip-adresse

		SetLock = msg
		
	'Meldingen var ikke l�st
	Else
	
		Set fs = fso.CreateTextFile(lockFile, ForWriting)
		
		fs.WriteLine gSign		'sign
		fs.WriteLine Now()		'tid
		fs.WriteLine Request.ServerVariables("REMOTE_ADDR")		'ip-adresse

		fs.Close
	End If

End Function


'---------------------------------------------------------------------
'-- Function RemoveLock
'-- Fjerner l�s p� en melding.
'-- Inn:	Filnavn (full path i filsystemet)
'-- Ut:	Evt. melding om overstyrt l�sing
'---------------------------------------------------------------------
Function RemoveLock(lFile)

	Dim lockFile, msg

	lockFile = lFile & ".lck"
	
	If fso.FileExists(lockFile) Then
		'Sjekk om andre har overstyrt
		Set fs = fso.OpenTextFile(lockFile, ForReading)
		
		msg = fs.ReadLine 'signatur
		If msg = gSign Then
			msg = ""
			fs.Close
			On Error Resume Next
			fso.DeleteFile lockFile
		Else
			msg = "Meldingen ble l�st av " & msg & " "
			msg = msg & fs.ReadLine & " "				'tid
			msg = msg & "fra maskin " & fs.ReadLine	'ip-adresse
			fs.Close
		End If
	End If

	RemoveLock = msg
	
End Function


'---------------------------------------------------------------------
'-- Function DeleteFile
'-- Fjerner en fil (legger den i #delme). Sjekker f�rst om den er l�st.
'-- Inn:	Filnavn (full path i filsystemet)
'-- Ut:	True/False
'---------------------------------------------------------------------
Function DeleteFile(dFile)

	Dim delme, bName, retVal
	
	delme = Server.MapPath(AddBaseDir("#delme/") & FormatDateTime(Now(), vbShortDate))
	If Not fso.FolderExists(delme) Then
		fso.CreateFolder(delme)
	End If

	retVal = RemoveLock(dFile)
	If retVal = "" Then
		bName = fso.GetBaseName(dFile)
		retVal = CopyFile(dFile, delme, bName)
	
		On Error Resume Next
		fso.DeleteFile dFile
		
		DeleteFile = True
		
	Else
		DeleteFile = False
	End If

End Function

'---------------------------------------------------------------------
'-- Function CopyFile
'-- Lager en kopi av en fil
'-- Inn:	Original fil (full path), dest. katalog (full path), nytt navn
'-- Ut:	Full path til ny fil
'---------------------------------------------------------------------
Function CopyFile(fullName, destDir, chgName)
	Dim cpName, orgName

	If chgName = "" Then
		cpName = GetUniqueName
	Else
		cpName = chgName
	End If

	''response.write "##" & fullname & "##" & mydest & "##" & cpname
    
	fso.CopyFile fullName, destDir & "\" & cpName

	CopyFile = destDir & "\" & cpName
	
End Function


'---------------------------------------------------------------------
'-- Sub RemoveFile
'-- Sletter en fil fra filsystemet
'-- Inn:	filnavn (full path)
'-- Ut:	n/a
'---------------------------------------------------------------------
Sub RemoveFile (delFile)

	On Error Resume Next
	fso.DeleteFile delFile

End Sub


'---------------------------------------------------------------------
'-- Function InArray
'-- Sjekker om et array (en liste) inneholder et gitt element.
'-- Inn:	Array, element
'-- Ut:	True/False
'---------------------------------------------------------------------
Function InArray (myArray, myElement)

	Dim elm, retval
	
	retval = False
	
	For Each elm In myArray
		If LCase(elm) = LCase(myElement) Then
			retval = True
			Exit For
		End If
	Next

	InArray = retval

End Function

'---------------------------------------------------------------------
'-- Function FinnUkedag
'-- Finner ukedag (f.eks. onsdag) fra dagnummer.
'-- Inn:	nummer
'-- Ut:		ukedag
'---------------------------------------------------------------------
Function FinnUkedag(dagnr)
	Dim retval

	Select Case dagnr
	Case 1 retval = "Mandag"
	Case 2 retval = "Tirsdag"
	Case 3 retval = "Onsdag"
	Case 4 retval = "Torsdag"
	Case 5 retval = "Fredag"
	Case 6 retval = "L�rdag"
	Case 7 retval = "S�ndag"
	End Select

	FinnUkedag = retval

End Function

'---------------------------------------------------------------------
'-- Function FinnMaaned
'-- Finner m�nedsnavn
'-- Inn:	nummer
'-- Ut:		m�ned
'---------------------------------------------------------------------
Function FinnMaaned(maanednr)
	Dim retval

	Select Case maanednr
	Case 1 retval = "Januar"
	Case 2 retval = "Februar"
	Case 3 retval = "Mars"
	Case 4 retval = "April"
	Case 5 retval = "Mai"
	Case 6 retval = "Juni"
	Case 7 retval = "Juli"
	Case 8 retval = "August"
	Case 9 retval = "September"
	Case 10 retval = "Oktober"
	Case 11 retval = "November"
	Case 12 retval = "Desember"
	End Select

	FinnMaaned = retval

End Function

'---------------------------------------------------------------------
'-- Function FinnKortAvdeling
'-- Finner kort avdelingsnavn (iriks osv.)
'-- Inn:	avdeling
'-- Ut:		kort navn
'---------------------------------------------------------------------
Function FinnKortAvdeling(avdeling)
	Dim retval

	Select Case LCase(avdeling)
	Case "innenriks" retval = "iriks"
	Case "utenriks" retval = "uriks"
	Case Else retval = LCase(avdeling)
	End Select

	FinnKortAvdeling = retval

End Function


'---------------------------------------------------------------------
'-- Function Upper1
'-- Setter f�rste bokstav i strengen til uppercase, resten lower
'-- Inn:	streng
'-- Ut:		streng
'---------------------------------------------------------------------
Function Upper1(str)

	If Len(str) = 0 Then
		Upper1 = ""
	Else
		Upper1 = UCase(Left(str, 1)) & LCase(Right(str, Len(str)-1))
	End If
	
End Function

%>
