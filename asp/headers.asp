<%  
'---------------------------------------------------------------------
'headers.asp
'Globale innstillinger samt lister med gyldige feltverdier.
'---------------------------------------------------------------------

Const ForReading = 1, ForWriting = 2, ForAppending = 8

Dim gKategorier, gKanal, gFelt, gVerdi() 
Dim gIptcKat, gIptcKat2, gIptcKat3, gIptcUKat, gIptcEvloc
Dim gBaseDir, gAspDir, gDomain
Dim objUser
Dim gSign, gAvdeling, gOrg
Dim fso, fs, auth

'--Katalog hele greia ligger i (for mapping til filsystemnavn)
gBaseDir = "/fretex/data"
gAspDir = "/fretex/asp"

'--Domene for sjekk av grupper (ADSI)
'gDomain = "NTB"
gDomain = "portal"

'--Liste over definerte avdelinger (sjekkes mot NT-grupper)
'''gAvdList = Array("Avd_NTB_Innenriks", "Avd_NTB_Utenriks", "Avd_NTB_Sport", "Avd_Pluss_Redaksjon", "Avd_Pluss_Produksjon")
gAvdList = Array("NTB_Innenriks", "NTB_Utenriks", "NTB_Sport", "NTB_DirekteRed", "NTB_Tekstarkiv", "NTB_IT", "Pluss_Kultur", "Pluss_Personalia", "Pluss_Media", "Pluss_Feature", "Scanpix")
gDestList = Array("Dest_Nyhetsdesken", "Dest_Reportasjedesken")

'--Gyldige verdier i headerfeltene
gKategorier = Array("INN", "UTE", "SPO", "PRI", "INT", "ART", "PRM", "RTV", "NRK", "FEA", "�KO")
gKanal = Array("A", "A,N", "C", "C,N", "N")

'--Nye headerfelt 17.09.2002 RoV
gIptcKat = Array("", "Arbeidsliv", "Fritid", "Krig og konflikter", "Kriminalitet og rettsvesen", "Kultur og underholdning", "Kuriosa og kjendiser", "Medisin og helse", "Natur og milj�", "Politikk", "Religion og livssyn", "Sosiale forhold", "Sport", "Ulykker og naturkatastrofer", "Utdanning", "Vitenskap og teknologi", "V�r", "�konomi og n�ringsliv")
gIptcKat2 = Array("", "ARB", "FRI", "KRI", "KRE", "KUL", "KUR", "MED", "NAT", "POL", "REL", "SOS", "SPO", "ULY", "UTD", "VIT", "VAR", "OKO")
gIptcKat3 = Array("", "110000", "12000", "Krig og konflikter", "Kriminalitet og rettsvesen", "Kultur og underholdning", "Kuriosa og kjendiser", "Medisin og helse", "Natur og milj�", "Politikk", "Religion og livssyn", "Sosiale forhold", "Sport", "Ulykker og naturkatastrofer", "Utdanning", "Vitenskap og teknologi", "V�r", "�konomi og n�ringsliv")

gIptcUKat = Array("", "INN", "UTE", "SPO", "PRI", "INT", "ART", "PRM", "RTV", "NRK", "FEA", "�KO")

gIptcEvloc = Array("", "Afrika", "Asia", "Australia og Oceania", "Europa", "Global", "Latin-Amerika og Karibia", "Midt�sten", "Norden", "Norge", "Russland og det tidligere Sovjet", "USA og Canada")
'--Slutt nye headerfelt

gFelt = Array("GR", "UG", "PR", "KA", "DI", "ST", "SI", "TI", "TilDesk", "TilRed", "Ticker", "Byline", "Tittel", "Ingress", "BrodTekst", "Bilder", "Sign")

'Setter antall verdier lik antall headerfelt
ReDim Preserve gVerdi(UBound(gFelt))


'---------------------------------------------------------------------
'--Sub SetDefaultHeaders--
'--Setter defaultverdier for ny melding.
'--Sjekker ogs� om brukeren har innstillinger i databasen.
'---------------------------------------------------------------------
Sub SetDefaultHeaders

	Dim mystr

	'Default hvis ikke annet angis
	SetFieldValue "PR", "5"
	SetFieldValue "DI", "ALL" 
	SetFieldValue "KA", "A"
	SetFieldValue "UG", "NNN"

	'Setter headerfelt basert p� avdeling
	Select Case LCase(gAvdeling)

	Case "innenriks", "direkte"
		SetFieldValue "GR", "INN"

	Case "utenriks"
		SetFieldValue "GR", "UTE"
		
	Case "sport"
		SetFieldValue "GR", "SPO"
		
	Case "kultur"
		SetFieldValue "GR", "ART"
		SetFieldValue "UG", "KUL"
		SetFieldValue "KA", "C"
		SetFieldValue "DI", "KUL"   
		
	Case "personalia"
		SetFieldValue "GR", "ART"
		SetFieldValue "UG", "PRS"
		SetFieldValue "KA", "C"
		SetFieldValue "DI", "PRS"   

	Case "media"
		SetFieldValue "GR", "PRM"
		SetFieldValue "KA", "C"
		SetFieldValue "DI", "PRM"

	Case "feature"
		SetFieldValue "GR", "FEA"
		SetFieldValue "KA", "C"
		SetFieldValue "DI", "INT"
			
	End Select
	
	'--Spesielle verdier ved forskjellige meldingstyper
	Select Case gMldType

	Case "il"
		SetFieldValue "PR", "1"
		SetFieldValue "ST", "IL-"
		SetFieldValue "Tittel", "NTB-IL: "
		SetFieldValue "Ticker", "ja"
		gSendTil = "ut"
	
	Case "hast", "fm"
		SetFieldValue "PR", "3"
		SetFieldValue "ST", "HAST-"
		SetFieldValue "Ticker", "ja"
			
	Case "priv"
		SetFieldValue "GR", "PRI"
		
	Case "logg", "nyhetslogg", "reportasjelogg"
		SetFieldValue "DI", "INT"
		SetFieldValue "KA", "C"
		SetFieldValue "GR", "INT"
		gSendTil = gAvdeling & "/logger"
		datstr = DatePart("d", Now()) & ". " & LCase(FinnMaaned(DatePart("m", Now()))) & " kl. " & FormatDateTime(Now(), vbShortTime)
		If gMldType = "nyhetslogg" Then
			SetFieldValue "Tittel", "Logg nyhetsdesken " & LCase(FinnUkedag(DatePart("w", Now(), vbMonday, vbFirstFourDays))) & " " & datstr
			SetFieldValue "ST", "Nyhetslogg " & datstr
			gSendTil = "nyhet/logger"
		ElseIf gMldType = "reportasjelogg" Then
			SetFieldValue "Tittel", Upper1(gAvdeling) & "logg reportasjedesken " & LCase(FinnUkedag(DatePart("w", Now(), vbMonday, vbFirstFourDays))) & " " & datstr
			SetFieldValue "ST", Upper1(gAvdeling) & "logg repdesk " & datstr
			gSendTil = "reportasje/logger"
		End If

	Case "meny"
		SetFieldValue "GR", "PRI"
		tomorrow = DateSerial(Year(Now()), Month(Now()), Day(Now()) + 1)
		datstr = DatePart("d", tomorrow) & ". " & LCase(FinnMaaned(DatePart("m", tomorrow)))
		SetFieldValue "ST", FinnKortAvdeling(gAvdeling) & "-" & LCase(FinnUkedag(DatePart("w", tomorrow, vbMonday, vbFirstFourDays)))
		SetFieldValue "Tittel", Upper1(gAvdeling)  & " meny " & LCase(FinnUkedag(DatePart("w", tomorrow, vbMonday, vbFirstFourDays))) & " " & datstr
		SetFieldValue "Ingress", "NTB " & gAvdeling & " har f�lgende saker p� programmet i dag, " & datstr & "."
		If LCase(gAvdeling) = "innenriks" Then
			SetFieldValue "TilRed", "Reportasjeleder tlf 22 03 45 53" & vbCrLf & "E-post: innenriks@ntb.no"
		ElseIf LCase(gAvdeling) = "utenriks" Then
			SetFieldValue "TilRed", "Reportasjeleder tlf 22 03 45 54" & vbCrLf & "E-post: utenriks@ntb.no"		
		End If


	Case "prm"
		SetFieldValue "DI", "PRM"
		SetFieldValue "KA", "C"
		SetFieldValue "GR", "PRM"
		SetFieldValue "ST", "PRM: kunde/tittel"
		SetFieldValue "Tittel", "PRM: kunde/tittel"
		SetFieldValue "TilRed", "***** NTB PLUSS' PRESSEMELDINGSTJENESTE *****"
		SetFieldValue "Ingress", "tittel"
		SetFieldValue "BrodTekst", vbCrLf & vbCrLf & vbCrLf & "{re}**** Dette stoff formidler NTB PLUSS for andre ****"
		gSendTil = "ut"

	Case "prmpriv"
		SetFieldValue "DI", "PRM"
		SetFieldValue "GR", "PRI"
		SetFieldValue "ST", "PRM: kunde/tittel"
		SetFieldValue "Tittel", "PRM: kunde/tittel"
		SetFieldValue "TilRed", "(Til red.)"
		SetFieldValue "BrodTekst", "Vi gj�r oppmerksom p� at KUNDE n� har sendt ut en ny melding i NTB Pluss' pressemeldingstjeneste PRM:" & vbCrLf & vbCrLf & "TITTEL" & vbCrLf & vbCrLf & "{in}Meldingen ligger i stoffgruppe PRM." & vbCrLf
		gSendTil = "ut"

	Case "kuldagmeny"
		SetFieldValue "GR", "PRI"
		SetFieldValue "ST", "kultur-" & LCase(FinnUkedag(DatePart("w", Now(), vbMonday, vbFirstFourDays))) & "/K&U"
		datstr = LCase(FinnUkedag(DatePart("w", Now(), vbMonday, vbFirstFourDays))) & " "
		If DatePart("d", Now()) < 10 Then datstr = datstr & "0" 
		datstr = datstr & DatePart("d", Now()) & "."
 		If DatePart("m", Now()) < 10 Then datstr = datstr & "0"
		datstr = datstr & DatePart("m", Now()) & "." & DatePart("yyyy", Now())
		SetFieldValue "Tittel", "Kulturmeny " & datstr
		SetFieldValue "TilRed", "(Til red: Her er dagens K&U-saker. Vi tar forbehold for endringer (saker og/eller tidspunkt) som m�tte oppst� underveis. NB! K&U-sakene kan ogs� hentes fra www.ntb.no. Hvis din avis �nsker passord, vennligst send en e-post til kirsten.flagstad@ntb.no)"
		SetFieldValue "BrodTekst", "Kultur- og underholdningsnotiser (ca kl. 11.00, 14.00 og 17.00)"

	Case "kulukemeny"
		SetFieldValue "DI", "KUL"
		SetFieldValue "KA", "C"
		SetFieldValue "GR", "ART"
		SetFieldValue "UG", "KUL"
		SetFieldValue "ST", "kulturmeny-uke " & DatePart("ww", Now(), vbMonday, vbFirstFourDays) & "/K&U"
		SetFieldValue "Tittel", "Kulturplan, uke " & DatePart("ww", Now(), vbMonday, vbFirstFourDays)
		SetFieldValue "TilRed", "(Til red: Her er planlagte utsendelser for denne uken. Vi tar forbehold for endringer som kan oppst� underveis. Merk at vi mandag-fredag i tillegg produserer tre daglige notisbunker (fem notiser i hver)." & vbCrLf & "Meny sendes ut hver morgen i PRI-katalogen med ca. klokkeslett for dagens utsendelser." & vbCrLf & "NB! K&U-sakene kan ogs� hentes fra www.ntb.no. Hvis du �nsker passord, vennligst send en e-post til kirsten.flagstad@ntb.no)"

	Case "kulnotis"
		SetFieldValue "DI", "KUL"
		SetFieldValue "KA", "C"
		SetFieldValue "GR", "ART"
		SetFieldValue "UG", "KUL"
		If DatePart("d", Now()) < 10 Then datstr = "0" 
		datstr = datstr & DatePart("d", Now()) & "."
 		If DatePart("m", Now()) < 10 Then datstr = datstr & "0"
		datstr = datstr & DatePart("m", Now())
		SetFieldValue "ST", "notis-X/" & datstr & "/K&U"
		SetFieldValue "Tittel", "Kultur- og underholdningsnotiser-X, " & datstr
		SetFieldValue "BrodTekst", "[PT]" & vbCrLf & "{ti}" & vbCrLf & "{in}" & vbCrLf & "{bt}" & vbCrLf & "[PT]" & vbCrLf & vbCrLf & "{ti}" & vbCrLf & "{in}" & vbCrLf & "{bt}" & vbCrLf & "[PT]" & vbCrLf & vbCrLf & "{ti}" & vbCrLf & "{in}" & vbCrLf & "{bt}" & vbCrLf & "[PT]" & vbCrLf & vbCrLf & "{ti}" & vbCrLf & "{in}" & vbCrLf & "{bt}" & vbCrLf & "[PT]" & vbCrLf & vbCrLf & "{ti}" & vbCrLf & "{in}" & vbCrLf & "{bt}" & vbCrLf & "[PT]" & vbCrLf & vbCrLf

	Case "omtale"
		SetFieldValue "GR", "ART"
		SetFieldValue "UG", "PRS"
		SetFieldValue "KA", "C"
		SetFieldValue "DI", "PRS"   
		SetFieldValue "ST", "f�dselsdag-"
		SetFieldValue "TilRed", "(Til red: Spesialtjeneste, bare for spesialabonnenter.)"
		SetFieldValue "BrodTekst", vbCrLf & vbCrLf & vbCrLf &"(NTB Pluss)"
		SetFieldValue "Bilder", "Foto nr. xx kan bestilles hos Scanpix. Bestillingen sendes Scanpix v/Tove Dammen, postboks 1178 Sentrum, 0107 Oslo eller faks 22 00 32 10."
		gSendTil = "hold"
		
	Case "dagenidag"
		SetFieldValue "GR", "ART"
		SetFieldValue "UG", "DID"
		SetFieldValue "KA", "C"
		SetFieldValue "DI", "DID"   
		SetFieldValue "ST", "dagen-i-dag/"
		SetFieldValue "Tittel", "Dagen i dag "
		SetFieldValue "TilRed", "(Til red: Spesialtjeneste, bare for spesialabonnenter.)"
		mystr = "{mt}Soloppgang og solnedgang" & vbcrlf & "{bt}I dag, onsdag 29. desember, st�r sola opp i Oslo kl.  og g�r ned kl.  (dagen minker med  minutter), i Bergen opp kl. , ned kl.  (-  min.), i Trondheim opp kl.  og ned kl.  (-  min.), i Troms�  opp kl.  og ned kl.  (-  min.)." & vbCrLf & vbCrLf
		mystr = mystr & "{mt}Flo og fj�re" & vbcrlf & "{bt}I Oslo er det flo kl.  og , fj�re kl.  og , i Bergen flo kl.  og kl. , fj�re kl.  og kl. , i Trondheim flo kl.  og kl. , fj�re kl.  og kl. , i Troms� flo kl.  og kl. , fj�re kl.  og kl. ." & vbCrLf & vbCrLf
		mystr = mystr & "{mt}Det hendte" & vbcrlf & "{bt}" & vbCrLf & vbCrLf
		mystr = mystr & "{mt}Navnedag" & vbcrlf & "{bt}" & vbCrLf & vbCrLf
		mystr = mystr & "{mt}F�dselsdager" & vbcrlf & "{bt}" & vbCrLf & vbCrLf
		mystr = mystr & "{mt}Dagens sitat" & vbcrlf & "{bt}" & vbCrLf & vbCrLf
		SetFieldValue "BrodTekst", mystr
		gSendTil = "hold"

	Case "sitat"
		SetFieldValue "GR", "ART"
		SetFieldValue "UG", "SIT"
		SetFieldValue "KA", "C"
		SetFieldValue "DI", "NTB"   
		SetFieldValue "ST", "dagens-sitat/"
		SetFieldValue "Tittel", "Dagens sitat "
		gSendTil = "hold"
	
	End Select

End Sub

'---------------------------------------------------------------------
'--Function CheckPostVars --
'--Regler for hva som m� v�re fylt ut i en melding.
'--Returnerer feilmelding hvis den fant feil.
'---------------------------------------------------------------------
Function CheckPostVars

	Dim myMsg

	myMsg = ""
	If GetFieldValue("GR") = "" Then
		myMsg = "Du m� velge en gruppe"
	ElseIf GetFieldValue("UG") = "" Then
		myMsg = "Meldingen m� ha en undergruppe"
	ElseIf GetFieldValue("PR") = "" Then
		myMsg = "Prioritet m� fylles ut"
	ElseIf GetFieldValue("DI") = "" Then
		myMsg = "Meldingen m� ha en distribusjonskode"
	ElseIf GetFieldValue("ST") = "" Then
		myMsg = "Meldingen m� ha et stikkord"
	ElseIf GetFieldValue("Tittel") = "" Then
		myMsg = "Meldingen m� ha en tittel"
	End If
    
	CheckPostVars = myMsg
	
End Function

%>
