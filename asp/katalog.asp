<%  
	'-------------------------------------------
	'katalog.asp
	'Lister stikkordet p� meldinger i en katalog
	'9/8 1999 (jet)
	'-------------------------------------------
%>

<!-- #include file="headers.asp" -->
<!-- #include file="functions.asp" -->

<%
	Dim folder, file, files, myInfo(), myFolder, orgfolder, fullPath, showFolder
	Dim preStr, postStr, myRef
	Dim sorted(), tmp

	InitGlobals

	'S�rger for at oversikten alltid oppdateres
	Response.Expires = 0

	myFolder = Request.QueryString("dir")
	myType = Request.QueryString("type")
	orgfolder = myFolder
	'--locAddr = Request.ServerVariables("LOCAL_ADDR" )

	'Sjekker etter spesielle navn
	'Egen katalog
	If myFolder = "mydir" Then
		showFolder = gSign
		myFolder = "brukere/" & showFolder

	'Desk-katalog
	ElseIf myFolder = "mydesk" Then
		showFolder = gAvdeling
		If showFolder <> "" Then
			myFolder = showFolder & "/desk"
		Else
			myFolder = ""
		End If
		
	'Begge desk-kataloger
	ElseIf myFolder = "nyogrepdesk" Then
		showFolder = "Begge desker"
		If showFolder <> "" Then
			myFolder = "nyhet/desk"
		Else
			myFolder = ""
		End If

	'Utsendte meldinger
	ElseIf myFolder = "kilde" Then
		showFolder = "sendt/" & gAvdeling
		myFolder = showFolder
		
	'Ellers pr�ver vi � vise angitt katalog
	Else
		showFolder = myFolder
		
	End If
%>
	<HTML>
	<HEAD>
		<TITLE>Katalog: <%=showFolder%></TITLE>
		<!-- generert <%=Now()%> -->
	</HEAD>
	<BODY bgcolor=#ffeedd>
		<H2>Katalog: <%=showFolder%></H2>

<%
	If myFolder = "" Then
		Response.Write "<FONT color='red' size='4'>"
		Response.Write "Ingen katalog angitt."
		Response.Write "<P>Hvis du har bedt om � se desk-katalogen, s� skyldes dette at du ikke er "
		Response.Write "medlem av en gyldig avdeling. Kontakt da 505."

	Else
		myFolder = AddBaseDir(myFolder)   
		fullPath = Server.MapPath(myFolder)

		Set fso = CreateObject("Scripting.FileSystemObject")

		If Not fso.FolderExists(fullPath) Then
			If InStr(1, fullPath, "brukere\" & gSign) Then
				fso.CreateFolder fullPath
			Else
				Response.Write "<FONT size=4><I>Finner ikke katalogen..</I></FONT>"
			End If
		End If

		If fso.FolderExists(fullPath) Then
			If myType = "tekstarkiv" Then
%>
	<A href="<%=gAspDir%>/katalog.asp?dir=<%=orgFolder%>&type=tekstarkiv">[Oppdater]</A><BR>
<%
			Else
%>
	<A href="<%=gAspDir%>/katalog.asp?dir=<%=orgFolder%>">[Oppdater]</A><BR>
<%
			End If
%>
			
			<FORM action="<%=gAspDir%>/sok.asp" method="POST">
				S�k: <INPUT type="textbox" name="srch4" size="10">
				<INPUT type="hidden" name="dir" value="<%=myFolder%>">
			</FORM>
<%
			Set folder = fso.GetFolder(fullPath)
			Set files = folder.Files

			fileCnt = files.Count

			i = files.Count
			ReDim myInfo(5, i)

			For Each file in files

				'Overser l�se-filer
				If Right(file.Name, 4) <> ".lck" Then

					myRef = file.Name
					myInfo(0, i) = file.DateLastModified
					myInfo(1, i) = file.Name

					Set fs = fso.OpenTextFile(file, ForReading)
					myLine = fs.ReadAll
					fs.Close
		
					preStr = "<!--pre-ST-->"
					postStr = "<!--post-ST-->"
					If myType = "tekstarkiv" Then
						preStr = "<!--pre-SO-->"
						postStr = "<!--post-SO-->"
					End If
					myref = "[uten stikkord]"
					myStart = InStr(1, myLine, preStr)
					If myStart > 0 Then
						myEnd = InStr(1, myLine, postStr)
						If myEnd > myStart + Len(preStr) Then
							myRef = Mid(myLine, myStart + Len(preStr), myEnd - myStart - Len(PreStr))
						End If
					End If
					myInfo(2, i) = myRef

					preStr = "<!--pre-SI-->"
					postStr = "<!--post-SI-->"
					myStart = InStr(1, myLine, preStr)
					If myStart > 0 Then
						myEnd = InStr(1, myLine, postStr)
						If myEnd > 0 Then
							myInfo(3, i) = Mid(myLine, myStart + Len(preStr), myEnd - myStart - Len(PreStr))
						End If
					End If

					If myType = "tekstarkiv" Then
						preStr = "<!--pre-UT-->"
						postStr = "<!--post-UT-->"
						myStart = InStr(1, myLine, preStr)
						If myStart > 0 Then
							myEnd = InStr(1, myLine, postStr)
							If myEnd > myStart + Len(preStr) Then
								arkivtid = Mid(myLine, myStart + Len(preStr), myEnd - myStart - Len(PreStr))
							End If
						End If
						preStr = "<!--pre-DA-->"
						postStr = "<!--post-DA-->"
						myStart = InStr(1, myLine, preStr)
						If myStart > 0 Then
							myEnd = InStr(1, myLine, postStr)
							If myEnd > myStart + Len(preStr) Then
								arkivdato = Mid(myLine, myStart + Len(preStr), myEnd - myStart - Len(PreStr))
							End If
						End If
						''myinfo(0, i) = CDate(arkivdato & " " & arkivtid)
						myinfo(0, i) = Right(arkivdato, 2) & "." & Mid(arkivdato, 4, 2) & "." & Left(arkivdato, 2) & " " & arkivtid
					End If

					myinfo(5, i) = myfolder
								
				End If

				i = i - 1
			Next

			'-- Ekstra ved visning av begge desker --
			If LCase(orgFolder) = "nyogrepdesk" Then
				myFolder = "reportasje/desk"
				myFolder = AddBaseDir(myFolder)  
				fullPath = Server.MapPath(myFolder)

				Set folder = fso.GetFolder(fullPath)
				Set files = folder.Files

				fileCnt = fileCnt + files.Count

				i = fileCnt

				ReDim Preserve myInfo(5, i)

				For Each file in files

					'Overser l�se-filer
					If Right(file.Name, 4) <> ".lck" Then

						myRef = file.Name
						myInfo(0, i) = file.DateLastModified
						myInfo(1, i) = file.Name

						Set fs = fso.OpenTextFile(file, ForReading)
						myLine = fs.ReadAll
						fs.Close
		
						preStr = "<!--pre-ST-->"
						postStr = "<!--post-ST-->"
						myref = "[uten stikkord]"
						myStart = InStr(1, myLine, preStr)
						If myStart > 0 Then
							myEnd = InStr(1, myLine, postStr)
							If myEnd > myStart + Len(preStr) Then
								myRef = Mid(myLine, myStart + Len(preStr), myEnd - myStart - Len(PreStr))
							End If
						End If
						myInfo(2, i) = myRef

						preStr = "<!--pre-SI-->"
						postStr = "<!--post-SI-->"
						myStart = InStr(1, myLine, preStr)
						If myStart > 0 Then
							myEnd = InStr(1, myLine, postStr)
							If myEnd > 0 Then
								myInfo(3, i) = Mid(myLine, myStart + Len(preStr), myEnd - myStart - Len(PreStr))
							End If
						End If

						myinfo(5, i) = myfolder
											
					End If

					i = i - 1
				Next

			End If ' ekstra for begge desker


			If fileCnt > 0 Then
				ReDim sorted(fileCnt)
				sorted(1) = 1
			End If
	
			For i = 2 To fileCnt
				For j = i To 2 Step -1
					''If (myinfo(0, sorted(j-1)) < myinfo(0, i)) Then
					If DateDiff("s", myinfo(0, sorted(j-1)), myinfo(0, i)) > 0 Then
						sorted(j) = sorted(j-1)
						sorted(j-1) = i
					Else
						sorted(j) = i
						Exit For
					End If
				Next
			Next

			For j = 1 To fileCnt
				i = sorted(j)
				If myInfo(2, i) <> "" Then
					dato = FormatDateTime(myInfo(0, i), vbShortDate)
				
					If dato <> LastDate Then
						Response.Write "<H4>" & dato & "</H4>" & vbCrLf
						LastDate = dato
					End If
				
					Response.Write FormatDateTime(myInfo(0, i), vbShortTime)
					Response.Write "<A href='" & myinfo(5, i) & "/" & myInfo(1, i) & "' target='data'>" & vbCrLf
					Response.Write myInfo(2, i) & "</A> (" & myInfo(3, i) & ")</P>" & vbCrLf
				End If			
	
			Next

		End If 'FolderExists
        
	End If' myFolder <> ""
%>
</BODY>
</HTML>
    
