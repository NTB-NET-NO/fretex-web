<!-- #include file="headers.asp" -->
<%
	response.expires = 0
%>
<html>
<head>
    <title>Velg katalog</title>

    <script language="javascript">
    <!--
    function VisKatalog(katalog) {
	var url, opts;
	opts = "?dir=" + katalog;
	url = "<%=gAspDir%>/katalog.asp" + opts;
        opener.window.open(url,"indeks");
        self.close();
    }

    function VisAkopi(avdeling) {
	var url, opts;
	opts = "?avd=" + avdeling;
	url = "<%=gAspDir%>/akopi.asp" + opts;
        opener.window.open(url,"indeks");
        self.close();
    }
    // -->
    </script>
</head>
<body>
<p><h4>Velg katalog fra listen:</h4>
<ul>
<li><a href="javascript:VisKatalog('nyhet/desk')">Nyhetsdesk</a>
<li><a href="javascript:VisKatalog('reportasje/desk')">Reportasjedesk</a>
<li><a href="javascript:VisKatalog('sport/desk')">Sportsdesk</a>
<li><a href="javascript:VisKatalog('kultur/desk')">K&U desk</a>
<li><a href="javascript:VisKatalog('direkte/desk')">Direktedesk</a>
<li><a href="javascript:VisKatalog('media/desk')">Media-desk</a>
<li><a href="javascript:VisKatalog('feature/desk')">Featuredesk</a>
</ul>
<ul>
<li><a href="javascript:VisKatalog('sendt/innenriks')">Innenriks - dagens produksjon</a>
<li><a href="javascript:VisKatalog('sendt/utenriks')">Utenriks - dagens produksjon</a>
<li><a href="javascript:VisKatalog('sendt/sport')">Sport - dagens produksjon</a>
<li><a href="javascript:VisKatalog('sendt/annet')">Dagens produksjon - annet</a>
</ul>
<ul>
<li><a href="javascript:VisKatalog('hold')">Hold (p� vent)</a>
</ul>
<ul>
<li><a href="javascript:VisKatalog('nyhet/logger')">Nyhetslogger</a>
<li><a href="javascript:VisKatalog('reportasje/logger')">Reportasjelogger</a>
<li><a href="javascript:VisKatalog('sport/logger')">Sport logger</a>
</ul>
<ul>
<li><a href="javascript:VisAkopi('innenriks')">Innenriks kopier</a>
<li><a href="javascript:VisAkopi('utenriks')">Utenriks kopier</a>
<li><a href="javascript:VisAkopi('sport')">Sport kopier</a>
</ul>
<ul>
<li><a href="javascript:VisKatalog('sport/fakta')">Sport fakta-katalog</a>
</ul>
</p>
</body>
</html>
    
