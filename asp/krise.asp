<!--
    krise.asp
    Skjema for krise-melding til satellittsystemet.
    27/5 1999 (jet)
-->

<HTML>
<HEAD>
<TITLE>NTBs krisel�sning</TITLE>
</HEAD>
<BODY bgcolor="#FFEEDD">

<%
if Request.ServerVariables("REQUEST_METHOD") = "POST" then
    postMethod = true
    msgOK = true

    GR = Request.Form("GR")
    UG = Request.Form("UG")
    PR = Request.Form("PR")
    DI = Request.Form("DI")
    KA = Request.Form("KA")
    ST = Request.Form("ST")
    SI = Request.Form("SI")
    Tittel = Request.Form("tittel")
    TilRed = Request.Form("tilred")
    Ingress = Request.Form("ingress")
    Brodtekst = Request.Form("brodtekst")

    if UG = "" then
        UG = "NNN"
    end if
    if PR = "" then
        PR = "5"
    end if
    if DI = "" then
        DI = "ALL"
    end if

    if ST = "" then %>
        <FONT color="red" size="5">Feil: Meldingen m� ha et stikkord</FONT>
    <%
        msgOK = false
    end if

    if msgOK and Len(UG) <> 3 then %>
        <FONT color="red" size="5">Feil: Undergruppen m� v�re p� tre tegn</FONT>
        <% msgOK = false
    end if 

    if msgOk and Len(PR) <> 1 then %>
        <FONT color="red" size="5">Feil: Prioritet kan kun v�re ett tegn</FONT>
        <% msgOK = false
    end if
    if msgOK and not IsNumeric(PR) then %>
        <FONT color="red" size="5">Feil: Prioritet m� v�re et tall</FONT>
        <% msgOK = false
    end if 

    if msgOK and Len(DI) <> 3 then %>
        <FONT color="red" size="5">Feil: Distribusjonskoden m� v�re p� tre tegn</FONT>
        <% msgOK = false
    end if 


    if msgOk and Tittel = "" then %>
        <FONT color="red" size="5">Feil: Meldingen m� ha en tittel</FONT>
        <% msgOK = false
    end if 

end if  'Slutt p� if post.. %>

<%
'Meldingen er OK og sendes til Fip
if msgOK then
    ' Map current path to physical path
    'curDir = Server.MapPath("\usr\fip\spool\krise")
    ' Create FileSytemObject Component
    Set ScriptObject = Server.CreateObject("Scripting.FileSystemObject")

    tid = Now()
    FileName = Year(tid) & "-" & Month(tid) & "-" & Day(tid) & "-" & Hour(Tid) & "-" & Minute(tid)

    Application.Lock
    Application("Count") = Application("Count") + 1
    Application.Unlock
    FileName = FileName & "-" & Application("Count") & ".txt"

    ' Create and Write to a File
    Set MyFile = ScriptObject.CreateTextFile("d:\usr\fip\spool\krise\" & FileName, 2)
                
    MyFile.WriteLine "~"
    MyFile.WriteLine "GR:" & Trim(GR)
    MyFile.WriteLine "UG:" & UCase(Trim(UG))
    MyFile.WriteLine "PR:" & Trim(PR)
    MyFile.WriteLine "KA:" & Trim(KA)
    MyFile.WriteLine "DI:" & UCase(Trim(DI))
    MyFile.WriteLine "RT:" & Trim(ST)
    MyFile.WriteLine "ST:" & Trim(ST)
    MyFile.WriteLine "SI:" & Trim(SI)
    MyFile.WriteLine "QT:" & Trim(SI)
    MyFile.WriteLine "~"
    MyFile.WriteLine "#tittel:" & Tittel
    MyFile.WriteLine "#red:" & TilRed
    MyFile.WriteLine "#ingress:" & Ingress
    'Tekst = Replace(BrodTekst, Chr(10) & Chr(10), Chr(10) & Chr(10) & "@mtit")
    MyFile.WriteLine "#txt:" & Brodtekst

    Myfile.Close
    %>
    <H3>Meldingen er sendt til satellittsystemet</H3>
    <I>(Meldingsnavn: <%=FileName%>)</I>

<%
'Meldingen hadde mangler - viser skjema p� nytt
else %>
    <% if not postMethod then
        UG = "NNN"
        DI = "ALL"
        PR = "5"
        ST = ""
        SI = ""
        Tittel = ""
        TilRed = ""
        Ingress = ""
        Brodtekst = ""
    end if %>

    <FORM method="POST">
    <TABLE cellspacing="15">
    <TR>
	<TD>
        <!-- GRUPPE -->
		<FONT color="navy">Gruppe:</FONT>&nbsp;
		<SELECT name="GR" alt="Hei">
        <% if postMethod then %>
            <% if GR = "INN" then %>
		    	<OPTION SELECTED>INN
			    <OPTION>UTE
    			<OPTION>SPO
	    		<OPTION>PRI
            <% end if %>
            <% if GR = "UTE" then %>
			    <OPTION>INN
    			<OPTION SELECTED>UTE
	    		<OPTION>SPO
		    	<OPTION>PRI
            <% end if %>
            <% if GR = "SPO" then %>
	    		<OPTION>INN
		    	<OPTION>UTE
			    <OPTION SELECTED>SPO
    			<OPTION>PRI
            <% end if %>
            <% if GR = "PRI" then %>
		    	<OPTION>INN
			    <OPTION>UTE
    			<OPTION>SPO
	    		<OPTION SELECTED>PRI
            <% end if %>
        <% else %>
           	<OPTION SELECTED>INN
		    <OPTION>UTE
   			<OPTION>SPO
    		<OPTION>PRI
        <% end if %>
		</SELECT>
		&nbsp;&nbsp;&nbsp;
        <!-- UNDERGRUPPE -->
		<FONT color="navy">Undergruppe:</FONT>&nbsp;
   		<INPUT type="textbox" name="UG" size="5" value="<%=UG%>">
		&nbsp;&nbsp;&nbsp;

        <!-- PRIORITET -->
		<FONT color="navy">Prioritet:&nbsp;
   		<INPUT type="textbox" name="PR" size="3" value="<%=PR%>">
	</TD>
	<TD>
        <!-- KANAL -->
		<FONT color="navy">Kanal:</FONT>&nbsp;
		<SELECT name="KA">
            <% if postMethod then %>
                <% if KA = "A" then %>
        			<OPTION SELECTED>A
		    	    <OPTION>A,N
    			    <OPTION>C
	    		    <OPTION>C,N
		        	<OPTION>N
                <% end if %>
                <% if KA = "A,N" then %>
        			<OPTION>A
		    	    <OPTION SELECTED>A,N
    			    <OPTION>C
	    		    <OPTION>C,N
		        	<OPTION>N
                <% end if %>
                <% if KA = "C" then %>
        			<OPTION>A
		    	    <OPTION>A,N
    			    <OPTION SELECTED>C
	    		    <OPTION>C,N
		        	<OPTION>N
                <% end if %>
                <% if KA = "C,N" then %>
        			<OPTION>A
		    	    <OPTION>A,N
    			    <OPTION>C
	    		    <OPTION SELECTED>C,N
		        	<OPTION>N
                <% end if %>
                <% if KA = "N" then %>
        			<OPTION>A
		    	    <OPTION>A,N
    			    <OPTION>C
	    		    <OPTION>C,N
		        	<OPTION SELECTED>N
                <% end if %>
            <% else %>
       			<OPTION SELECTED>A
	    	    <OPTION>A,N
   			    <OPTION>C
    		    <OPTION>C,N
	        	<OPTION>N
            <% end if %>
		</SELECT>
		&nbsp;&nbsp;&nbsp;
        <!-- DISTKODE -->
		<FONT color="navy">Distkode:</FONT>&nbsp;
   		<INPUT type="textbox" name="DI" size="5" value="<%=DI%>" align="middle">
	</TD>
    </TR>
    <TR>

    <!-- STIKKORD -->
	<TD><FONT color="navy">Stikkord:</FONT>&nbsp;
        <INPUT type="textbox" name="ST" size="16" value="<%=ST%>">

    <!-- SIGNATUR -->
	<TD><FONT color="navy">Signatur:</FONT>&nbsp;
        <INPUT type="textbox" name="SI" size="16" value="<%=SI%>">
    </TR>
    </TABLE>
    <HR>
    <TABLE>
    <TR>

    <!-- TITTEL -->
	<TD align="right"><FONT color="navy" size="5">Tittel:</FONT>
	<TD><INPUT type="textbox" name="tittel" size="70" value="<%=Tittel%>">
    </TR>
    <TR>

    <!-- TILRED -->
	<TD align="right"><FONT color="navy" size="5">Til red.:</FONT>
	<TD><TEXTAREA name="tilred" rows="3" cols="65" wrap="physical"><%=TilRed%></TEXTAREA>
    </TR>
    <TR>

    <!-- INGRESS -->
	<TD align="right"><FONT color="navy" size="5">Ingress:</FONT>
	<TD><TEXTAREA name="ingress" rows="5" cols="65" wrap="physical"><%=Ingress%></TEXTAREA>
    </TR>
    <TR>

    <!-- BRODTEKST -->
	<TD align="right">
        <FONT color="navy">
            <FONT size="5">Br�dtekst:</FONT>
            <BR>(Ved nytt avsnitt m�<BR>linjen begynne med<BR>ordmellomrom.)
        </FONT>
	<TD><TEXTAREA name="brodtekst" rows="10" cols="65" wrap="physical"><%=Brodtekst%></TEXTAREA>
    </TR>
    <TR></TR>
    <TR>
	<TD>
	<TD><INPUT type="submit" value="Send meldingen">
    <!--
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <INPUT type="reset" value="Nullstill alle felter">
    -->
    </TR>
    </TABLE>
<% end if  'slutt p� msgOK %>
</BODY>
</HTML>
