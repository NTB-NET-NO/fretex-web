<!-- #include file="headers.asp" -->
<%
	response.expires = 0
%>
<html>
<head>
    <title>Velg meldingstype</title>

    <script language="javascript">
    <!--
	function OpenWindow(url, target) {
		opener.window.open(url,target,'toolbar=1,location=0,directories=0,status=1,menubar=1,scrollbars=1,resizable=1,width=750,height=600,top=0,left=50');
		self.close();
	}

    function LagMelding(type) {
		var url, opts;
		opts = "?dir=" + katalog;
		url = "<%=gAspDir%>/rediger.asp" + opts;
        opener.window.open(url,"indeks");
        self.close();
    }
    // -->
    </script>
</head>
<body>
<p><h4>Velg meldingstype:</h4>
<ul>
<li><a href="javascript:OpenWindow('<%=gAspDir%>/valuta.asp','indeks')">Valuta</a>
<li><a href="javascript:OpenWindow('<%=gAspDir%>/teleavis.asp?action=ny','_blank')">Teleavis</a>
</ul>
<ul>
<li><a href="javascript:OpenWindow('<%=gAspDir%>/rediger.asp?type=omtale','_blank')">Personalia-omtale</a>
<li><a href="javascript:OpenWindow('<%=gAspDir%>/rediger.asp?type=dagenidag','_blank')">Dagen i dag</a>
</li>
</ul>
<ul>
<li><b>Manuell tabeller:</b></li>
<ul>
<li><a href="javascript:OpenWindow('<%=gAspDir%>/tabell-manuell.asp?type=fotball','_blank')">Fotball</a>
<li><a href="javascript:OpenWindow('<%=gAspDir%>/tabell-manuell.asp?type=handball','_blank')">H�ndball</a>
<li><a href="javascript:OpenWindow('<%=gAspDir%>/tabell-manuell.asp?type=volleyball','_blank')">Volleyball</a>
<li><a href="javascript:OpenWindow('<%=gAspDir%>/tabell-manuell.asp?type=ishockey','_blank')">Ishockey</a>
<li><a href="javascript:OpenWindow('<%=gAspDir%>/tabell-manuell.asp?type=bandy','_blank')">Bandy</a>
<li><a href="javascript:OpenWindow('<%=gAspDir%>/tabell-manuell.asp?type=basket','_blank')">Basketball</a>
<li><a href="javascript:OpenWindow('<%=gAspDir%>/tabell-manuell.asp?type=medaljer','_blank')">Medaljeoversikt</a>
</li>
</ul>
</ul>
</p>
</body>
</html>
    
