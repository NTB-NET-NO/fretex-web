<%  
'-------------------------------------------
'nrkomtaler.asp
'For � samle NRK-omtaler.
'5/1 2000 (jet)
'-------------------------------------------
%>
<!-- #include file="headers.asp" -->
<!-- #include file="functions.asp" -->
<!-- #include file="savefile.asp" -->
<%
Dim omtDir, fsDir, theDir, file, str, mystr
Dim outfile, mycnt
Dim mydate, dateParts

omtDir = AddBaseDir("media/omtaler")

''Set auth = Server.CreateObject("AUTHXOCX.AuthXOCXCtrl.1")
''Set fso = CreateObject("Scripting.FileSystemObject")
''gSign = GetSign()
''gAvdeling = GetAvdeling()

InitGlobals

If Request.ServerVariables("REQUEST_METHOD") = "POST" Then
	mydate = Request.Form("dato")
Else
	mydate = Request.QueryString("dato")
End If

dateParts = Split(mydate, "/")

fsDir = Server.MapPath(omtDir & "/" & dateParts(0) & "/" & dateParts(1) & "/" & dateParts(2))

If fso.FolderExists(fsdir) Then
	Set theDir = fso.GetFolder(fsDir)

	str = vbCrLf
	mycnt = 0
	For Each file In theDir.files
		Set fs = fso.OpenTextFile(file.path, ForReading)
		mystr = fs.ReadAll
		fs.Close

		i = InStr(1, mystr, vbCrLf)
		str = str & "{in}" & Left(mystr, i) & "{bt}"
		str = str & Right(mystr, Len(mystr)-i-1)
 		str = str & vbCrLf & vbCrLf
		mycnt = mycnt + 1
	Next

	str = replace(str, vbCrLf, "<BR>")

	stikkord = "nrk-omt-" & LCase(Left(FinnUkedag(DatePart("w", mydate, vbMonday, vbFirstFourDays)), 3)) & "-" 
	If Day(mydate) < 10 Then stikkord = stikkord & "0"
	stikkord = stikkord & Day(mydate) & "-" 
	stikkord = stikkord & LCase(FinnMaaned(DatePart("m", mydate, vbMonday, vbFirstFourDays)))

	SetFieldValue "ST", stikkord
	SetFieldValue "GR", "NRK"
	SetFieldValue "UG", "OMT"
	SetFieldValue "PR", "5"
	SetFieldValue "KA", "C"
	SetFieldValue "DI", "ALL"
	SetFieldValue "Tittel", "NRK-omtaler " & LCase(FinnUkedag(DatePart("w", mydate, vbMonday, vbFirstFourDays))) & " " & Day(mydate) & ". " & LCase(FinnMaaned(DatePart("m", mydate, vbMonday, vbFirstFourDays)))
	SetFieldValue "BrodTekst", str

	outFile = Server.MapPath(AddBaseDir("/brukere/" & gSign & "/" & GetUniqueName))

	SaveFile(outFile)	

%>
<h3>Fant <%=mycnt%> omtaler for <%=mydate%>.</h3>
En meldingen som inneholder disse er opprettet i din personlige katalog.
<%
Else
%>
<h3>Fant ingen omtaler for <%=mydate%>.</h3>
<%
End If
%>

