<!-- #include file="headers.asp" -->
<!-- #include file="functions.asp" -->
<!-- #include file="writeform.asp" -->
<!-- #include file="formjscript.asp" -->
<!-- #include file="savefile.asp" -->

<%  
'---------------------------------------------------------------------
'rediger.asp
'Kalles n�r en melding skal behandles (opprettes, lagres osv.)
'---------------------------------------------------------------------

'--Globale variabler--
Dim gFileName			'Filnavn som evt. sendes som parameter
Dim gAction			'Funksjon som skal utf�res
Dim gDestDir			'Destinasjon hvis filen skal sendes
Dim gMldType			'Meldingstype ved ny melding
Dim gSrvFile			'Filnavn i filsystemet
Dim gMsg			'Evt. melding som skal returneres til brukeren
Dim gTitMsg			'Flagg for om melding skal skrives i tittelen
Dim gDoWrite			'Flagg for om skjema skal returneres
Dim gIsLocked			'Flagg for om meldingen var l�st (ved redigering)
Dim gNoPerm			'Flagg for ikke redigeringstilgang
Dim gForceOpen			'Flagg for om l�sing skal overstyres
Dim gSendTil			'Inneholder std. destinasjon (for enkelte meldingstyper)
'
''Dim gAvdeling			'Brukerens avdeling
''Dim gSign			'Signatur
''Dim gOrg			'Organisasjon (NTB/Pluss)

gDoWrite = True
gIsLocked = False
gForceOpen = False

Response.Expires = 0

''Set fso = Server.CreateObject("Scripting.FileSystemObject")
''Set auth = Server.CreateObject("AUTHXOCX.AuthXOCXCtrl.1")
''gSign = GetSign()
''gAvdeling = GetAvdeling()

InitGlobals

'---------------------------------------------------------------------
'REQUEST_METHOD = POST
'Brukes n�r meldingen skal redigeres, lagres, eller sendes samt n�r
'l�sing skal overstyres.
'---------------------------------------------------------------------
if Request.ServerVariables("REQUEST_METHOD") = "POST" then

	'-- Henter variablene som ble sendt fra formen.
	'-- Globale variabler som settes her:
	'-- gFileName, gAction, gDestDir, gForceOpen
	GetPostVars

	'-- Sjekker gAction fra GetPostVars
	Select Case gAction
		'Filen skal lagres (i egen katalog)
		Case "save"
			SaveFile(gSrvFile)
			SetLock(gSrvFile)
			gMsg = "Sist lagret " & FormatDateTime(Now(), vbShortTime)
			gTitMsg = True
			gSendTil = gDestDir
			
		Case "saveclose"
			gMsg = RemoveLock(gSrvFile)
			If gMsg = "" Then
				gMsg = "Meldingen er lagret og l�sing er fjernet"
			Else
				i = InStrRev(gSrvFile, "\")
				gSrvFile = Left(gSrvFile, i) & GetUniqueName
				gMsg = gMsg & "<P>Meldingen er lagret med nytt navn"
			End If
			SaveFile(gSrvFile)
			
			gDoWrite = False

		Case "delete"
			DeleteFile(gSrvFile)
			gDoWrite = False
			gMsg = "Meldingen er lagret og l�sing er fjernet"

		'Filen skal sendes
		Case "send"
			gMsg = RemoveLock(gSrvFile)
			If gMsg = "" Then
				'Lagrer filen i katalogen gDestDir
				If SendFile(gSrvFile, gDestDir) Then
					gMsg = "Meldingen er sendt til '" & gDestDir & "'"
					gDoWrite = False
				Else
					If gMsg = "" Then
						gMsg = "Klarte ikke � sende filen."
					End If
					gDoWrite = True
				End If
			Else
				i = InStrRev(gSrvFile, "\")
				gSrvFile = Left(gSrvFile, i) & GetUniqueName
				SaveFile(gSrvFile)
				gMsg = gMsg & "<P>Meldingen er lagret med nytt navn.<BR>Hent opp den nye meldingen og send evt. p� nytt"
				gDoWrite = False
			End If

		'Ny versjon
		Case "nyver"
			'Leser info fra filen
			ParseFile(gSrvFile)
			
			'Endrer stikkord og setter TilRed
			LagNyVersjon	
			
			'Setter nytt filnavn
			gFileName = AddBaseDir("brukere/" & gSign & "/" & GetUniqueName) 
			gSrvFile = Server.MapPath(gFileName)
				
			'Lagrer info igjen
			SaveFile(gSrvFile)
			SetLock(gSrvFile)
			gMsg = "Sist lagret " & FormatDateTime(Now(), vbShortTime)
			gTitMsg = True
			
		'Meldingen skal redigeres
		Case "edit"
			'Henter info fra filen og setter l�s
			RedigerFil
			SetLock(gSrvFile)

		'Meldingen er �pnet - tar kopi til egen katalog
			Case "copy"
				'Leser info fra filen
				ParseFile(gSrvFile)
				
				'Setter nytt filnavn
				gFileName = AddBaseDir("brukere/" & gSign & "/" & GetUniqueName) 
				gSrvFile = Server.MapPath(gFileName)
				
				'Lagrer info igjen
				SaveFile(gSrvFile)
				SetLock(gSrvFile)
				gMsg = "Sist lagret " & FormatDateTime(Now(), vbShortTime)
				''gDoWrite = False

	End Select

'---------------------------------------------------------------------
'REQUEST_METHOD = GET
'Brukes for � opprette en ny eller hente en melding.
'---------------------------------------------------------------------
Else
	'Henter evt. filnavn (ved redigering av filer)
	gFileName = Request.QueryString("filename")
	'Henter evt. meldingstype (ved ny melding)
	gMldType = LCase(Request.QueryString("type"))

	'-- Redigere hvis et filnavn var angitt
	If gFileName <> "" Then

		gFileName = AddBaseDir(gFileName)
		gSrvFile = Server.MapPath(gFileName)

		If Request.QueryString("nyver") = 1 Then
			'Leser info fra filen
			ParseFile(gSrvFile)
			
			'Endrer stikkord og setter TilRed
			LagNyVersjon	
			
			'Setter nytt filnavn
			gFileName = AddBaseDir("brukere/" & gSign & "/" & GetUniqueName) 
			gSrvFile = Server.MapPath(gFileName)
				
			'Lagrer info igjen
			SaveFile(gSrvFile)
			SetLock(gSrvFile)
			gMsg = "Sist lagret " & FormatDateTime(Now(), vbShortTime)
			gTitMsg = True

		Else
			RedigerFil
		End If

	'-- Ellers opprettes ny melding
	Else
		'Sett header basert p� avdeling
		SetDefaultHeaders
		
		'Gi meldingen et navn
		gFileName = gBaseDir & "/brukere/" & gSign & "/" & GetUniqueName
	End If
End If

Set fso = Nothing

%>

<HTML>
<HEAD>
   <STYLE TYPE="text/css">
        <!--
          A { text-decoration: none;  }  
          A:visited, A:hover, A:active { text-decoration: none; } 
        // -->
    </STYLE>

<%
	response.write "<TITLE>Fretex: "
	myval = GetFieldValue("ST")
	If myVal = "" Then
		response.write "[uten stikkord]"
	Else
		response.write myVal
	End If

	If gDoWrite Then
		If (gMsg <> "") And (gTitMsg) Then
			Response.Write " - " & gMsg
		End If
		Response.Write "</TITLE>" & vbCrLf
		WriteFormJScript
		
	Else
		Response.Write "</TITLE>" & vbCrLf
	End If
	
%>

</HEAD>
<BODY bgcolor="#ffeedd" alink="navy" vlink="navy" link="navy">

<%
'--Skriv evt. melding til brukeren
If (gMsg <> "") And (Not gTitMsg) Then
		Response.Write "<H3><FONT color='red'>" & gMsg & "</FONT></H3>" & vbCrLf
		If Not gDoWrite Then
			Response.Write "<FORM><INPUT type='button' value='Lukk' onclick='self.close()'></FORM>"
		End If
End If

'--Skriv skjema hvis det skal gj�res
If gDoWrite Then
	WriteForm

'--Hvis meldingen var l�st lages knapper for overstyring av dette
ElseIf gIsLocked Or gNoPerm Then
%>
	<FORM method="POST">
	<INPUT type="hidden" name="filename" value="<%=gFileName%>" size="1">
<%
	If gIsLocked Then
%>
		<INPUT type="Submit" name="Go" value="Rediger likevel">
<%
	End If
%>
		<INPUT type="Submit" name="Go" value="Kopier melding">
	</FORM>
<%    
End If
%>
</BODY>
</HTML>





