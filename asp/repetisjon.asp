<%  
	'-------------------------------------------
	'repetisjon.asp
	'Lister meldinger for repetisjon til en 
	'enkelt satellitt-kunde
	'29/11 1999 (jet)
	'-------------------------------------------
%>

<!-- #include file="headers.asp" -->
<!-- #include file="functions.asp" -->
<!-- #include file="savefile.asp" -->

<%
	Dim folder, file, files, myInfo(), myFolder, fullPath, showFolder
	Dim preStr, postStr, myRef, myDate
	Dim sorted(), tmp
''	Dim gAvdeling
''	Dim gSign
''	Dim gOrg
	Dim kunde
	Dim sendtDir, utDir

	sendtDir = gBaseDir & "/sendt"
	utDir = gBaseDir & "/ut"

''	Set auth = Server.CreateObject("AUTHXOCX.AuthXOCXCtrl.1")
''	Set fso = CreateObject("Scripting.FileSystemObject")
''	gSign = GetSign()
''	gAvdeling = GetAvdeling()

	InitGlobals

	'S�rger for at oversikten alltid oppdateres
	Response.Expires = 0

	If Request.ServerVariables("REQUEST_METHOD") = "POST" Then

		kunde = UCase(Request.Form("dist"))

		i = 0
		If Request.Form("send") = "Send" Then
			Response.Write "<H3>F�lgende meldinger ble repetert til " & kunde & " den " & FormatDateTime(Now(), vbShortDate) & ":</H3>"
			Response.Write vbCrLf & "<UL>"

			For Each file In Request.Form("filename")
				i = i + 1
				ParseFile Server.MapPath(file)

				response.write "<LI>" & GetFieldValue("ST") & vbCrLf

				SetFieldValue "DI", kunde
				SetFieldValue "KA", "C"
				SetFieldValue "ST", "/REP/" & GetFieldValue("ST")
				SetFieldValue "Tittel", "/REPETISJON/" & GetFieldValue("Tittel")
				SetFieldValue "Ticker", ""

				bName = GetUniqueName
				fNames = Split(bName, "-")
				If UBound(fNames) = 5 Then
					fi = InStrRev(fNames(5), ".")
					cName = fNames(2) & "-" & fNames(3) & "-" & fNames(4) & "-" & Left(fNames(5), fi)
				End If
				SaveFile(fso.BuildPath(Server.MapPath(utDir), cName))
				Request.Form("filename")
			Next
		
			response.write "</UL><P><B>Totalt " & i & " meldinger.</B></P>"

		End If
		
	Else
%>
	<HTML>
	<HEAD>
		<TITLE>Repetisjoner</TITLE>
		<!-- generert <%=Now()%> -->
		<SCRIPT language="javascript">
		<!--
			function VisSatKunder() {
				window.open("satkunder.asp","popup",'toolbar=0,location=0,directories=0,status=0,menubar=0,scrollbars=1,resizable=0,width=300,height=600,top=0,left=100');
			}

			function SjekkDist() {
				var str;	
				str = document.form.dist.value;
				if (str.length != 3) {
					alert("Du m� angi mottaker (tre tegn)!");
					return false;
				}	
			}
		// -->
		</SCRIPT>
	</HEAD>
	<BODY bgcolor=#ffeedd>
		<H2>Repetisjon</H2>

<%
	myDate = Request.QueryString("dato")
	longdate = Year(mydate)
	If Month(mydate) < 10 Then longdate = longdate & "0"
	longdate = longdate & Month(mydate)
	If Day(mydate) < 10 Then longdate = longdate & "0"
	longdate = longdate & Day(mydate)

	rootFolder = Server.MapPath(sendtDir)

	myFolder = Request.QueryString("dir")
	showFolder = myFolder
		
	If Not fso.FolderExists(rootFolder) Then
		Response.Write "<FONT size=4><I>Finner ikke katalogen..</I></FONT>"
	Else
%>
		Velg mottaker, marker meldingene som skal repeteres og trykk 'Send'.
			<FORM name="form" target="data" method="POST" action="repetisjon.asp?dir=<%=myFolder%>" onsubmit="return SjekkDist()">
				<a href="javascript:VisSatKunder()">[Mottaker]</a>:
				 <INPUT type="textbox" size="3" name="dist">
				&nbsp;&nbsp;&nbsp;<input type="submit" value="Send" name="send">
<%
			Set rFolder = fso.GetFolder(rootFolder)
			Set sFolders = rFolder.SubFolders
			totCnt = 0

			For Each folder in sFolders
				'Sjekk f�rst om meldingene er flyttet til undermappe
				oldFolder = folder & "\" & longDate
				If fso.FolderExists(oldFolder) Then
					xtraFolder = fso.GetBaseName(folder)
					Set folder = fso.GetFolder(oldFolder)
				Else
					xtraFolder = ""
				End If

				Set files = folder.Files
			
				totCnt = totCnt + files.Count
				ReDim Preserve myInfo(5, totCnt)

				i = totCnt
				For Each file in files

					'Overser l�se-filer
					If Right(file.Name, 4) <> ".lck" Then
						If DateDiff("d", file.DateLastModified, myDate) = 0 Then
							myRef = file.Name
							myInfo(0, i) = file.DateLastModified
							myInfo(1, i) = file.Name

							Set fs = fso.OpenTextFile(file, ForReading)
							myLine = fs.ReadAll
							fs.Close
		
							preStr = "<!--pre-ST-->"
							postStr = "<!--post-ST-->"
							myref = "xxx"
							myStart = InStr(1, myLine, preStr)
							If myStart > 0 Then
								myEnd = InStr(1, myLine, postStr)
								If myEnd > 0 Then
									myRef = Mid(myLine, myStart + Len(preStr), myEnd - myStart - Len(PreStr))
								End If
							End If
							myInfo(2, i) = myRef

							preStr = "<!--pre-SI-->"
							postStr = "<!--post-SI-->"
							myStart = InStr(1, myLine, preStr)
							If myStart > 0 Then
								myEnd = InStr(1, myLine, postStr)
								If myEnd > 0 Then
									myInfo(3, i) = Mid(myLine, myStart + Len(preStr), myEnd - myStart - Len(PreStr))
								End If
							End If

							If xtraFolder <> "" Then
								myInfo(5, i) = xtraFolder & "/" & folder.name
							Else
								myInfo(5, i) = folder.name
							End If
						End If
					End If

					i = i - 1
				Next
			Next

			If totCnt > 0 Then
				ReDim sorted(totCnt)
				sorted(1) = 1
			End If
	
			For i = 2 To totCnt
				For j = i To 2 Step -1
					If (myinfo(0, sorted(j-1)) < myinfo(0, i)) Then
						sorted(j) = sorted(j-1)
						sorted(j-1) = i
					Else
						sorted(j) = i
						Exit For
					End If
				Next
			Next

			For j = 1 To totCnt
				i = sorted(j)
				If myInfo(2, i) <> "" Then
					dato = FormatDateTime(myInfo(0, i), vbShortDate)
				
					If dato <> LastDate Then
						Response.Write "<H4>" & dato & "</H4>" & vbCrLf
						LastDate = dato
					End If
				
					Response.Write "<INPUT type='checkbox' name='filename' value='" & sendtDir & "/" & myinfo(5, i) & "/" & myInfo(1, i) & "'>"
					Response.Write FormatDateTime(myInfo(0, i), vbShortTime)
					Response.Write "<A href='" & sendtDir & "/" & myInfo(5, i) & "/" & myInfo(1, i) & "' target='data'>" & vbCrLf
					Response.Write myInfo(2, i) & "</A> (" & myInfo(3, i) & ")</P>" & vbCrLf
				End If			
	
			Next

		End If 'FolderExists

	End If 'RequestMethod = POST
%>
</FORM>
</BODY>
</HTML>
    
