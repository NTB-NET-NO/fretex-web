<%
'---------------------------------------------------------------------
'savefile.asp
'Funksjoner som brukes til lagring av filer
'Endret av Roar Vestre, NTB, 17.09.2002
'Endret av Roar Vestre, NTB, 13.05.2003, Erstattet <BR>, <P> </P> i br�dtekst
'---------------------------------------------------------------------

'---------------------------------------------------------------------
'-- Function SaveFile
'-- Lagrer data til fil. Feltverdier m� v�re satt.
'-- Inn:	fullt filnavn
'-- Ut:	True/False
'---------------------------------------------------------------------
Function SaveFile(saveName)

	SaveFile = WriteHTML(saveName)

End Function

'---------------------------------------------------------------------
'-- Function SendFile
'-- Sender en fil, dvs. lagrer i en ny katalog, lager kopi
'-- og sletter original
'-- Inn:	Fullt originalnavn, ny katalog
'-- Ut:	True/False
'---------------------------------------------------------------------
Function SendFile(saveName, destDir)

	Dim bName, newDir, i, retVal, cpDir, dato, myAvd
	Dim ingenKopi

	ingenKopi = Array("atex", "brukere", "hold")

	retVal = False

	' Erstatt "vennlige navn"
	destDir = LCase(destDir)
	destDir = Replace(destDir, "nyhetsdesken", "nyhet/desk")
	destDir = Replace(destDir, "reportasjedesken", "reportasje/desk")
	destDir = Replace(destDir, "sportsdesken", "sport/desk")
	destDir = Replace(destDir, "featuredesken", "feature/desk")

	' Sett ticker hvis meldingen sendes til nyhetsdesken
	If destDir = "nyhet/desk" Then
		SetFieldValue "Ticker", "ja"
	End If

	newDir = AddBaseDir(destDir)
	newDir = Server.MapPath(newDir)

	' Opprett mappe hvis den ikke finnes
	If Not fso.FolderExists(newDir) Then
		gMsg = "Katalogen " & RemoveBaseDir(newDir) & " finnes ikke"
		gMsgCritical = True
		gDoWrite = True
		SendFile = False
		Exit Function
	End If

	' Finn filnavn
	i = InStrRev(saveName, "\")
	If i > 0 Then
		bName = Right(saveName, Len(saveName)-i)

		' Sjekk om filen finnes
		If fso.FileExists(newDir & "\" & bName) Then
			bName = GetUniqueName
		End If

		cName = bName
		i = InStrRev(newdir, "\")
		theDir = LCase(Trim(Right(newdir, Len(newDir)-i)))

		Select Case theDir

		Case "ut"
			' Sjekk om dette er NTB Direkte
			If InStr(1, LCase(newdir), "\direkte\") Then
				cpDir = Server.MapPath(AddBaseDir("direkte/sendt"))

			ElseIf InStr(1, LCase(newdir), "\feature\") Then
				cpDir = Server.MapPath(AddBaseDir("feature/sendt"))

			Else ' Til satellitt
				fNames = Split(bName, "-")
				If UBound(fNames) = 5 Then
					fi = InStrRev(fNames(5), ".")
					cName = fNames(2) & "-" & fNames(3) & "-" & fNames(4) & "-" & Left(fNames(5), fi)
				End If
				SetFieldValue "TI", Now()
				cpDir = Server.MapPath(AddBaseDir("sendt/" & GruppeTilAvdeling))
			End If

		Case "logger"
			fNames = Split(bName, "-")
			If UBound(fNames) = 5 Then
				fi = InStrRev(fNames(5), ".")
				bName = fNames(2) & "-" & fNames(3) & "-" & fNames(4) & "-" & Left(fNames(5), fi)
			End If
			cpDir = Server.MapPath(AddBaseDir("atex/logger"))

		Case Else
			dato = FormatDateTime(Now(), vbShortDate)

			i = 0
			For Each cpDir in ingenKopi
				i = i + InStr(1, newdir, cpdir)
			Next
			If i = 0 Then
				cpDir = newDir & "\..\kopier\" & dato
			End If

		End Select

		' Opprett kopi-katalog hvis den ikke finnes
		If (cpDir <> "") And (Not fso.FolderExists(cpDir)) Then
			fso.CreateFolder(cpDir)
		End If

		' Lagre p� ny destinasjon
		retVal = WriteHTML(newDir & "\" & cName)

		' Lag kopi (et niv� opp, underkatalog kopier/dato)
		' CopyFile newDir & "\" & bName, cpDir, bName
		If cpDir <> "" Then
			If fso.FileExists(cpDir & "\" & bName) Then
				bName = GetUniqueName
			End If
			retval = WriteHTML(cpDir & "\" & bName)
		End If

		'-- Ekstra kopi ved utsendelse
		If LCase(destDir) = "ut" Then
			''''Kopi av pressemeldinger til idesk
			'''If UCase(GetFieldValue("GR")) = "PRM" Then
			'''	retval = WriteHTML(Server.MapPath(AddBaseDir("innenriks/desk")) & "\c" & bName)
			'''End If

			' Kopi av iriksmeny (dagsmeny)
			If (UCase(GetFieldValue("GR")) = "PRI") And (Left(LCase(GetFieldValue("ST")), 6) = "iriks-") Then
				SetFieldValue "GR", "INN"
				SetFieldValue "UG", "NNN"
				SetFieldValue "DI", "ALL"
				SetFieldValue "KA", "A"
				retval = WriteHTML(newDir & "\" & cName & "cp1")
			End If

			' Kopi av uriksmeny (dagsmeny)
			If (UCase(GetFieldValue("GR")) = "PRI") And (Left(LCase(GetFieldValue("ST")), 6) = "uriks-") Then
				SetFieldValue "GR", "UTE"
				SetFieldValue "UG", "NNN"
				SetFieldValue "DI", "ALL"
				SetFieldValue "KA", "A"
				retval = WriteHTML(newDir & "\" & cName & "cp1")
			End If

			' Kopi av kulturmeny (dagsmeny)
			If (UCase(GetFieldValue("GR")) = "PRI") And (Left(LCase(GetFieldValue("ST")), 7) = "kultur-") Then
				SetFieldValue "GR", "ART"
				SetFieldValue "UG", "KUL"
				SetFieldValue "DI", "KUL"
				SetFieldValue "KA", "C"
				retval = WriteHTML(newDir & "\" & cName & "cp1")
			End If

			' Kopi av valutamelding
			If (UCase(GetFieldValue("GR")) = "INN") And (Left(LCase(GetFieldValue("ST")), 7) = "valuta-") Then
				retval = WriteHTML(Server.MapPath(AddBaseDir("valuta")) & "\c" & bName)
			End If

			' Kopi til NTB Direkte (INN, UTE, SPO)
			If (UCase(GetFieldValue("GR")) = "INN") Or (UCase(GetFieldValue("GR")) = "UTE") Or (UCase(GetFieldValue("GR")) = "SPO") Then
				retval = WriteHTML(Server.MapPath(AddBaseDir("direkte/feed")) & "\c" & bName)
			End If
		End If

		' Slett fra original katalog
		RemoveFile saveName

		' Slett evt. lock-fil
		' RemoveFile saveName & ".lck"

		retVal = True

	End If

	SendFile = retVal

End Function


'---------------------------------------------------------------------
'-- Function WriteHTML
'-- Lagrer en fil i HTML-format. Returnerer true hvis lagring gikk ok.
'-- Inn:	Full path til ny fil
'-- Ut:	True/False
'---------------------------------------------------------------------
Function WriteHTML(fullPath)
	Dim BrodTekst, Bilder, urlPath, nameParts, editCaption, saveDir, i, strSign

	' Oppretter evt. katalog
	i = InStrRev(fullPath, "\")
	If i > 0 Then
		saveDir = Left(fullPath, i)
		If Not fso.FolderExists(saveDir) Then
			fso.CreateFolder(saveDir)
		End If
	End If

	nameParts = Split(fullPath, "\")
	' Lag path
	' Finn avdeling + tre siste hvis kopi
	If LCase(nameParts(UBound(nameParts) - 2)) = "kopier" Then
		urlPath = nameParts(UBound(nameParts) - 5) & "/" & nameParts(UBound(nameParts) - 2) & "/" _
				& nameParts(UBound(nameParts) - 1) & "/" 	& nameParts(UBound(nameParts))

	' Hold-katalog
	ElseIf (nameParts(UBound(nameParts) - 1) = "hold") Then
		urlPath = nameParts(UBound(nameParts) - 1) & "/" & nameParts(UBound(nameParts))

	' Plukk ut de tre siste ellers
	Else
		urlPath = nameParts(UBound(nameParts) - 2) & "/" & nameParts(UBound(nameParts) - 1) & "/" _
				& nameParts(UBound(nameParts))
	End If

	urlPath = gAspDir & "/rediger.asp?filename=" & AddBaseDir(urlPath)

	' Sett editCaption
	If LCase(nameParts(UBound(nameParts) - 2)) = "sendt" Then
		editCaption = "Ny versjon"
		urlPath = urlPath & "&nyver=1"
	Else
		editCaption = "Rediger"
	End If

	' Create and Write to a File
	On Error Resume Next
	Set fs = fso.CreateTextFile(fullPath, ForWriting)

	If err = 0 Then
		fs.WriteLine "<HTML>"
		fs.WriteLine "<HEAD>"
		fs.WriteLine "<SCRIPT>"
		fs.WriteLine "<!--"
		fs.WriteLine "function RedigerMld(url) {"
		fs.writeline "var pos, hoyde;"
		fs.writeline "hoyde = screen.height - 150;"
		fs.writeline "pos = 'toolbar=1,location=0,directories=0,status=1,menubar=1,scrollbars=1,width=750,height=' + hoyde + ',top=0,left=40';"
		fs.WriteLine "window.open(url,'_blank',pos);"
		fs.WriteLine "}"
		fs.WriteLine "// -->"
		fs.WriteLine "</SCRIPT>"
		fs.WriteLine "<TITLE>" & GetFieldValue("ST") & "</TITLE>"
		fs.WriteLine "</HEAD>"
		fs.WriteLine "<BODY bgcolor='#ffffff'>"
		fs.WriteLine "<TABLE bgcolor='#ffeedd'>"
		fs.WriteLine "<TR>"
		fs.WriteLine "<TD><B>GR: </B><!--pre-GR-->" & UCase(GetFieldValue("GR")) & "<!--post-GR--></TD>"
		fs.WriteLine "<TD><B>UG: </B><!--pre-UG-->" & UCase(GetFieldValue("UG")) & "<!--post-UG--></TD>"
		fs.WriteLine "<TD><B>PR: </B><!--pre-PR-->" & GetFieldValue("PR") & "<!--post-PR--></TD>"
		fs.WriteLine "<TD><B>KA: </B><!--pre-KA-->" & GetFieldValue("KA") & "<!--post-KA--></TD>"
		fs.WriteLine "<TD><B>DI: </B><!--pre-DI-->" & UCase(GetFieldValue("DI")) & "<!--post-DI--></TD>"
		fs.WriteLine "</TR><TR>"
		fs.WriteLine "<TD colspan='2'><B>ST: </B><!--pre-ST-->" & GetFieldValue("ST") & "<!--post-ST--></TD>"
		fs.WriteLine "<TD colspan='2'><B>SI: </B><!--pre-SI-->" & GetFieldValue("SI") & "<!--post-SI--></TD>"
		fs.WriteLine "<TD><B>Tid: </B><!--pre-TI-->" & GetFieldValue("TI") & "<!--post-TI--></TD>"
		fs.WriteLine "</TR><TR>"
		fs.WriteLine "<TD colspan='4'><B>Til desk: </B><FONT color=red><!--pre-TilDesk-->" & GetFieldValue("TilDesk") & "<!--post-TilDesk--></FONT></TD>"
		fs.WriteLine "<TD><B>Ticker: </B><!--pre-Ticker-->" & GetFieldValue("Ticker") & "<!--post-Ticker--></TD>"
		fs.WriteLine "</TR>"
		fs.WriteLine "</TABLE>"
		'''fs.WriteLine "<FORM method='POST' action='/fretex/asp/rediger.asp' target='_blank'>"
		fs.WriteLine "<FORM onSubmit=" & Chr(34) & "javascript:RedigerMld('" & urlPath & "')" & Chr(34) & ">"
		fs.WriteLine "<INPUT type='submit' name='Go' value='" & editCaption & "'>"
		fs.WriteLine "<INPUT type='hidden' name='filename' value='" & urlPath & "'>"
		fs.WriteLine "</FORM>"

		BrodTekst = Trim(GetFieldValue("BrodTekst"))
		If BrodTekst <> "" Then
			i = InStr(1, BrodTekst, vbCrLf & "++")
			If i > 0 Then
				strSign = Right(BrodTekst, Len(BrodTekst) - i)
				BrodTekst = Left(BrodTekst, i-1)
			End If
			BrodTekst = Replace(BrodTekst, vbCrLf & vbCrLf, "</P>" & "<P>")
			BrodTekst = Replace(BrodTekst, vbCrLf & "    ", "<BR>&nbsp;&nbsp;&nbsp;")
			BrodTekst = Replace(BrodTekst, vbCrLf & "   ", "<BR>&nbsp;&nbsp;&nbsp;")
			BrodTekst = Replace(BrodTekst, vbCrLf & "  ", "<BR>&nbsp;&nbsp;&nbsp;")
			BrodTekst = Replace(BrodTekst, vbCrLf & " ", "<BR>&nbsp;&nbsp;&nbsp;")
			BrodTekst = Replace(BrodTekst, vbCrLf, "<BR>")
		End If

		Bilder = Trim(GetFieldValue("Bilder"))
		If Bilder <> "" Then
			Bilder = Replace(Bilder, vbCrLf & vbCrLf, "</P>" & "<P>")
			Bilder = Replace(Bilder, vbCrLf & " ", "<BR>&nbsp;&nbsp;&nbsp;")
			Bilder = Replace(Bilder, vbCrLf, "<BR>")
			'--If Left(Bilder, 10) <> "Bildetekst" Then
			'--	Bilder = "Bildetekst(er):<BR>" & Bilder
			'--End If
		End If

		fs.WriteLine "<H2><!--pre-Tittel-->" & Trim(GetFieldValue("Tittel")) & "<!--post-Tittel--></H2>"
		fs.WriteLine "<P><I><!--pre-TilRed-->" & Trim(GetFieldValue("TilRed")) & "<!--post-TilRed--></I><P>"
		fs.WriteLine "<P><I><!--pre-Byline-->" & Trim(GetFieldValue("Byline")) & "<!--post-Byline--></I><P>"
		fs.WriteLine "<P><B><!--pre-Ingress-->" & Trim(GetFieldValue("Ingress")) & "<!--post-Ingress--></B><P>"
		fs.WriteLine "<P><!--pre-BrodTekst-->" & BrodTekst & "<!--post-BrodTekst--></P>"
		fs.WriteLine "<P><I><!--pre-Bilder-->" & Bilder & "<!--post-Bilder--></I></P>"
		fs.WriteLine "<P><I><!--pre-Sign-->" & strSign & "<!--post-Sign--></I></P>"

		fs.WriteLine "</BODY>" & vbCrLf & "</HTML>" & vbCrLf

		fs.Close
		Set fs = Nothing
		WriteHTML = True

	Else
		WriteHTML = False
	End If

	' Ny funksjon som lager NITF-fil for NTB Portalen
	WriteNITF(fullPath)

	' Ny NITF til portalen
	WriteNyNITF(fullPath)

	' Kun for debug info
	' WriteDEBUG(fullPath)

End Function


'---------------------------------------------------------------------
'-- Function WriteDEBUG kun for debuging
'---------------------------------------------------------------------
Function WriteDEBUG(fullPath)
	Dim BrodTekst, Bilder, urlPath, nameParts, editCaption, saveDir, i, strSign, Ingress, Tittel, TilRed

	nameParts = Split(fullPath, "\")
	' Lag path
	' Finn avdeling + tre siste hvis kopi
	dim dtDato, nitfDato
	dtDato = GetFieldValue("TI")
	nitfDato = mid(dtDato, 7, 4) & mid(dtDato, 4, 2) & mid(dtDato, 1, 2) & "T" & mid(dtDato, 12, 2) & mid(dtDato, 15, 2) & mid(dtDato, 18, 2)

	If LCase(nameParts(UBound(nameParts)-1)) <> "ut" Then
		exit function
	end if

	fullPath = replace(lcase(fullPath), "\ut\", "\DEBUG\")

	' Oppretter evt. katalog
	i = InStrRev(fullPath, "\")
	If i > 0 Then
		saveDir = Left(fullPath, i)
		If Not fso.FolderExists(saveDir) Then
			fso.CreateFolder(saveDir)
		End If
	End If

	On Error Resume Next
	Set fs = fso.CreateTextFile(fullPath & "txt", ForWriting)

	fs.WriteLine GetFieldValue("BrodTekst")

	fs.Close
	Set fs = Nothing
	WriteNITF = True

End Function


'---------------------------------------------------------------------
'-- Function WriteNITF
'-- Ny funksjon som lager NITF XML-fil for NTB Portalen.
'-- Av Roar Vestre, NTB, 04.09.2002
'-- Lagrer en fil i NITF XML-format i folder NITF. Returnerer true hvis lagring gikk ok.
'-- Inn: Full path til ny fil
'-- Ut:	True/False
'---------------------------------------------------------------------
Function WriteNITF(fullPath)
	Dim BrodTekst, Bilder, urlPath, nameParts, editCaption, saveDir, i, strSign, Ingress, Tittel, TilRed

	nameParts = Split(fullPath, "\")
	'Lag path
	'Finn avdeling + tre siste hvis kopi
	dim dtDato, nitfDato
	dtDato = GetFieldValue("TI")
	nitfDato = mid(dtDato, 7, 4) & mid(dtDato, 4, 2) & mid(dtDato, 1, 2) & "T" & mid(dtDato, 12, 2) & mid(dtDato, 15, 2) & mid(dtDato, 18, 2)

	If LCase(nameParts(UBound(nameParts)-1)) <> "ut" Then
		exit function
	end if

	fullPath = replace(lcase(fullPath), "\ut\", "\NITF\")

	'Oppretter evt. katalog
	i = InStrRev(fullPath, "\")
	If i > 0 Then
		saveDir = Left(fullPath, i)
		If Not fso.FolderExists(saveDir) Then
			fso.CreateFolder(saveDir)
		End If
	End If

	' Create and Write to a File
	On Error Resume Next
	Set fs = fso.CreateTextFile(fullPath & "xml", ForWriting)
	'Set fs = fso.CreateTextFile(fullPath & "html", ForWriting)

	If err = 0 Then
		strSign = GetFieldValue("SI")
		fs.WriteLine "<?xml version=""1.0"" encoding=""iso-8859-1"" standalone=""yes""?>"
		fs.WriteLine "<nitf version=""-//IPTC-NAA//DTD NITF-XML 2.5//EN"" change.date=""9 august 2000"" change.time=""1900"" baselang=""no-NO"">"
		fs.WriteLine "<head>"
		fs.WriteLine "<title>" & GetFieldValue("ST") & "</title>"

		fs.WriteLine "<meta name=""ntb-folder"" content=""Ut-Satellitt"" />"
		fs.WriteLine "<meta name=""ntb-date"" content=""" & mid(dtDato, 1, 16) & """ />"
		fs.WriteLine "<meta name=""ntb-distribusjonsKode"" content=""" & UCase(GetFieldValue("DI")) & """ />"
		fs.WriteLine "<meta name=""ntb-kanal"" content=""" & GetFieldValue("KA") & """ />"
		fs.WriteLine "<meta name=""ntb-meldingsSign"" content=""" & strSign & """ />"
		fs.WriteLine "<meta name=""ntb-meldingsType"" content=""Fra Fretex"" />"
		'fs.WriteLine "<meta name=""ntb-bilderAntall"" content=""***0"" />"
		'fs.WriteLine "<meta name=""ntb-id"" content=""***RED020722_094658_uv"" />"
		'fs.WriteLine "<meta name=""ntb-meldingsVersjon"" content=""***0"" />"
		fs.WriteLine "<docdata>"
		fs.WriteLine "<evloc county-dist=""""></evloc>"

		dim strID
		strID = "FRX" & nitfDato & "_" & strSign
		fs.WriteLine "<doc-id regsrc=""NTB"" id-string=""" & strID & """ />"
		fs.WriteLine "<urgency ed-urg=""" & GetFieldValue("PR") & """ />"
		fs.WriteLine "<date.issue norm=""" & mid(nitfDato, 1, 8) & """ />"

		TilRed = Trim(GetFieldValue("TilRed"))
		if TilRed <> "" then
			TilRed = Replace(TilRed, "&", "&amp;")
			TilRed = Replace(TilRed, "<", "&lt;")
			TilRed = Replace(TilRed, """", "&quot;")
			TilRed = Replace(TilRed, vbCrLf, " ")
		end if
		fs.WriteLine "<ed-msg info=""" & TilRed & """ />"

		fs.WriteLine "<du-key version=""1"" key=""" & GetFieldValue("ST") & """ />"
		fs.WriteLine "<doc.copyright year=""" & mid(nitfDato, 1, 4) & """ holder=""NTB"" />"
		fs.WriteLine "<key-list>"
		fs.WriteLine "<keyword key=""" & GetFieldValue("ST") & """ />"
		fs.WriteLine "</key-list>"
		fs.WriteLine "</docdata>"

		dim intLength
		intLength = len(GetFieldValue("Tittel")) + len(GetFieldValue("Ingress")) + len(GetFieldValue("BrodTekst"))
		fs.WriteLine "<pubdata date.publication=""" & nitfDato & """ item-length=""" & intLength & """ unit-of-measure=""character"" />"
		fs.WriteLine "<revision-history name=""" & strSign & """ />"

		fs.WriteLine "<tobject tobject.type=""" & Change2NitfGroup("GR", UCase(GetFieldValue("GR"))) & """>"
		fs.WriteLine "<tobject.property tobject.property.type=""" & Change2NitfGroup("UG", GetFieldValue("UG")) & """ />"
		If UCase(GetFieldValue("GR")) = "SPO" Then
			fs.WriteLine "<tobject.subject tobject.subject.refnum=""15000000"" tobject.subject.code=""SPO"" tobject.subject.type=""Sport"" tobject.subject.matter="""" />"
		End If
		fs.WriteLine "</tobject>"

		fs.WriteLine "</head>"

		fs.WriteLine "<body>"
		fs.WriteLine "<body.head>"
		fs.WriteLine "<distributor>NTB</distributor>"
		fs.WriteLine "<hedline>"

		Tittel = Trim(GetFieldValue("Tittel"))
		Tittel = Replace(Tittel, "&", "&amp;")
		Tittel = Replace(Tittel, "<", "&lt;")

		Tittel = Replace(Tittel, Chr(148), """")
		Tittel = Replace(Tittel, Chr(150), "-")

		fs.WriteLine "<hl1>" & Tittel & "</hl1>"

		fs.WriteLine "</hedline>"
		fs.WriteLine "<byline>"
		fs.WriteLine "<p>" & Trim(GetFieldValue("Byline")) & "</p>"
		fs.WriteLine "</byline>"
		fs.WriteLine "</body.head>"
		fs.WriteLine "<body.content>"

		Ingress = Trim(GetFieldValue("Ingress"))
		if Ingress <> "" then
			Ingress = Replace(Ingress, "&", "&amp;")
			Ingress = Replace(Ingress, "<", "&lt;")
			Ingress = Replace(Ingress, vbCrLf, " ")
			Ingress = Replace(Ingress, Chr(148), """")
			Ingress = Replace(Ingress, Chr(150), "-")
			Ingress = Replace(Ingress, Chr(151), "--")
			fs.WriteLine "<p lede=""true"">" & Ingress & "</p>"
		end if

		' Ny Kode for br�dtekst:
		BrodTekst = Trim(GetFieldValue("BrodTekst"))
		If BrodTekst <> "" Then
			BrodTekst = Replace(BrodTekst, "<P>", "")
			BrodTekst = Replace(BrodTekst, "</P>", vbCrLf)
			BrodTekst = Replace(BrodTekst, "<BR>", vbCrLf)
			BrodTekst = Replace(BrodTekst, "&nbsp;", vbCrLf)

			BrodTekst = Replace(BrodTekst, "&", "&amp;")
			BrodTekst = Replace(BrodTekst, "<", "&lt;")
			BrodTekst = Replace(BrodTekst, Chr(148), """")
			BrodTekst = Replace(BrodTekst, Chr(150), "-")
			BrodTekst = Replace(BrodTekst, Chr(151), "--")

			Dim arrBrodtekst
			Dim blTableStarted, blMellomtittel
			Dim arrLinje
			Dim strCol
			Dim strType

			arrBrodtekst = split(BrodTekst, vbCrLf)
			dim strLinje, strForrigeLinje
			strForrigeLinje = "tom"
			for i = 0 to ubound(arrBrodtekst)
				strLinje = trim(arrBrodtekst(i))

				If 	blTableStarted Then
					'Is inside a table
					If left(strLinje, 4) = "{et}" _
						Or Trim(strLinje) = "{bi}" _
						Or left(strLinje, 8) = "{et}{tn}" _
						Or left(strLinje, 12) = "{bi}{et}{tn}" _
						Or trim(strLinje) = "" then
						'Tabell slutt
						fs.WriteLine "</table>"
						blTableStarted = False
					ElseIf 	left(strLinje, 6) = "[tp04]" _
						Or left(strLinje, 9) = "{sl}_{tl}" _
						Or right(strLinje, 4) = "{et}" _
						Then
						' Skip Line
					ElseIf left(strLinje, 1) = "[" _
						Or left(strLinje, 4) = "{bi}" Then
						fs.WriteLine "</table>"
						blTableStarted = False
						i = i -1
					Else
						strLinje = replace(strLinje, "_{", "-{")
						strLinje = replace(strLinje, " _", " - ")

						strLinje = replace(strLinje, "{bt}", "")
						strLinje = replace(strLinje, "{sl}", "")
						strLinje = replace(strLinje, "{sp}", "")
						strLinje = replace(strLinje, "{et}{tn}", "")

						strLinje = replace(strLinje, "{tr}{tn}", "{r}")
						strLinje = replace(strLinje, "{tl}{tn}", "{l}")
						strLinje = replace(strLinje, "{tc}{tn}", "{c}")

						strLinje = replace(strLinje, "{tl}", "{l}<td/>")
						strLinje = replace(strLinje, "{tr}", "{r}<td/>")
						strLinje = replace(strLinje, "{tc}", "{c}<td/>")

						arrLinje = split(strLinje, "<td/>")

						fs.Write "<tr>"

						For each strCol in arrLinje
							strType = Right(strCol, 3)
							strCol = Left(strCol, len(strCol) - 3)
							Select Case strType
							case "{l}"
								fs.Write "<td>" & Trim(strCol) & "</td>"
							case "{r}"
								fs.Write "<td align='right'>" & Trim(strCol) & "</td>"
							case "{c}"
								fs.Write "<td align='center'>" & Trim(strCol) & "</td>"
							case else
								fs.Write "<td>" & Trim(strCol) & "</td>"
							End Select
						Next

						fs.WriteLine "</tr>"

					End If
				Else
					'Is inside normal paragraphs
					strLinje = replace(strLinje, "{et}", "")
					strLinje = replace(strLinje, "{bt}", "")
					strLinje = replace(strLinje, "{tn}", "")
					if left(strLinje, 4) = "{sl}" OR left(strLinje, 8) = "{bt}{sl}" then
						'Tabell linjer
						fs.WriteLine "<table border='1'>"
						blTableStarted = True
						i = i - 1
					ElseIf left(strLinje, 6) = "[tp00]" then
						blMellomtittel = true
						fs.WriteLine strLinje
					ElseIf blMellomtittel then
						' Mellomtittel
						fs.WriteLine "<hl2>" & strLinje & "</hl2>"
						blMellomtittel = False

					ElseIf left(arrBrodtekst(i), 1) = " " then
						' Avsnitt med innrykk
						fs.WriteLine "<p innrykk=""true"">" & strLinje & "</p>"
					elseif left(strLinje, 4) = "{bi}" then
						' Mellomtittel
						fs.WriteLine "<hl2>" & Mid(strLinje, 5) & "</hl2>"
					elseif left(strLinje, 2) = "++" then
						' Signatur
						strSign = lcase(mid(strLinje, 3))
						strSign = replace(strSign, "/", "")
						strSign = replace(strSign, " ", ".")
						strSign = replace(strSign, "�", "a")
						strSign = replace(strSign, "�", "o")
						strSign = replace(strSign, "�", "a")
					'elseif strForrigeLinje = "" and (len(strLinje) < 40) and strLinje <> "" then
					'	' Mellomtittel
					'	fs.WriteLine "<hl2>" & strLinje & "</hl2>"
					elseif strLinje = "" then
						' Tomt asvnitt Avsnitt
						fs.WriteLine "<p/>"
					'elseif strForrigeLinje = "" and strLinje <> "" then
					'	' Avsnitt med innrykk
					'	fs.WriteLine "<p innrykk=""true"">" & strLinje & "</p>"
					elseif strLinje <> "" then
						' Avsnitt
						fs.WriteLine "<p>" & strLinje & "</p>"
					else
						' Tomme linjer droppes
					end if

					strForrigeLinje = strLinje
				End If
			next
			If blTableStarted then
				fs.WriteLine "</table>"
				blTableStarted = False
			end if
		end if

		dim strMediaStart, strMediaSlutt
		If Bilder <> "" Then
			Bilder = Trim(GetFieldValue("Bilder"))
			Bilder = Replace(Bilder, "&", "&amp;")
			Bilder = Replace(Bilder, "<", "&lt;")

			strMediaStart = "<media media-type=""image"">" & vbCrLf & "<media-reference mime-type=""image/jpeg"" source="""" />" & vbCrLf & "<media-caption>"
			strMediaSlutt = "</media-caption>" & vbCrLf & "</media>" & vbCrLf
			Bilder = Replace(Bilder, vbCrLf & vbCrLf, strMediaSlutt & strMediaStart)
			'Bilder = Replace(Bilder, vbCrLf & " ", "<br/>")
			'Bilder = Replace(Bilder, vbCrLf, "<br/>")
			'--If Left(Bilder, 10) <> "Bildetekst" Then
			'--	Bilder = "Bildetekst(er):<BR>" & Bilder
			'--End If

			fs.WriteLine strMediaStart & Bilder & strMediaSlutt
		End If

		fs.WriteLine "</body.content>"
		fs.WriteLine "<body.end>"
		fs.WriteLine "<tagline>"
		if strSign <> "" then
			fs.WriteLine "<a href=""mailto:" & strSign & "@ntb.no"">" & strSign & "@ntb.no</a>"
		end if

		fs.WriteLine "</tagline>"
		fs.WriteLine "</body.end>"

		fs.WriteLine "</body>"
		fs.WriteLine "</nitf>"

		fs.Close
		Set fs = Nothing
		WriteNITF = True

	Else
		WriteNITF = False
	End If

End Function


'---------------------------------------------------------------------
'-- Function WriteNyNITF
'-- Ny funksjon som lager nytt  NITF XML format
'-- Av LCH (kokt fra Roar Vestre) NTB, 17.10.2003
'-- Lagrer en fil i NITF XML-format i folder NITF-NY. Returnerer true hvis lagring gikk ok.
'-- Inn: Full path til ny fil
'-- Ut:	True/False
'---------------------------------------------------------------------
Function WriteNyNITF(fullPath)
	Dim BrodTekst, Bilder, urlPath, nameParts, editCaption, saveDir, i, strSign, Ingress, Tittel, TilRed

	nameParts = Split(fullPath, "\")
	'Lag path
	'Finn avdeling + tre siste hvis kopi

	dim dtDato, nitfDato, timestamp, timenorm
	dtDato = GetFieldValue("TI")
	nitfDato = mid(dtDato, 7, 4) & mid(dtDato, 4, 2) & mid(dtDato, 1, 2) & "T" & mid(dtDato, 12, 2) & mid(dtDato, 15, 2) & mid(dtDato, 18, 2)
	timestamp = mid(dtDato, 7, 4) & "." & mid(dtDato, 4, 2) & "." & mid(dtDato, 1, 2) & " " & mid(dtDato, 12, 2) & ":" & mid(dtDato, 15, 2) & ":" & mid(dtDato, 18, 2)
	timenorm = mid(dtDato, 7, 4) & "-" & mid(dtDato, 4, 2) & "-" & mid(dtDato, 1, 2) & "T" & mid(dtDato, 12, 2) & ":" & mid(dtDato, 15, 2) & ":" & mid(dtDato, 18, 2)

	If LCase(nameParts(UBound(nameParts)-1)) <> "ut" Then
		exit function
	end if

	fullPath = replace(lcase(fullPath), "\ut\", "\NITF-NY\")

	'Oppretter evt. katalog
	i = InStrRev(fullPath, "\")
	If i > 0 Then
		saveDir = Left(fullPath, i)
		If Not fso.FolderExists(saveDir) Then
			fso.CreateFolder(saveDir)
		End If
	End If

	' Create and Write to a File
	On Error Resume Next
	Set fs = fso.CreateTextFile(fullPath & "xml", ForWriting)
	'Set fs = fso.CreateTextFile(fullPath & "html", ForWriting)

	If err = 0 Then
		strSign = ConvertCharsToXMLAttribute(GetFieldValue("SI"))

		fs.WriteLine "<?xml version=""1.0"" encoding=""iso-8859-1"" standalone=""yes""?>"
		fs.WriteLine "<nitf version=""-//IPTC//DTD NITF 3.2//EN"" change.date=""October 10, 2003"" change.time=""19:30"" baselang=""no-NO"">"
		fs.WriteLine "<head>"

		Tittel = ConvertCharsToXMLEntity(GetFieldValue("Tittel"))

		fs.WriteLine "<title>" & Tittel & "</title>"
		fs.WriteLine "<meta name=""timestamp"" content=""" & timestamp & """ />"
		fs.WriteLine "<meta name=""foldername"" content=""Ut-Satellitt"" />"
		fs.WriteLine "<meta name=""NTBDistribusjonsKode"" content=""" & UCase(GetFieldValue("DI")) & """ />"
		fs.WriteLine "<meta name=""NTBKanal"" content=""" & GetFieldValue("KA") & """ />"
		fs.WriteLine "<meta name=""NTBPrioritet"" content=""" & GetFieldValue("PR") & """ />"
		fs.WriteLine "<meta name=""NTBStikkord"" content=""" & ConvertCharsToXMLAttribute(GetFieldValue("ST")) & """ />"
		fs.WriteLine "<meta name=""NTBMeldingsSign"" content=""" & strSign & """ />"
		fs.WriteLine "<meta name=""NTBMeldingsType"" content=""Fra Fretex"" />"

		fs.WriteLine "<meta name=""ntb-dato"" content=""" & mid(dtDato, 1, 16) & """ />"

		'fs.WriteLine "<meta name=""ntb-bilderAntall"" content=""***0"" />"
		'fs.WriteLine "<meta name=""ntb-id"" content=""***RED020722_094658_uv"" />"
		'fs.WriteLine "<meta name=""ntb-meldingsVersjon"" content=""***0"" />"

		fs.WriteLine "<tobject tobject.type=""" & Change2NitfGroup("GR", UCase(GetFieldValue("GR"))) & """>"
		fs.WriteLine "<tobject.property tobject.property.type=""" & Change2NitfGroup("UG", GetFieldValue("UG")) & """ />"
		If UCase(GetFieldValue("GR")) = "SPO" Then
			fs.WriteLine "<tobject.subject tobject.subject.refnum=""15000000"" tobject.subject.code=""SPO"" tobject.subject.type=""Sport"" tobject.subject.matter="""" />"
		End If
		fs.WriteLine "</tobject>"

		fs.WriteLine "<docdata>"

		'fs.WriteLine "<evloc county-dist=""""/>"

		dim strID
		strID = "FRX" & nitfDato & "_" & strSign
		fs.WriteLine "<doc-id regsrc=""NTB"" id-string=""" & strID & """ />"
		fs.WriteLine "<urgency ed-urg=""" & GetFieldValue("PR") & """ />"
		fs.WriteLine "<date.issue norm=""" & timenorm & """ />"

		TilRed = ConvertCharsToXMLAttribute(GetFieldValue("TilRed"))
		fs.WriteLine "<ed-msg info=""" & TilRed & """ />"

		fs.WriteLine "<du-key version=""1"" key=""" & ConvertCharsToXMLAttribute(GetFieldValue("ST")) & """ />"
		fs.WriteLine "<doc.copyright year=""" & mid(nitfDato, 1, 4) & """ holder=""NTB"" />"
		fs.WriteLine "<key-list>"
		fs.WriteLine "<keyword key=""" & ConvertCharsToXMLAttribute(GetFieldValue("ST")) & """ />"
		fs.WriteLine "</key-list>"
		fs.WriteLine "</docdata>"

		dim intLength
		intLength = len(GetFieldValue("Tittel")) + len(GetFieldValue("Ingress")) + len(GetFieldValue("BrodTekst"))
		fs.WriteLine "<pubdata date.publication=""" & nitfDato & """ item-length=""" & intLength & """ unit-of-measure=""character"" />"
		fs.WriteLine "<revision-history name=""" & strSign & """ />"

		fs.WriteLine "</head>"

		fs.WriteLine "<body>"
		fs.WriteLine "<body.head>"
		fs.WriteLine "<hedline>"

		fs.WriteLine "<hl1>" & Tittel & "</hl1>"

		fs.WriteLine "</hedline>"
		fs.WriteLine "<byline>"
		fs.WriteLine "<p>" & ConvertCharsToXMLEntity(GetFieldValue("Byline")) & "</p>"
		fs.WriteLine "</byline>"
		fs.WriteLine "<distributor><org>NTB</org></distributor>"
		fs.WriteLine "</body.head>"
		fs.WriteLine "<body.content>"

		Ingress = ConvertCharsToXMLEntity(GetFieldValue("Ingress"))
		if Ingress <> "" then
			fs.WriteLine "<p lede=""true"">" & Ingress & "</p>"
		end if

		' Ny Kode for br�dtekst:
		BrodTekst = ConvertCharsToXMLEntity(GetFieldValue("BrodTekst"))
		If BrodTekst <> "" Then
			BrodTekst = Replace(BrodTekst, "<P>", "")
			BrodTekst = Replace(BrodTekst, "</P>", vbCrLf)
			BrodTekst = Replace(BrodTekst, "<BR>", vbCrLf)
			BrodTekst = Replace(BrodTekst, "&nbsp;", vbCrLf)

			Dim arrBrodtekst
			Dim blTableStarted, blMellomtittel
			Dim arrLinje
			Dim strCol
			Dim strType

			arrBrodtekst = split(BrodTekst, vbCrLf)
			dim strLinje, strForrigeLinje
			strForrigeLinje = "tom"
			for i = 0 to ubound(arrBrodtekst)
				strLinje = trim(arrBrodtekst(i))

				If 	blTableStarted Then
					'Is inside a table
					If left(strLinje, 4) = "{et}" _
						Or Trim(strLinje) = "{bi}" _
						Or left(strLinje, 8) = "{et}{tn}" _
						Or left(strLinje, 12) = "{bi}{et}{tn}" _
						Or trim(strLinje) = "" then
						'Tabell slutt
						fs.WriteLine "</table>"
						blTableStarted = False
					ElseIf 	left(strLinje, 6) = "[tp04]" _
						Or left(strLinje, 9) = "{sl}_{tl}" _
						Or right(strLinje, 4) = "{et}" _
						Then
						' Skip Line
					ElseIf left(strLinje, 1) = "[" _
						Or left(strLinje, 4) = "{bi}" Then
						fs.WriteLine "</table>"
						blTableStarted = False
						i = i -1
					Else
						strLinje = replace(strLinje, "_{", "-{")
						strLinje = replace(strLinje, " _", " - ")

						strLinje = replace(strLinje, "{bt}", "")
						strLinje = replace(strLinje, "{sl}", "")
						strLinje = replace(strLinje, "{sp}", "")
						strLinje = replace(strLinje, "{et}{tn}", "")

						strLinje = replace(strLinje, "{tr}{tn}", "{r}")
						strLinje = replace(strLinje, "{tl}{tn}", "{l}")
						strLinje = replace(strLinje, "{tc}{tn}", "{c}")

						strLinje = replace(strLinje, "{tl}", "{l}<td/>")
						strLinje = replace(strLinje, "{tr}", "{r}<td/>")
						strLinje = replace(strLinje, "{tc}", "{c}<td/>")

						arrLinje = split(strLinje, "<td/>")

						fs.Write "<tr>"

						For each strCol in arrLinje
							strType = Right(strCol, 3)
							strCol = Left(strCol, len(strCol) - 3)
							Select Case strType
							case "{l}"
								fs.Write "<td>" & Trim(strCol) & "</td>"
							case "{r}"
								fs.Write "<td align='right'>" & Trim(strCol) & "</td>"
							case "{c}"
								fs.Write "<td align='center'>" & Trim(strCol) & "</td>"
							case else
								fs.Write "<td>" & Trim(strCol) & "</td>"
							End Select
						Next

						fs.WriteLine "</tr>"

					End If
				Else
					'Is inside normal paragraphs
					strLinje = replace(strLinje, "{et}", "")
					strLinje = replace(strLinje, "{bt}", "")
					strLinje = replace(strLinje, "{tn}", "")
					if left(strLinje, 4) = "{sl}" OR left(strLinje, 8) = "{bt}{sl}" then
						'Tabell linjer
						fs.WriteLine "<table border='1'>"
						blTableStarted = True
						i = i - 1
					ElseIf left(strLinje, 6) = "[tp00]" then
						blMellomtittel = true
						fs.WriteLine strLinje
					ElseIf blMellomtittel then
						' Mellomtittel
						fs.WriteLine "<hl2>" & strLinje & "</hl2>"
						blMellomtittel = False

					ElseIf left(arrBrodtekst(i), 1) = " " then
						' Avsnitt med innrykk
						fs.WriteLine "<p innrykk=""true"">" & strLinje & "</p>"
					elseif left(strLinje, 4) = "{bi}" then
						' Mellomtittel
						fs.WriteLine "<hl2>" & Mid(strLinje, 5) & "</hl2>"
					elseif left(strLinje, 2) = "++" then
						' Signatur
						strSign = lcase(mid(strLinje, 3))
						strSign = replace(strSign, "/", "")
						strSign = replace(strSign, " ", ".")
						strSign = replace(strSign, "�", "a")
						strSign = replace(strSign, "�", "o")
						strSign = replace(strSign, "�", "a")
					'elseif strForrigeLinje = "" and (len(strLinje) < 40) and strLinje <> "" then
					'	' Mellomtittel
					'	fs.WriteLine "<hl2>" & strLinje & "</hl2>"
					elseif strLinje = "" then
						' Tomt asvnitt Avsnitt
						fs.WriteLine "<p/>"
					'elseif strForrigeLinje = "" and strLinje <> "" then
					'	' Avsnitt med innrykk
					'	fs.WriteLine "<p innrykk=""true"">" & strLinje & "</p>"
					elseif strLinje <> "" then
						' Avsnitt
						fs.WriteLine "<p>" & strLinje & "</p>"
					else
						' Tomme linjer droppes
					end if

					strForrigeLinje = strLinje
				End If
			next
			If blTableStarted then
				fs.WriteLine "</table>"
				blTableStarted = False
			end if
		end if

		dim strMediaStart, strMediaSlutt

		Bilder = ConvertCharsToXMLEntity(GetFieldValue("Bilder"))
		If Bilder <> "" Then

			strMediaStart = "<media media-type=""image"">" & vbCrLf & "<media-reference mime-type=""image/jpeg"" source="""" />" & vbCrLf & "<media-caption>"
			strMediaSlutt = "</media-caption>" & vbCrLf & "</media>" & vbCrLf
			Bilder = Replace(Bilder, vbCrLf & vbCrLf, strMediaSlutt & strMediaStart)

			'Bilder = Replace(Bilder, vbCrLf & " ", "<br/>")
			'Bilder = Replace(Bilder, vbCrLf, "<br/>")
			'--If Left(Bilder, 10) <> "Bildetekst" Then
			'--	Bilder = "Bildetekst(er):<BR>" & Bilder
			'--End If

			fs.WriteLine strMediaStart & Bilder & strMediaSlutt
		End If

		fs.WriteLine "</body.content>"
		fs.WriteLine "<body.end>"
		fs.WriteLine "<tagline>"
		if strSign <> "" then
			fs.WriteLine "<a href=""mailto:" & strSign & "@ntb.no"">" & strSign & "@ntb.no</a>"
		end if

		fs.WriteLine "</tagline>"
		fs.WriteLine "</body.end>"

		fs.WriteLine "</body>"
		fs.WriteLine "</nitf>"

		fs.Close
		Set fs = Nothing
		WriteNyNITF = True

	Else
		WriteNyNITF = False
	End If

End Function

Function ConvertCharsToXMLEntity (strInn)

	dim strOut
	strOut = Trim(strInn)

	if ( strOut <> "" ) then

		'Replace basic XML screwups
		strOut = Replace(strOut, "&", "&amp;")
		strOut = Replace(strOut, "<", "&lt;")
		strOut = Replace(strOut, ">", "&gt;")

		'Newlines
		strOut = Replace(strOut, vbCrLf, " ")

		'Encode Special chars
		for i = 128 to 159
			strOut = Replace(strOut, Chr(i), "&#" & AscW(Chr(i)) & ";")
		next

	end if

	ConvertCharsToXMLEntity = strOut
End Function

Function ConvertCharsToXMLAttribute (strInn)

	dim strOut
	strOut = Trim(strInn)

	if ( strOut <> "" ) then
		'Replace basic XML screwups
		strOut = Replace(strOut, "&", "&amp;")
		strOut = Replace(strOut, "<", "&lt;")
		strOut = Replace(strOut, ">", "&gt;")

		'Newlines
		strOut = Replace(strOut, vbCrLf, " ")

		'Replace quotes
		strOut = Replace(strOut, """", "&quot;")
		strOut = Replace(strOut, "'", "&apos;")

		'Encode Special chars
		for i = 128 to 159
			strOut = Replace(strOut, Chr(i), "&#" & AscW(Chr(i)) & ";")
		next
	end if

	ConvertCharsToXMLAttribute = strOut
End Function


Function Change2NitfGroup(strType, strGroup)
	strGroup = UCase(strGroup)

	select Case UCASE(strType)
	case "GR"
		select Case strGroup
		case "INN"
			strGroup = "Innenriks"
		case "UTE"
			strGroup = "Utenriks"
		case "SPO"
			strGroup = "Sport"
		case "PRI"
			strGroup = "Priv-til-red"
		case "INT"
			strGroup = "Formidlingstjenester"
		case "ART"
			strGroup = "Kultur og underholdning"
		case "PRM"
			strGroup = "Formidlingstjenester"
		case "RTV"
			strGroup = "Formidlingstjenester"
		case "NRK"
			strGroup = "Formidlingstjenester"
		case "FEA"
			strGroup = "Feature"
		case "�KO"
			strGroup = "Formidlingstjenester"
		case else
			strGroup = "Innenriks"
		end select

	case "UG"
		select Case strGroup
		case "NNN"
			strGroup = "Nyheter"
		case "BAK"
			strGroup = "Bakgrunn"
		case "TAB"
			strGroup = "Tabeller og resultater"
		case else
			strGroup = "Nyheter"
		end select

	end select


	Change2NitfGroup = strGroup

End Function

%>

