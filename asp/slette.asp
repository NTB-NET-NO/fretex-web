<%  
	'-------------------------------------------
	'katalog.asp
	'Lister stikkordet p� meldinger i en katalog
	'9/8 1999 (jet)
	'-------------------------------------------
%>

<!-- #include file="headers.asp" -->
<!-- #include file="functions.asp" -->

<%
	Dim folder, file, files, myInfo(), myFolder, fullPath, showFolder, i, j
	Dim preStr, postStr, myRef
''	Dim gAvdeling, gSign, gOrg
	Dim mytype

''	Set auth = Server.CreateObject("AUTHXOCX.AuthXOCXCtrl.1")
''	gSign = GetSign()
''	gAvdeling = GetAvdeling()
''	Set fso = CreateObject("Scripting.FileSystemObject")
	InitGlobals

	'
	If Request.ServerVariables("REQUEST_METHOD") = "POST" Then
		If Request.Form("slett") = "Slett" Then
			i = 0
			For Each file In Request.Form("filename")
				If DeleteFile(Server.MapPath(AddBaseDir(file))) Then
					i = i + 1
				Else
					j = j + 1
				End If
				Request.Form("filename")
			Next
		End If
		Response.Write "<H3>" & i & " filer slettet fra " & Request.Form("dir")
		If j > 0 Then
				Response.Write "<P>Det var " & j & " filer som var l�st (ikke slettet)."
		End If

	
	Else
		'S�rger for at oversikten alltid oppdateres
		Response.Expires = 0

		myFolder = Request.QueryString("dir")
		mytype = Request.QueryString("type")
		'--locAddr = Request.ServerVariables("LOCAL_ADDR" )

		'Sjekker etter spesielle navn
		'Egen katalog
		If myFolder = "mydir" Then
			showFolder = gSign
			myFolder = "brukere/" & showFolder

		'Desk-katalog
		ElseIf myFolder = "mydesk" Then
			showFolder = gAvdeling
			myFolder = showFolder & "/desk"
		
		'Utsendte meldinger
		ElseIf myFolder = "kilde" Then
			showFolder = "sendt/" & gAvdeling
			myFolder = showFolder
		
		'Ellers pr�ver vi � vise angitt katalog
		Else
			showFolder = myFolder
		
		End If

		If myFolder <> "" Then
			myFolder = AddBaseDir(myFolder)   
%>
			<HTML>
			<HEAD>
				<TITLE>Katalog: <%=showFolder%></TITLE>
<SCRIPT language="javascript">
<!--
	function merkAlle() {
		var i, cnt, obj;
		cnt = document.theform.fileCount.value;
		for (i=0; i<cnt; i++) {
			document.theform.filename(i).checked = true;
		}
	}
//-->
</SCRIPT>
			</HEAD>
			<BODY bgcolor=#ffeedd>
				<H2>Katalog: <%=showFolder%></H2>
				Sett kryss ved filen(e) du �nsker � slette og trykk 'Slett'
				<FORM name="theform" target="data" action="slette.asp" method="POST">
				<INPUT type="Submit" name="slett" value="Slett">
				&nbsp;&nbsp;&nbsp;
				<INPUT type="button" onClick="merkAlle()" value="Merk alle">
				<INPUT type="hidden" size="1" name="dir" value="<%=myFolder%>">
<%
			fullPath = Server.MapPath(myFolder)

			If Not fso.FolderExists(fullPath) Then
				Response.Write "<FONT size=4><I>Finner ikke katalogen..</I></FONT>"
			Else
				Set folder = fso.GetFolder(fullPath)
				Set files = folder.Files

				i = files.Count
				ReDim myInfo(i, 4)

				For Each file in files

					'Overser l�se-filer
					If Right(file.Name, 4) <> ".lck" Then

						myRef = "[uten stikkord]"
						myInfo(i, 0) = file.DateLastModified
						myInfo(i, 1) = file.Name

						Set fs = fso.OpenTextFile(file, ForReading)
						myLine = fs.ReadAll
						fs.Close
		
						preStr = "<!--pre-ST-->"
						postStr = "<!--post-ST-->"
						If myType = "tekstarkiv" Then
							preStr = "<!--pre-SO-->"
							postStr = "<!--post-SO-->"
						End If
						myStart = InStr(1, myLine, preStr)
						If myStart > 0 Then
							myEnd = InStr(1, myLine, postStr)
							If myEnd > myStart + Len(preStr) Then
								myRef = Mid(myLine, myStart + Len(preStr), myEnd - myStart - Len(PreStr))
							End If
						End If
						myInfo(i, 2) = myRef

						preStr = "<!--pre-SI-->"
						postStr = "<!--post-SI-->"
						myStart = InStr(1, myLine, preStr)
						If myStart > 0 Then
							myEnd = InStr(1, myLine, postStr)
							If myEnd > 0 Then
								myInfo(i, 3) = Mid(myLine, myStart + Len(preStr), myEnd - myStart - Len(PreStr))
							End If
						End If


						If myType = "tekstarkiv" Then
							preStr = "<!--pre-UT-->"
							postStr = "<!--post-UT-->"
							myStart = InStr(1, myLine, preStr)
							If myStart > 0 Then
								myEnd = InStr(1, myLine, postStr)
								If myEnd > myStart + Len(preStr) Then
									arkivtid = Mid(myLine, myStart + Len(preStr), myEnd - myStart - Len(PreStr))
								End If
							End If
							preStr = "<!--pre-DA-->"
							postStr = "<!--post-DA-->"
							myStart = InStr(1, myLine, preStr)
							If myStart > 0 Then
								myEnd = InStr(1, myLine, postStr)
								If myEnd > myStart + Len(preStr) Then
									arkivdato = Mid(myLine, myStart + Len(preStr), myEnd - myStart - Len(PreStr))
								End If
							End If
							myinfo(i, 0) = CDate(arkivdato & " " & arkivtid)

						End If
								
					End If

					i = i - 1
				Next

				'La inn sortering 14/1-00 (jet)
				If files.Count > 0 Then
					ReDim sorted(files.count)
					sorted(1) = 1
				End If
	
				For i = 2 To files.Count
					For j = i To 2 Step -1
						If (myinfo(sorted(j-1), 0) < myinfo(i, 0)) Then
							sorted(j) = sorted(j-1)
							sorted(j-1) = i
						Else
							sorted(j) = i
							Exit For
						End If
					Next
				Next

				myCnt = 0
				For j = 1 To files.count
					i = sorted(j)			
					If myInfo(i, 2) <> "" Then
						dato = FormatDateTime(myInfo(i, 0), vbShortDate)
				
						If dato <> LastDate Then
							Response.Write "<H4>" & dato & "</H4>" & vbCrLf
							LastDate = dato
						End If

						Response.Write "<INPUT type='checkbox' name='filename' value='" & myFolder & "/" & myInfo(i, 1) & "'>"
						Response.Write FormatDateTime(myInfo(i, 0), vbShortTime)
						Response.Write "<A href='" & myFolder & "/" & myInfo(i, 1) & "' target='data'>" & vbCrLf
						Response.Write myInfo(i, 2) & "</A> (" & myInfo(i, 3) & ")</P>" & vbCrLf
						myCnt = myCnt + 1
					End If			
	
				Next
%>
				<INPUT type="hidden" name="fileCount" value="<%=myCnt%>">
				</FORM>
				</BODY>
				</HTML>
<%
			End If 'FolderExists
        
		End If' myFolder <> ""
		
	End If 'Method = post
%>

    
