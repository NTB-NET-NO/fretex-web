<% 
'---- sok.asp ----
'---- S�k i Fretex ----
	
Dim str, mydir, depth

If Request.ServerVariables("REQUEST_METHOD") = "POST" Then
		
	str = Request.Form("srch4")
	mydir = Request.Form("dir")

	If mydir = "" Then 
		mydir = "/fretex/data"	
		depth = "deep"
	Else
		depth = "shallow"
	End If

	msg = "S�k etter <I>'" & str & "'</I>:"

	'Opprett et s�keobjekt
	set Q = Server.CreateObject("IXSSO.Query")
	set util = Server.CreateObject("ixsso.Util")

	'Set s�kevaribale
	FormScope = mydir
	Q.query = str

	Q.SortBy = "write[d]"
	Q.Columns = "DocTitle, vpath, filename, write"
	Q.MaxRecords = 300

	util.AddScopeToQuery Q, FormScope, depth

	'Utf�r s�ket
	set RS = Q.CreateRecordSet("sequential")

	Response.Expires = 0

%>
<HTML>
<HEAD>
<TITLE>S�k i Fretex</TITLE>
</HEAD>
<BODY bgcolor="#ffeedd" alink="green">
<%
   If msg <> "" Then
      Response.Write "<P>" & msg & "</P>"
   End If

   i = 0
   do while not RS.EOF
      i = i + 1
      thisdate = formatdatetime(DateAdd("h", 1, RS("write")), vbLongDate)
      if thisdate <> mydate then
	 mydate = thisdate
         response.write "<H3>" & thisdate & "</H3>"
      end if

      response.write "<p>" & formatdatetime(DateAdd("h", 1, RS("write")), vbShortTime)
		
      if VarType(RS("DocTitle")) = 1 or RS("DocTitle") = "" then %>
         <a href="<%=RS("vpath")%>" target="data"><%= Server.HTMLEncode( RS("filename") )%></a>
      <% else %>
         <a href="<%=RS("vpath")%>" target="data"><%= Server.HTMLEncode(RS("DocTitle"))%></a>
      <% end if %>
<% 
      RS.MoveNext 
   Loop 
%>
<P><I><%=i%> dokumenter</I></P>
<%
   RS.close
   Set RS = Nothing
   Set Q = Nothing
   set Session("Query") = Nothing
   set Session("RecordSet") = Nothing

End If
%>
</BODY>
</HTML>
