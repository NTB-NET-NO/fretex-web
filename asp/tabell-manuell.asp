<!-- #include file="headers.asp" -->
<!-- #include file="functions.asp" -->
<!-- #include file="writeform.asp" -->
<!-- #include file="formjscript.asp" -->

<%
'------------------------------------------------------------
' tabellmanuell.asp
' Manuelle tabeller (ikke vsport)
' Desember 1999 (jet)
'------------------------------------------------------------

Response.Expires = 0

''Set fso = Server.CreateObject("Scripting.FileSystemObject")
''Set auth = Server.CreateObject("AUTHXOCX.AuthXOCXCtrl.1")
''gSign = GetSign()
''gAvdeling = GetAvdeling()
InitGlobals

If Request.ServerVariables("REQUEST_METHOD") = "GET" then
	mldType = Request.QueryString("type")

	'Sett headere
	SetDefaultHeaders

	'Lag tabell
	Select Case LCase(mldType)

	'-- Fotball --
	Case "fotball"
		SetFieldValue "Tittel", "Infolinje for fotball"
		SetFieldValue "TilDesk", "Husk at bredden inne i tabellen m� bevares!"

		brodTekst = ""
		brodTekst = brodTekst & "[t01]{tn}" & vbCrLf
		brodTekst = brodTekst & "[t01u1]{tn}" & vbCrLf
		brodTekst = brodTekst & "Tabelloverskrift fotball" & vbCrLf
		brodTekst = brodTekst & "[t01u2]{tn}" & vbCrLf
		brodTekst = brodTekst & "{sl}LAG1 - LAG2{sp}{tl} 0{tr}-{tc} 0{tr}{tn}" & vbCrLf
		brodTekst = brodTekst & "[t01u3]{tn}" & vbCrLf
		brodTekst = brodTekst & "Her kommer eventuell tekst for utsatte kamper osv. Hvis ikke i bruk, stryk [t01u3]." & vbCrLf
		brodTekst = brodTekst & "[t01u5]{tn}" & vbCrLf
		For i = 1 To 12
			brodTekst = brodTekst & "{sl}               {sp}{tl}  0{tr}  0{tr}  0{tr}  0{tr}    0{tr}-{tc}    0{tr}  0{tr}{tn}" & vbCrLf
		Next
		brodTekst = brodTekst & "{et}" & vbCrLf & vbCrLf

	'-- H�ndball --
	Case "handball"
		SetFieldValue "Tittel", "Infolinje for h�ndball"
		SetFieldValue "TilDesk", "Husk at bredden inne i tabellen m� bevares!"

		brodTekst = ""
		brodTekst = brodTekst & "[t01]{tn}" & vbCrLf
		brodTekst = brodTekst & "[t01u1]{tn}" & vbCrLf
		brodTekst = brodTekst & "Tabelloverskrift h�ndball" & vbCrLf
		brodTekst = brodTekst & "[t01u2]{tn}" & vbCrLf
		brodTekst = brodTekst & "{sl}LAG1 - LAG2{sp}{tl} 0{tr}-{tc} 0{tr}{tn}" & vbCrLf
		brodTekst = brodTekst & "[t01u3]{tn}" & vbCrLf
		brodTekst = brodTekst & "Her kommer eventuell tekst for utsatte kamper osv. Hvis ikke i bruk, stryk [t01u3]." & vbCrLf
		brodTekst = brodTekst & "[t01u5]{tn}" & vbCrLf
		For i = 1 To 12
			brodTekst = brodTekst & "{sl}               {sp}{tl}  0{tr}  0{tr}  0{tr}  0{tr}    0{tr}-{tc}    0{tr}  0{tr}{tn}" & vbCrLf
		Next
		brodTekst = brodTekst & "{et}" & vbCrLf & vbCrLf

	'-- Volleyball --
	Case "volleyball"
		SetFieldValue "Tittel", "Infolinje for volleyball"
		SetFieldValue "TilDesk", "Husk at bredden inne i tabellen m� bevares!"

		brodTekst = ""
		brodTekst = brodTekst & "[t01]{tn}" & vbCrLf
		brodTekst = brodTekst & "[t01u1]{tn}" & vbCrLf
		brodTekst = brodTekst & "Tabelloverskrift volleyball" & vbCrLf
		brodTekst = brodTekst & "[t01u7]{tn}" & vbCrLf
		brodTekst = brodTekst & "{sl}LAG1 - LAG2{sp}{tl} 0{tr}-{tc} 0{tr}{tn}" & vbCrLf
		brodTekst = brodTekst & "{sl}{sp}{tl}(0-0, 0-0, 0-0){tr}{tn}" & vbCrLf
		brodTekst = brodTekst & "[t01u3]{tn}" & vbCrLf
		brodTekst = brodTekst & "Her kommer eventuell tekst for utsatte kamper osv. Hvis ikke i bruk, stryk [t01u3]." & vbCrLf
		brodTekst = brodTekst & "[t01u8]{tn}" & vbCrLf
		For i = 1 To 8
			brodTekst = brodTekst & "{sl}               {sp}{tl}  0{tr}  0{tr}  0{tr}  0{tr}    0{tr}-{tc}    0{tr}  0{tr}{tn}" & vbCrLf
		Next
		brodTekst = brodTekst & "{et}" & vbCrLf & vbCrLf

	'-- Ishockey --
	Case "ishockey"
		SetFieldValue "Tittel", "Infolinje for ishockey"
		SetFieldValue "TilDesk", "Husk at bredden inne i tabellen m� bevares!"

		brodTekst = ""
		brodTekst = brodTekst & "[t01]{tn}" & vbCrLf
		brodTekst = brodTekst & "[t01u1]{tn}" & vbCrLf
		brodTekst = brodTekst & "Tabelloverskrift ishockey" & vbCrLf
		brodTekst = brodTekst & "[t01u2]{tn}" & vbCrLf
		brodTekst = brodTekst & "{sl}LAG1 - LAG2{sp}{tl} 0{tr}-{tc} 0{tr}{tn}" & vbCrLf
		brodTekst = brodTekst & "[t01u3]{tn}" & vbCrLf
		brodTekst = brodTekst & "Her kommer eventuell tekst for utsatte kamper osv. Hvis ikke i bruk, stryk [t01u3]." & vbCrLf
		brodTekst = brodTekst & "[t01u6]{tn}" & vbCrLf
		For i = 1 To 8
			brodTekst = brodTekst & "{sl}               {sp}{tl}  0{tr}  0{tr}  0{tr}  0{tr}    0{tr}-{tc}    0{tr}  0{tr}{tn}" & vbCrLf
		Next
		brodTekst = brodTekst & "{et}" & vbCrLf & vbCrLf

	'-- Bandy --
	Case "bandy"
		SetFieldValue "Tittel", "Infolinje for bandy"
		SetFieldValue "TilDesk", "Husk at bredden inne i tabellen m� bevares!"

		brodTekst = ""
		brodTekst = brodTekst & "[t01]{tn}" & vbCrLf
		brodTekst = brodTekst & "[t01u1]{tn}" & vbCrLf
		brodTekst = brodTekst & "Tabelloverskrift bandy" & vbCrLf
		brodTekst = brodTekst & "[t01u2]{tn}" & vbCrLf
		brodTekst = brodTekst & "{sl}LAG1 - LAG2{sp}{tl} 0{tr}-{tc} 0{tr}{tn}" & vbCrLf
		brodTekst = brodTekst & "[t01u3]{tn}" & vbCrLf
		brodTekst = brodTekst & "Her kommer eventuell tekst for utsatte kamper osv. Hvis ikke i bruk, stryk [t01u3]." & vbCrLf
		brodTekst = brodTekst & "[t01u6]{tn}" & vbCrLf
		For i = 1 To 8
			brodTekst = brodTekst & "{sl}               {sp}{tl}  0{tr}  0{tr}  0{tr}  0{tr}    0{tr}-{tc}    0{tr}  0{tr}{tn}" & vbCrLf
		Next
		brodTekst = brodTekst & "{et}" & vbCrLf & vbCrLf

	'-- Basket --
	Case "basket"
		SetFieldValue "Tittel", "Infolinje for basket"
		SetFieldValue "TilDesk", "Husk at bredden inne i tabellen m� bevares!"

		brodTekst = ""
		brodTekst = brodTekst & "[t01]{tn}" & vbCrLf
		brodTekst = brodTekst & "[t01u1]{tn}" & vbCrLf
		brodTekst = brodTekst & "Tabelloverskrift basketball" & vbCrLf
		brodTekst = brodTekst & "[t01u10]{tn}" & vbCrLf
		brodTekst = brodTekst & "{sl}LAG1 - LAG2{sp}{tl} 0{tr}-{tc} 0{tr}{tn}" & vbCrLf
		brodTekst = brodTekst & "[t01u3]{tn}" & vbCrLf
		brodTekst = brodTekst & "Her kommer eventuell tekst for utsatte kamper osv. Hvis ikke i bruk, stryk [t01u3]." & vbCrLf
		brodTekst = brodTekst & "[t01u11]{tn}" & vbCrLf
		For i = 1 To 8
			brodTekst = brodTekst & "{sl}               {sp}{tl}   0{tr}   0{tr}   0{tr}     0{tr}-{tc}     0{tr}  0{tr}{tn}" & vbCrLf
		Next
		brodTekst = brodTekst & "{et}" & vbCrLf & vbCrLf

	'-- Medaljeoversikter --
	Case "medaljer"
		SetFieldValue "Tittel", "Medaljeoversikt XXX"
		SetFieldValue "TilDesk", "Husk at bredden inne i tabellen m� bevares!"
		SetFieldValue "Ingress", "Sted (NTB): Medaljeoversikt for XXX etter AA av BB �velser:"

		brodTekst = ""
		brodTekst = brodTekst & "[med]{tn}" & vbCrLf
		brodTekst = brodTekst & "[med1]{tn}" & vbCrLf
		brodTekst = brodTekst & "{sl}                    Gull{tr}   S�lv{tr} Bronse{tr}{tn}" & vbCrLf
		brodTekst = brodTekst & "[xmed]{tn}" & vbCrLf
		brodTekst = brodTekst & "[med2]{tn}" & vbCrLf
		brodTekst = brodTekst & "{sl}Land           {sp}{tl}      0{tr}      0{tr}      0{tr}{tn}" & vbCrLf
		brodTekst = brodTekst & "{et}" & vbCrLf & vbCrLf

	End Select

	SetFieldValue "BrodTekst", brodTekst

	'Gi meldingen et navn
	gFileName = gBaseDir & "/brukere/" & gSign & "/" & GetUniqueName

%>
<HTML>
<HEAD>
   <STYLE TYPE="text/css">
        <!--
          A { text-decoration: none;  }  
          A:visited, A:hover, A:active { text-decoration: none; } 
        // -->
    </STYLE>
    <TITLE>Fretex: [uten stikkord]</TITLE>
</HEAD>
<BODY bgcolor="#ffeedd" alink="navy" vlink="navy" link="navy">
<%
	WriteFormJScript
	WriteForm
End If
%>
</BODY>
</HTML>


