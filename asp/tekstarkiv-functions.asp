<%

'---------------------------------------------------------------------
'-- Function GetFieldValue
'-- Returnerer verdien p� et felt i formen.
'-- Inn:	Feltnavn
'-- Ut:	Verdi
'---------------------------------------------------------------------
Function ArkivGetFieldValue(fldName)

	Dim i, fld
       
	fldName = LCase(fldName)
	i = 0
	For Each fld in gArkivFelt
	   	If LCase(fld) = fldName Then
       			ArkivGetFieldValue = gArkivVerdi(i)
			Exit For
		End If
		
		i = i + 1        
	Next

End Function


'---------------------------------------------------------------------
'-- Sub ArkivSetFieldValue
'-- Setter verdien p� et felt fra formen.
'-- Inn:	Feltnavn, Verdi
'-- Ut:	n/a
'---------------------------------------------------------------------
Sub ArkivSetFieldValue(fldName, value)

	Dim i, fld

	fldName = LCase(fldName)
	i = 0
	For Each fld in gArkivFelt
	
		If LCase(fld) = fldName Then
			gArkivVerdi(i) = value
			Exit For
		End If

		i = i + 1        
	Next

End Sub

'---------------------------------------------------------------------
'-- Sub ArkivParseFile
'-- Leser headerfelt fra en arkivmelding.
'-- Inn: filnavn
'-- Ut:	n/a
'---------------------------------------------------------------------
Sub ArkivParseFile(filename)
	Dim myText, fld, myVal, strBef, strAft, myStart, myEnd

	Set fs = fso.OpenTextFile(fileName, ForReading)
	myText = fs.ReadAll

	For Each fld in gArkivFelt

		myVal = ""
		strBef = "<!--pre-" & fld & "-->"
		strAft = "<!--post-" & fld & "-->"

		myStart = InStr(1, myText, strBef)
		myEnd = InStr(1, myText, strAft)

		If (myStart > 0 And myEnd > 0) Then
			myVal = Mid(MyText, myStart + Len(strBef), myEnd - myStart - Len(StrBef))
		End If

		ArkivSetFieldValue fld, myVal

	Next
	fs.Close
End Sub

'---------------------------------------------------------------------
'-- Sub ArkivWriteForm
'-- Leser headerfelt fra en arkivmelding.
'-- Inn: filnavn p� neste melding i k�en
'-- Ut:	n/a
'---------------------------------------------------------------------
Sub ArkivWriteForm(fileName)
	Dim i, myVal, myStr, ent
	
%>
<HTML>
<HEAD>
<TITLE>Fretex - <%=ArkivGetFieldValue("SO")%></TITLE>
</HEAD>
<BODY bgcolor=#ffeedd>
<FORM name="arkform" method="POST" action="tekstarkiv.asp">
<TABLE>
<TR>
	<TD><FONT color="navy">GE:</FONT>&nbsp;</TD>
	<TD><INPUT TYPE='text' NAME='GE' size='50' value='<%=ArkivGetFieldValue("GE")%>'></TD>
</TR>
<TR>
	<TD><FONT color="navy">ES:</FONT>&nbsp;</TD>
	<TD><INPUT TYPE='text' NAME='ES' size='50' value='<%=ArkivGetFieldValue("ES")%>'></TD>
</TR>
<TR>
	<TD><FONT color="navy">EO:</FONT>&nbsp;</TD>
	<TD><INPUT TYPE='text' NAME='EO' size='50' value='<%=ArkivGetFieldValue("EO")%>'></TD>
</TR>
<TR>
	<TD><FONT color="navy">SA:</FONT>&nbsp;</TD>
	<TD><INPUT TYPE='text' NAME='SA' size='50' value='<%=ArkivGetFieldValue("SA")%>'></TD>
</TR>
<TR>
	<TD><FONT color="navy">RE:</FONT>&nbsp;</TD>
	<TD><INPUT TYPE='text' NAME='RE' size='50' value='<%=ArkivGetFieldValue("RE")%>'></TD>
</TR>
<TR>
	<TD>&nbsp;</TD>
	<TD>
		<INPUT value='Send (og hent neste)' name='Go' type='Submit' action='<%=gAspDir%>/tekstarkiv.asp' method='POST'>
		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		<INPUT value='Slett (og hent neste)' name='Go' type='Submit' action='<%=gAspDir%>/tekstarkiv.asp' method='POST'>
		<INPUT type='hidden' name='action' value='sendme'>
		<INPUT type='hidden' name='filename' value='<%=fileName%>'>
		<INPUT type='hidden' name='next' value='<%=ArkivGetFieldValue("next")%>'>
	</TD>
</TR>
</TABLE>
<HR>
<TABLE>
<TR>
	<TD><FONT color="navy">NR:</FONT></TD>
	<TD><INPUT TYPE='text' NAME='NR' size='12' value='<%=ArkivGetFieldValue("NR")%>'></TD>

	<TD><FONT color="navy">DA:</FONT>&nbsp;</TD>
	<TD><INPUT TYPE='text' NAME='DA' size='10' value='<%=ArkivGetFieldValue("DA")%>'></TD>

	<TD><FONT color="navy">UT:</FONT>&nbsp;</TD>
	<TD><INPUT TYPE='text' NAME='UT' size='5' value='<%=ArkivGetFieldValue("UT")%>'></TD>

	<TD><FONT color="navy">SG:</FONT>&nbsp;</TD>
	<TD><INPUT TYPE='text' NAME='SG' size='3' value='<%=ArkivGetFieldValue("SG")%>'></TD>

	<TD><FONT color="navy">SI:</FONT>&nbsp;</TD>
	<TD><INPUT TYPE='text' NAME='SI' size='15' value='<%=ArkivGetFieldValue("SI")%>'></TD>
</TR>
<TR>
	<TD><FONT color="navy">SO:</FONT>&nbsp;</TD>
	<TD colspan='10'><INPUT TYPE='text' NAME='SO' size='35' value='<%=ArkivGetFieldValue("SO")%>'></TD>
</TR>
<TR>
	<TD><FONT color="navy">TI:</FONT>&nbsp;</TD>
	<TD colspan='10'><INPUT TYPE='text' NAME='TI' size='65' value='<%=ArkivGetFieldValue("TI")%>'></TD>
</TR>
<%
	myVal = ArkivGetFieldValue("IN")
	myVal = Replace(myVal, "<BR>&nbsp;&nbsp;&nbsp; ", vbCrLf & "  ")
	myVal = Replace(myVal, "<BR>&nbsp;&nbsp;&nbsp;", vbCrLf & "  ")
	myVal = Replace(myVal, "<BR>", vbCrLf)
	myVal = Replace(myVal, "</P><P>", vbCrLf & vbCrLf)
%>
<TR>
	<TD><FONT color="navy">IN:</FONT>&nbsp;</TD>
	<TD colspan='10'><TEXTAREA name='IN' cols='65' rows='5'><%=myVal%></TEXTAREA></TD>
</TR>
<%
	myVal = ArkivGetFieldValue("TE")
	myVal = Replace(myVal, "<BR>&nbsp;&nbsp;&nbsp; ", vbCrLf & "  ")
	myVal = Replace(myVal, "<BR>&nbsp;&nbsp;&nbsp;", vbCrLf & "  ")
	myVal = Replace(myVal, "<BR>", vbCrLf)
	myVal = Replace(myVal, "</P><P>", vbCrLf & vbCrLf)
%>
<TR>
	<TD><FONT color="navy">TE:</FONT>&nbsp;</TD>
	<TD colspan='10'><TEXTAREA name='TE' cols='65' rows='25'><%=myVal%></TEXTAREA></TD>
</TR>
</TABLE>
</FORM>
<%
End Sub


'---------------------------------------------------------------------
'-- Sub SamleForIndeksering
'-- Samler meldinger i arkiv-katalogen.
'-- Inn: dato
'-- Ut:	n/a
'---------------------------------------------------------------------
Sub SamleForIndeksering(mydate)
	Dim fradir, tildir, longdate
	Dim i, j, totCnt, myCnt
	Dim folder, fFolders, oldFolder, files
	Dim myinfo(), sorted()

	fradir = Server.MapPath(gBaseDir & "/sendt")
	tildir = Server.MapPath(gBaseDir & "/tekstarkiv/indeksering")

	longDate = DatePart("yyyy", mydate)
	If DatePart("m", mydate) < 10 Then longDate = longDate & "0"
	longDate = longDate & DatePart("m", mydate)
	If DatePart("d", mydate) < 10 Then longDate = longDate & "0"
	longDate = longDate & DatePart("d", mydate)

	Set fraFolder = fso.GetFolder(fradir)
	Set fFolders = fraFolder.SubFolders

	totCnt = 0

	For Each folder in fFolders
		'Sjekk f�rst om meldingene er flyttet til undermappe
		oldFolder = folder & "\" & longDate
		If fso.FolderExists(oldFolder) Then
			Set folder = fso.GetFolder(oldFolder)
		End If

		Set files = folder.Files

		totCnt = totCnt + files.Count
		'- myinfo inneholder tid, navn og mappe
		ReDim Preserve myInfo(3, totCnt)

		i = totCnt			
		For Each file in files
			'Overser l�se-filer
			If Right(file.Name, 4) <> ".lck" Then
				myinfo(1, i) = file.name
				myinfo(2, i) = file.path
	
				myinfo(0, i) = file.datelastmodified

				'Sjekker om det er riktig dato
				If DateDiff("d", mydate, myInfo(0, i)) <> 0 Then
					myinfo(2, i) = ""
				End If
			End If

			i = i - 1
		Next
	Next

	If totCnt > 0 Then
		ReDim sorted(totCnt)
		sorted(1) = 1
	End If
	
	For i = 2 To totCnt
		For j = i To 2 Step -1
			If (myinfo(0, sorted(j-1)) < myinfo(0, i)) Then
				sorted(j) = sorted(j-1)
				sorted(j-1) = i
			Else
				sorted(j) = i
				Exit For
			End If
		Next
	Next

	response.write "<INPUT type='button' value='Skriv ut' onclick='window.print()'>"
	response.write "<H3>Meldinger klargjort for indeksering " & mydate & "</H3>" & vbCrLf
	response.write "<TABLE>" & vbCrLf

	myCnt = 0
	For j = totCnt To 1 Step -1
		i = sorted(j)
		If myinfo(2, i) <> "" Then
			myCnt = myCnt + 1
			ParseFile myinfo(2, i)
			If j > 1 Then
				ArkivSaveFile tildir & "\" & myinfo(1, i), myinfo(1, sorted(j-1))
			Else
				ArkivSaveFile tildir & "\" & myinfo(1, i), ""
			End If
			response.write "<TR>" & vbCrLf
			response.write "<TD>" & FormatDateTime(myinfo(0, i), vbShortTime) & vbCrLf
			response.write "<TD>" & GetFieldValue("ST") & vbCrLf
			response.write "</TR>" & vbCrLf
		End If
	Next
	response.write "</TABLE>" & vbCrLf
	response.write "<P><B>Totalt " & myCnt & " meldinger.</B></P>" & vbCrLf
End Sub


'---------------------------------------------------------------------
'-- Sub SamleFeatureMeldinger
'-- Samler featuremeldinger i arkiv-katalogen.
'-- Inn: n/a
'-- Ut:	n/a
'---------------------------------------------------------------------
Sub SamleFeatureMeldinger()
	Dim fradir, tildir, longdate
	Dim i, j, totCnt, myCnt
	Dim folder, fFolders, oldFolder, files
	Dim myinfo(), sorted()

	fradir = Server.MapPath(gBaseDir & "/feature/sendt")
	tildir = Server.MapPath(gBaseDir & "/tekstarkiv/indeksering")

	Set fraFolder = fso.GetFolder(fradir)
	''Set fFolders = fraFolder.SubFolders

	totCnt = 0

	''For Each folder in fFolders
		Set files = fraFolder.Files

		totCnt = totCnt + files.Count
		'- myinfo inneholder tid, navn og mappe
		ReDim Preserve myInfo(3, totCnt)

		i = totCnt			
		For Each file in files
	''response.write file.name
			'Overser l�se-filer
			If Right(file.Name, 4) <> ".lck" Then
				myinfo(1, i) = file.name
				myinfo(2, i) = file.path
				myinfo(0, i) = file.datelastmodified
			End If
			i = i - 1
		Next
	''Next

	If totCnt > 0 Then
		ReDim sorted(totCnt)
		sorted(1) = 1
	End If
	
	For i = 2 To totCnt
		For j = i To 2 Step -1
			If (myinfo(0, sorted(j-1)) < myinfo(0, i)) Then
				sorted(j) = sorted(j-1)
				sorted(j-1) = i
			Else
				sorted(j) = i
				Exit For
			End If
		Next
	Next

	response.write "<INPUT type='button' value='Skriv ut' onclick='window.print()'>"
	response.write "<H3>Featuremeldinger klargjort for indeksering</H3>" & vbCrLf
	response.write "<TABLE>" & vbCrLf

	myCnt = 0
	For j = totCnt To 1 Step -1
		i = sorted(j)
		If myinfo(2, i) <> "" Then
			myCnt = myCnt + 1
			ParseFile myinfo(2, i)
			SetFieldValue "TI", myinfo(0, i)
			If j > 1 Then
				ArkivSaveFile tildir & "\" & myinfo(1, i), myinfo(1, sorted(j-1))
			Else
				ArkivSaveFile tildir & "\" & myinfo(1, i), ""
			End If
			response.write "<TR>" & vbCrLf
			response.write "<TD>" & FormatDateTime(myinfo(0, i), vbShortTime) & vbCrLf
			response.write "<TD>" & GetFieldValue("ST") & vbCrLf
			response.write "</TR>" & vbCrLf
		End If
	Next
	response.write "</TABLE>" & vbCrLf
	response.write "<P><B>Totalt " & myCnt & " meldinger.</B></P>" & vbCrLf
End Sub


'---------------------------------------------------------------------
'-- Sub SendTilAftenposten
'-- Samler meldinger fra arkiv-katalogen og sender til Aftenposten.
'-- Inn: n/a
'-- Ut:	n/a
'---------------------------------------------------------------------
Sub SendTilAftenposten
	Dim fradir, tildir
	Dim i, j, totCnt, myCnt, mystr
	Dim folder, fFolders, oldFolder, files
	Dim myinfo(), sorted()

	fradir = Server.MapPath(gBaseDir & "/tekstarkiv/aftenposten")
	tildir = Server.MapPath(gBaseDir & "/tekstarkiv/2go")
	cpdir = Server.MapPath(gBaseDir & "/tekstarkiv/sendt")

	cpdir = cpdir & "/" & Year(Now())
	If Month(Now()) < 10 Then cpdir = cpdir & "0"
	cpdir = cpdir & Month(Now())
	If Day(Now()) < 10 Then cpdir = cpdir & "0"
	cpdir = cpdir & Day(Now())
	If Not fso.FolderExists(cpdir) Then fso.CreateFolder(cpdir)

	Set fraFolder = fso.GetFolder(fradir)
	Set files = fraFolder.Files

	i = files.Count
	ReDim myinfo(4, i)

	For Each file in files
		'Overser l�se-filer
		If Right(file.Name, 4) <> ".lck" Then
			myinfo(1, i) = file.name
			myinfo(2, i) = file.path

			Set fs = fso.OpenTextFile(file, ForReading)
			myLine = fs.ReadAll
			fs.Close

			preStr = "<!--pre-UT-->"
			postStr = "<!--post-UT-->"
			myStart = InStr(1, myLine, preStr)
			If myStart > 0 Then
				myEnd = InStr(1, myLine, postStr)
				If myEnd > myStart + Len(preStr) Then
					arkivtid = Mid(myLine, myStart + Len(preStr), myEnd - myStart - Len(PreStr))
				End If
			End If
			preStr = "<!--pre-DA-->"
			postStr = "<!--post-DA-->"
			myStart = InStr(1, myLine, preStr)
			If myStart > 0 Then
				myEnd = InStr(1, myLine, postStr)
				If myEnd > myStart + Len(preStr) Then
					arkivdato = Mid(myLine, myStart + Len(preStr), myEnd - myStart - Len(PreStr))
				End If
			End If
			myinfo(0, i) = CDate(arkivdato & " " & arkivtid)

			preStr = "<!--pre-SO-->"
			postStr = "<!--post-SO-->"
			myStart = InStr(1, myLine, preStr)
			If myStart > 0 Then
				myEnd = InStr(1, myLine, postStr)
				If myEnd > myStart + Len(preStr) Then
					myinfo(3, i) = Mid(myLine, myStart + Len(preStr), myEnd - myStart - Len(PreStr))
				End If
			End If
		End If
		i = i - 1
	Next

	If files.Count > 0 Then
		ReDim sorted(files.Count)
		sorted(1) = 1
	End If
	
	For i = 2 To files.Count
		For j = i To 2 Step -1
			If (myinfo(0, sorted(j-1)) < myinfo(0, i)) Then
				sorted(j) = sorted(j-1)
				sorted(j-1) = i
			Else
				sorted(j) = i
				Exit For
			End If
		Next
	Next

	response.write "<INPUT type='button' value='Skriv ut' onclick='window.print()'>"
	response.write "<H3>Meldinger sendt til Aftenposten " & Now() & "</H3>" & vbCrLf

	theDate = DatePart("yyyy", Now()) & "-" & DatePart("y", Now())

	theFile = tilDir & "\ntb.data-" & theDate & ".txt"
	logFile = tilDir & "\ntb.log-" & theDate & ".txt"
	i = 0
	Do While fso.FileExists(theFile)
		i = i + 1
		theFile = tilDir & "\ntb.data-" & theDate & "-" & i & ".txt"
		logFile = tilDir & "\ntb.log-" & theDate & "-" & i & ".txt"
	Loop

	Set fLog = fso.OpenTextFile(logFile, ForWriting, True)
	
	myCnt = 0
	For j = files.Count To 1 Step -1
		i = sorted(j)
		If myinfo(2, i) <> "" Then
			ArkivParseFile myinfo(2, i)
			WriteAftenposten theFile

			fso.CopyFile myinfo(2, i), cpdir & "\", True
			On Error Resume Next
			fso.DeleteFile myinfo(2, i)

			mydate = FormatDateTime(myinfo(0, i), vbShortDate)
			If mydate <> lastdate Then
				If lastdate <> "" Then
					response.write "</TABLE>"
					response.write "<B>Totalt " & myCnt & " meldinger den " & lastdate & ".</B>"
					response.write "<BR><HR><BR>"

					mystr = Right(DatePart("yyyy", lastdate), 2) & "-" 
					If DatePart("m", lastdate) < 10 Then mystr = mystr & "0"
					mystr = mystr & DatePart("m", lastdate)	& "-"
					If DatePart("d", lastdate) < 10 Then mystr = mystr & "0"
					mystr = mystr & DatePart("d", lastdate) 
					mystr = mystr & " " & myCnt
					fLog.WriteLine mystr
				End If
				lastdate = mydate
				mycnt = 0
				Response.Write "<B>" & lastdate & "<B><BR>"
				response.write "<TABLE>"
			End If
			response.write "<TR>" & vbCrLf
			response.write "<TD>" & FormatDateTime(myinfo(0, i), vbShortTime) & vbCrLf
			response.write "<TD>" & myinfo(3, i) & vbCrLf
			response.write "</TR>" & vbCrLf

			myCnt = myCnt + 1
		End If
	Next
	response.write "</TABLE>" & vbCrLf
	If myCnt > 0 Then
		response.write "<P><B>Totalt " & myCnt & " meldinger den " & lastdate & ".</B></P>" & vbCrLf
		
		mystr = Right(DatePart("yyyy", lastdate), 2) & "-" 
		If DatePart("m", lastdate) < 10 Then mystr = mystr & "0"
		mystr = mystr & DatePart("m", lastdate)	& "-"
		If DatePart("d", lastdate) < 10 Then mystr = mystr & "0"
		mystr = mystr & DatePart("d", lastdate) 
		mystr = mystr & " " & myCnt
		fLog.WriteLine mystr
	Else
		response.write "<P><B>Ingen meldinger overf�rt.</B></P>" & vbCrLf
	End If
	flog.close
	If myCnt < 1 Then
		fso.DeleteFile logFile
	End If
End Sub
%>
