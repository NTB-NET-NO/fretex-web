<%  
	'-------------------------------------------
	'repetisjon.asp
	'Lister meldinger for repetisjon til en 
	'enkelt satellitt-kunde
	'29/11 1999 (jet)
	'-------------------------------------------
%>

<!-- #include file="headers.asp" -->
<!-- #include file="functions.asp" -->
<!-- #include file="savefile.asp" -->

<%
	Dim folder, file, files, myInfo(), myFolder, fullPath, showFolder
	Dim preStr, postStr, myRef, myDate
	Dim sorted(), tmp
''	Dim gAvdeling
''	Dim gSign
''	Dim gOrg
	Dim kunde
	Dim sendtDir, utDir

	sendtDir = gBaseDir & "/tekstarkiv/sendt"
	utDir = gBaseDir & "/tekstarkiv/aftenposten"

''	Set auth = Server.CreateObject("AUTHXOCX.AuthXOCXCtrl.1")
''	Set fso = CreateObject("Scripting.FileSystemObject")

''	gSign = GetSign()
''	gAvdeling = GetAvdeling()

	InitGlobals

	'S�rger for at oversikten alltid oppdateres
	Response.Expires = 0

	If Request.ServerVariables("REQUEST_METHOD") = "POST" Then

		i = 0
		If Request.Form("send") = "Send" Then

			For Each file In Request.Form("filename")
				i = i + 1
				on error resume next
				fso.CopyFile Server.MapPath(file), Server.MapPath(utdir) & "\"
			Next
		
			response.write "<H3> " & i & " meldinger klargjort for overf�ring.</H3>"

		End If
		
	Else
%>
	<HTML>
	<HEAD>
		<TITLE>Repetisjoner</TITLE>
		<!-- generert <%=Now()%> -->
		<SCRIPT language="javascript">
		<!--
		function merkAlle() {
			var i, cnt, obj;
			cnt = document.theform.fileCount.value;
			for (i=0; i<cnt; i++) {
				document.theform.filename(i).checked = true;
			}
		}
		// -->
		</SCRIPT>
	</HEAD>
	<BODY bgcolor=#ffeedd>
<%
	myDate = Request.QueryString("dato")
	longdate = Year(mydate)
	If Month(mydate) < 10 Then longdate = longdate & "0"
	longdate = longdate & Month(mydate)
	If Day(mydate) < 10 Then longdate = longdate & "0"
	longdate = longdate & Day(mydate)
%>
	<H2>Repetisjon av arkivmeldinger (<%=longdate%>)</H2>
<%
	rootFolder = Server.MapPath(sendtDir & "\" & longdate)

	myFolder = Request.QueryString("dir")
	showFolder = myFolder
		
	If Not fso.FolderExists(rootFolder) Then
		Response.Write "<FONT size=4><I>Finner ikke katalogen..</I></FONT>"
	Else
%>
		Marker meldingene som skal repeteres og trykk 'Send'.<BR>
		Meldingene legges i 'Ferdig indeksert'-katalogen og overf�res n�r du velger dette fra menyen.
			<FORM name="theform" target="data" method="POST" action="tekstarkiv-repetisjon.asp?dir=<%=myFolder%>">
				<INPUT type="button" name="merkalle" value="Merk alle" onClick="merkAlle()">
				&nbsp;&nbsp;&nbsp;<input type="submit" value="Send" name="send">
<%
			Set rFolder = fso.GetFolder(rootFolder)
			Set files = rFolder.Files

			totCnt = 0
			i = files.Count
			ReDim myInfo(4, i)

			For Each file in files

				'Overser l�se-filer
				If Right(file.Name, 4) <> ".lck" Then

					myRef = file.Name
					'''myInfo(0, i) = file.DateLastModified
					myInfo(1, i) = file.Name

					Set fs = fso.OpenTextFile(file, ForReading)
					myLine = fs.ReadAll
					fs.Close

					preStr = "<!--pre-UT-->"
					postStr = "<!--post-UT-->"
					myStart = InStr(1, myLine, preStr)
					arkivtid = ""
					If myStart > 0 Then
						myEnd = InStr(1, myLine, postStr)
						If myEnd > myStart + Len(preStr) Then
							arkivtid = Mid(myLine, myStart + Len(preStr), myEnd - myStart - Len(PreStr))
						End If
					End If
					preStr = "<!--pre-DA-->"
					postStr = "<!--post-DA-->"
					myStart = InStr(1, myLine, preStr)
					If myStart > 0 Then
						myEnd = InStr(1, myLine, postStr)
						If myEnd > myStart + Len(preStr) Then
							arkivdato = Mid(myLine, myStart + Len(preStr), myEnd - myStart - Len(PreStr))
							arkivdato = Replace(arkivdato, "-", ".")
						End If
					End If

					on error resume next
					myinfo(0, i) = CDate(arkivdato & " " & arkivtid)
					''myinfo(0, i) = arkivdato & " " & arkivtid

					''preStr = "<!--pre-ST-->"
					''postStr = "<!--post-ST-->"
					preStr = "<!--pre-SO-->"
					postStr = "<!--post-SO-->"

					myref = "xxx"
					myStart = InStr(1, myLine, preStr)
					If myStart > 0 Then
						myEnd = InStr(1, myLine, postStr)
						If myEnd > 0 Then
							myRef = Mid(myLine, myStart + Len(preStr), myEnd - myStart - Len(PreStr))
						End If
					End If
					myInfo(2, i) = myRef

					preStr = "<!--pre-SI-->"
					postStr = "<!--post-SI-->"
					myStart = InStr(1, myLine, preStr)
					If myStart > 0 Then
						myEnd = InStr(1, myLine, postStr)
						If myEnd > 0 Then
							myInfo(3, i) = Mid(myLine, myStart + Len(preStr), myEnd - myStart - Len(PreStr))
						End If
					End If
					totCnt = totCnt + 1
				End If

				i = i - 1
			Next

			If totCnt > 0 Then
				ReDim sorted(totCnt)
				sorted(1) = 1
			End If
	
			For i = 2 To totCnt
				For j = i To 2 Step -1
					If DateDiff("n", myinfo(0, sorted(j-1)), myinfo(0, i)) > 0 Then
						sorted(j) = sorted(j-1)
						sorted(j-1) = i
					Else
						sorted(j) = i
						Exit For
					End If
				Next
			Next

			For j = 1 To totCnt
				i = sorted(j)
				If Not IsDate(myInfo(0, i)) Then myInfo(0, i) = "01.01.00 00:00"
				dato = FormatDateTime(myInfo(0, i), vbShortDate)
			
				If dato <> LastDate Then
					Response.Write "<H4>" & dato & "</H4>" & vbCrLf
					LastDate = dato
				End If
			
				Response.Write "<INPUT type='checkbox' name='filename' value='" & sendtDir & "/" & longdate & "/" & myInfo(1, i) & "'>"
				Response.Write FormatDateTime(myInfo(0, i), vbShortTime)
				Response.Write "<A href='" & sendtDir & "/" & longdate & "/" & myInfo(1, i) & "' target='data'>" & vbCrLf
				Response.Write myInfo(2, i) & "</A> (" & myInfo(3, i) & ")</P>" & vbCrLf

			Next
%>
			<INPUT type="hidden" name="fileCount" value="<%=totCnt%>">
<%
		End If 'FolderExists

	End If 'RequestMethod = POST
%>
</FORM>
</BODY>
</HTML>
    
