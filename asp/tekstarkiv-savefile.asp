<%
'---------------------------------------------------------------------
'tekstarkiv-savefile.asp
'Funksjoner som brukes til lagring av filer
'---------------------------------------------------------------------

'---------------------------------------------------------------------
'-- Function SaveFile
'-- Lagrer data til fil. Feltverdier m� v�re satt. 
'-- Inn:	fullt filnavn
'-- Ut:	True/False
'---------------------------------------------------------------------
Function ArkivSaveFile(saveName, nextMsg)

	SettFeltVerdier
	SaveFile = ArkivWriteHTML(saveName, nextMsg)	
	
End Function


'---------------------------------------------------------------------
'-- Function ArkivSendFile
'-- Lagrer data til fil. Feltverdier m� v�re satt. 
'-- Inn:	fullt filnavn
'-- Ut:	True/False
'---------------------------------------------------------------------
Function ArkivSendFile(fullPath)
	Dim BrodTekst, Bilder, urlPath, nameParts, editCaption, saveDir, i, strSign
	
	nameParts = Split(fullPath, "\")
	'Lag path
	urlPath = nameParts(UBound(nameParts) - 2) & "/" & nameParts(UBound(nameParts) - 1) & "/" _
			& nameParts(UBound(nameParts))
	
	urlPath = gAspDir & "/tekstarkiv.asp?action=indekser&filename=" & AddBaseDir(urlPath)
	urlPath = urlPath & "&next=" & nextMsg

	'Sett editCaption
	editCaption = "Indekser"
		
	' Create and Write to a File
	On Error Resume Next
	Set fs = fso.CreateTextFile(fullPath, ForWriting)
	
	If err = 0 Then
		fs.WriteLine "<HTML>"
		fs.WriteLine "<HEAD>"
		fs.WriteLine "<SCRIPT>"
		fs.WriteLine "<!--"
		fs.WriteLine "function RedigerMld(url) {"
		fs.WriteLine "window.open(url,'_blank', 'toolbar=1,location=0,directories=0,status=1,menubar=1,scrollbars=1,resizable=1,width=750,height=600,top=0,left=50');"
		fs.WriteLine "}"
		fs.WriteLine "// -->"
		fs.WriteLine "</SCRIPT>"
		fs.WriteLine "<TITLE>" & ArkivGetFieldValue("SO") & "</TITLE>"
		fs.WriteLine "</HEAD>"
		fs.WriteLine "<BODY bgcolor='#ffffff'>"
		fs.WriteLine "<TABLE bgcolor='#ffeedd'>"
		fs.WriteLine "<TR>"

		fs.WriteLine "<TD><B>NR: </B><!--pre-NR-->" & ArkivGetFieldValue("NR") & "<!--post-NR--></TD>"
		fs.WriteLine "<TD><B>DA: </B><!--pre-DA-->" & ArkivGetFieldValue("DA") & "<!--post-DA--></TD>"
		fs.WriteLine "<TD><B>UT: </B><!--pre-UT-->" & ArkivGetFieldValue("UT") & "<!--post-UT--></TD>"
		fs.WriteLine "<TD><B>SG: </B><!--pre-SG-->" & ArkivGetFieldValue("SG") & "<!--post-SG--></TD>"
		fs.WriteLine "<TD><B>SI: </B><!--pre-SI-->" & ArkivGetFieldValue("SI") & "<!--post-SI--></TD>"
		fs.WriteLine "</TR>"
		fs.WriteLine "<TR>"
		fs.WriteLine "<TD colspan='5'><B>SO: </B><!--pre-SO-->" & ArkivGetFieldValue("SO") & "<!--post-SO--></TD>"
		fs.WriteLine "</TR>"
		fs.WriteLine "<TR>"
		fs.WriteLine "<TD colspan='5'><B>GE: </B><!--pre-GE-->" & SjekkArkivFelt(ArkivGetFieldValue("GE")) & "<!--post-GE--></TD>"
		fs.WriteLine "</TR>"
		fs.WriteLine "<TR>"
		fs.WriteLine "<TD colspan='5'><B>ES: </B><!--pre-ES-->" & SjekkArkivFelt(ArkivGetFieldValue("ES")) & "<!--post-ES--></TD>"
		fs.WriteLine "</TR>"
		fs.WriteLine "<TR>"
		fs.WriteLine "<TD colspan='5'><B>EO: </B><!--pre-EO-->" & SjekkArkivFelt(ArkivGetFieldValue("EO")) & "<!--post-EO--></TD>"
		fs.WriteLine "</TR>"
		fs.WriteLine "<TR>"
		fs.WriteLine "<TD colspan='5'><B>SA: </B><!--pre-SA-->" & ArkivGetFieldValue("SA") & "<!--post-SA--></TD>"
		fs.WriteLine "</TR>"
		fs.WriteLine "<TR>"
		fs.WriteLine "<TD colspan='5'><B>RE: </B><!--pre-RE-->" & ArkivGetFieldValue("RE") & "<!--post-RE--></TD>"
		fs.WriteLine "</TR>"
		fs.WriteLine "</TABLE>"

		fs.WriteLine "<FORM onSubmit=" & chr(34) & "javascript:RedigerMld('" & urlPath & "')" & chr(34) & ">"
		fs.WriteLine "<INPUT type='submit' name='Go' value='" & editCaption & "'>"
		fs.WriteLine "<INPUT type='hidden' name='filename' value='" & urlPath & "'>"
		fs.WriteLine "<INPUT type='hidden' name='next' value='" & nextMsg & "'>"
		fs.WriteLine "</FORM>"

		fs.WriteLine "<H3><!--pre-TI-->" & ArkivGetFieldValue("TI") & "<!--post-TI--></H3>"

		myval = Trim(ArkivGetFieldValue("IN"))
		myval = Replace(myval, vbCrLf & vbCrLf, "</P>" & "<P>")
		myval = Replace(myval, vbCrLf & "    ", "<BR>&nbsp;&nbsp;&nbsp;")
		myval = Replace(myval, vbCrLf & "   ", "<BR>&nbsp;&nbsp;&nbsp;")
		myval = Replace(myval, vbCrLf & "  ", "<BR>&nbsp;&nbsp;&nbsp;")
		myval = Replace(myval, vbCrLf & " ", "<BR>&nbsp;&nbsp;&nbsp;")
		myval = Replace(myval, vbCrLf, "<BR>")
		fs.WriteLine "<P><B><!--pre-IN-->" & myval & "<!--post-IN--></B></P>"

		myval = "  " & Trim(ArkivGetFieldValue("TE"))
		myval = Replace(myval, vbCrLf & vbCrLf, "</P>" & "<P>")
		myval = Replace(myval, vbCrLf & "    ", "<BR>&nbsp;&nbsp;&nbsp;")
		myval = Replace(myval, vbCrLf & "   ", "<BR>&nbsp;&nbsp;&nbsp;")
		myval = Replace(myval, vbCrLf & "  ", "<BR>&nbsp;&nbsp;&nbsp;")
		myval = Replace(myval, vbCrLf & " ", "<BR>&nbsp;&nbsp;&nbsp;")
		myval = Replace(myval, vbCrLf, "<BR>")
		fs.WriteLine "<!--pre-TE-->" & myval & "<!--post-TE-->"

		fs.Close
		Set fs = Nothing
		WriteHTML = True
	
	Else
		WriteHTML = False
	End If	
End Function

'---------------------------------------------------------------------
'-- Function ArkivWriteHTML
'-- Lagrer en fil i HTML-format. Returnerer true hvis lagring gikk ok.
'-- Inn:	Full path til ny fil
'-- Ut:	True/False
'---------------------------------------------------------------------
Function ArkivWriteHTML(fullPath, nextMsg)
	Dim BrodTekst, Bilder, urlPath, nameParts, editCaption, saveDir, i, strSign
	Dim strTid
	
	'Oppretter evt. katalog
	i = InStrRev(fullPath, "\")
	If i > 0 Then
		saveDir = Left(fullPath, i)
		If Not fso.FolderExists(saveDir) Then
			fso.CreateFolder(saveDir)
		End If
	End If

	nameParts = Split(fullPath, "\")
	'Lag path
	'Finn avdeling + tre siste hvis kopi
	If LCase(nameParts(UBound(nameParts) - 2)) = "kopier" Then
		urlPath = nameParts(UBound(nameParts) - 5) & "/" & nameParts(UBound(nameParts) - 2) & "/" _
				& nameParts(UBound(nameParts) - 1) & "/" 	& nameParts(UBound(nameParts))

	'Hold-katalog
	ElseIf (nameParts(UBound(nameParts) - 1) = "hold") Then
		urlPath = nameParts(UBound(nameParts) - 1) & "/" & nameParts(UBound(nameParts))

	'Plukk ut de tre siste ellers
	Else
		urlPath = nameParts(UBound(nameParts) - 2) & "/" & nameParts(UBound(nameParts) - 1) & "/" _
				& nameParts(UBound(nameParts))
	End If
	
	urlPath = gAspDir & "/tekstarkiv.asp?action=indekser&filename=" & AddBaseDir(urlPath)
	urlPath = urlPath & "&next=" & nextMsg

	'Sett editCaption
	editCaption = "Indekser"
		
	' Create and Write to a File
	On Error Resume Next
	Set fs = fso.CreateTextFile(fullPath, ForWriting)
	
	If err = 0 Then
		fs.WriteLine "<HTML>"
		fs.WriteLine "<HEAD>"
		fs.WriteLine "<SCRIPT>"
		fs.WriteLine "<!--"
		fs.WriteLine "function RedigerMld(url) {"
		fs.WriteLine "window.open(url,'_blank', 'toolbar=1,location=0,directories=0,status=1,menubar=1,scrollbars=1,resizable=1,width=750,height=600,top=0,left=50');"
		fs.WriteLine "}"
		fs.WriteLine "// -->"
		fs.WriteLine "</SCRIPT>"
		fs.WriteLine "<TITLE>" & GetFieldValue("ST") & "</TITLE>"
		fs.WriteLine "</HEAD>"
		fs.WriteLine "<BODY bgcolor='#ffffff'>"
		fs.WriteLine "<TABLE bgcolor='#ffeedd'>"
		fs.WriteLine "<TR>"

		mystr = fso.GetBaseName(fullPath)
		i = InStrRev(mystr, "-")
		myval = GetFieldValue("GR") & Right(mystr, Len(mystr) - i) & "-" & Day(GetFieldValue("TI"))
		fs.WriteLine "<TD><B>NR: </B><!--pre-NR-->" & myval & "<!--post-NR--></TD>"

		myval = Right(Year(GetFieldValue("TI")), 2) & "-"
		If Month(GetFieldValue("TI")) < 10 Then myval = myval & "0"
		myval = myval & Month(GetFieldValue("TI")) & "-"
		If Day(GetFieldValue("TI")) < 10 Then myval = myval & "0"
		myval = myval & Day(GetFieldValue("TI"))
		fs.WriteLine "<TD><B>DA: </B><!--pre-DA-->" & myval & "<!--post-DA--></TD>"

		myval = ""
		If Hour(GetFieldValue("TI")) < 10 Then myval = myval & "0"
		myval = myval & Hour(GetFieldValue("TI")) & ":"
		If Minute(GetFieldValue("TI")) < 10 Then myval = myval & "0"
		myval = myval & Minute(GetFieldValue("TI"))
		fs.WriteLine "<TD><B>UT: </B><!--pre-UT-->" & myval & "<!--post-UT--></TD>"

		fs.WriteLine "<TD><B>SG: </B><!--pre-SG-->" & GetFieldValue("GR") & "<!--post-SG--></TD>"
		fs.WriteLine "<TD><B>SI: </B><!--pre-SI-->" & GetFieldValue("SI") & "<!--post-SI--></TD>"
		fs.WriteLine "</TR>"
		fs.WriteLine "<TR>"
		fs.WriteLine "<TD colspan='5'><B>SO: </B><!--pre-SO-->" & GetFieldValue("ST") & "<!--post-SO--></TD>"
		fs.WriteLine "</TR>"
		fs.WriteLine "<TR>"
		fs.WriteLine "<TD colspan='5'><B>GE: </B><!--pre-GE-->" & ArkivGetFieldValue("GE") & "<!--post-GE--></TD>"
		fs.WriteLine "</TR>"
		fs.WriteLine "<TR>"
		fs.WriteLine "<TD colspan='5'><B>ES: </B><!--pre-ES-->" & ArkivGetFieldValue("ES") & "<!--post-ES--></TD>"
		fs.WriteLine "</TR>"
		fs.WriteLine "<TR>"
		fs.WriteLine "<TD colspan='5'><B>EO: </B><!--pre-EO-->" & ArkivGetFieldValue("EO") & "<!--post-EO--></TD>"
		fs.WriteLine "</TR>"
		fs.WriteLine "<TR>"
		fs.WriteLine "<TD colspan='5'><B>SA: </B><!--pre-SA-->" & ArkivGetFieldValue("SA") & "<!--post-SA--></TD>"
		fs.WriteLine "</TR>"
		fs.WriteLine "<TR>"
		fs.WriteLine "<TD colspan='5'><B>RE: </B><!--pre-RE-->" & ArkivGetFieldValue("RE") & "<!--post-RE--></TD>"
		fs.WriteLine "</TR>"
		fs.WriteLine "</TABLE>"

		fs.WriteLine "<FORM onSubmit=" & chr(34) & "javascript:RedigerMld('" & urlPath & "')" & chr(34) & ">"
		fs.WriteLine "<INPUT type='submit' name='Go' value='" & editCaption & "'>"
		fs.WriteLine "<INPUT type='hidden' name='filename' value='" & urlPath & "'>"
		fs.WriteLine "<INPUT type='hidden' name='next' value='<!--pre-next-->" & nextMsg & "<!--post-next-->'>"
		fs.WriteLine "</FORM>"

		fs.WriteLine "<H3><!--pre-TI-->" & Trim(GetFieldValue("Tittel")) & "<!--post-TI--></H3>"

		myval = Trim(GetFieldValue("TilRed"))
		If myval <> "" Then myval = myval & vbCrLf
		myval = myval & Trim(GetFieldValue("Ingress"))
		myval = Replace(myval, vbCrLf & vbCrLf, "</P>" & "<P>")
		myval = Replace(myval, vbCrLf & "    ", "<BR>&nbsp;&nbsp;&nbsp;")
		myval = Replace(myval, vbCrLf & "   ", "<BR>&nbsp;&nbsp;&nbsp;")
		myval = Replace(myval, vbCrLf & "  ", "<BR>&nbsp;&nbsp;&nbsp;")
		myval = Replace(myval, vbCrLf & " ", "<BR>&nbsp;&nbsp;&nbsp;")
		myval = Replace(myval, vbCrLf, "<BR>")
		fs.WriteLine "<P><B><!--pre-IN-->" & myval & "<!--post-IN--></B></P>"

		myval = "  " & Trim(GetFieldValue("BrodTekst")) & vbCrLf & vbCrLf
		myval = myVal & Trim(GetFieldValue("Bilder"))
		myval = myVal & vbCrLf & "++" & Trim(GetFieldValue("SI"))
		myval = Replace(myval, vbCrLf & vbCrLf, "</P>" & "<P>")
		myval = Replace(myval, vbCrLf & "    ", "<BR>&nbsp;&nbsp;&nbsp;")
		myval = Replace(myval, vbCrLf & "   ", "<BR>&nbsp;&nbsp;&nbsp;")
		myval = Replace(myval, vbCrLf & "  ", "<BR>&nbsp;&nbsp;&nbsp;")
		myval = Replace(myval, vbCrLf & " ", "<BR>&nbsp;&nbsp;&nbsp;")
		myval = Replace(myval, vbCrLf, "<BR>")
		fs.WriteLine "<!--pre-TE-->" & myval & "<!--post-TE-->"

		fs.Close
		Set fs = Nothing
		WriteHTML = True
	
	Else
		WriteHTML = False
	End If
		
End Function


'---------------------------------------------------------------------
'-- Sub WriteAftenposten
'-- Skriver en melding i Aftenposten-format.
'-- Inn:	filnavn
'-- Ut:	n/a
'---------------------------------------------------------------------
Sub WriteAftenposten(fullPath)
	Dim felt, mytxt, fs

	Set fs = fso.OpenTextFile(fullPath, ForAppending, True)

	For Each felt in gArkivFelt
		Select Case felt

		Case "TI", "IN", "TE"
			mytxt = ArkivGetFieldValue(felt)
			mytxt = Replace(mytxt, "<BR>&nbsp;&nbsp;&nbsp; ", vbCrLf & "  ")
			mytxt = Replace(mytxt, "<BR>&nbsp;&nbsp;&nbsp;", vbCrLf & "  ")
			mytxt = Replace(mytxt, "<BR>", vbCrLf)
			mytxt = Replace(mytxt, "</P><P>", vbCrLf & vbCrLf)
			fs.WriteLine "*" & UCase(felt) & ":" & mytxt
		
		Case "next"	

		Case Else
			fs.WriteLine "*" & felt & ":" & ArkivGetFieldValue(felt)

		End Select
	Next
	
	fs.WriteLine vbCrLf & "*** BRS DOCUMENT BOUNDARY ***" & vbCrLf

	fs.Close
End Sub


'---------------------------------------------------------------------
'-- Sub SetArkivHeaders
'-- Flytter headerfelt til tilsvarende arkiv-felt.
'-- Inn:	n/a
'-- Ut:	n/a
'---------------------------------------------------------------------
Sub SetArkivHeaders
	Dim mytime, myno
	

End Sub


'---------------------------------------------------------------------
'-- Sub SettFeltVerdier
'-- Sjekker headerfelt og fors�ker indeksering.
'-- Inn:	n/a
'-- Ut:	n/a
'---------------------------------------------------------------------
Sub SettFeltVerdier
	Dim grp, ugrp, stord
	Dim i

	grp = UCase(GetFieldValue("GR"))
	ugrp = UCase(GetFieldValue("UG"))
	stord = LCase(GetFieldValue("ST"))

	'Nullstiller alle felt
	i = 0
	For Each fld In gArkivFelt
		gArkivVerdi(i) = ""
		i = i + 1
	Next	

	ArkivSetFieldValue "GE", "Norge;"

	If Left(stord, 5) = "hast-" Then
		ArkivSetFieldValue "EO", "Hast;"		
	End If

	Select Case grp
	Case "SPO"
		ArkivSetFieldValue "ES", "Sport"
		If ugrp = "TAB" Then
			ArkivSetFieldValue "EO", "Resultater;Tabeller"
		End If

	Case "ART"
		If ugrp = "PRS" Then
			ArkivSetFieldValue "ES", "F�dselsdag;Personalia"
		ElseIf ugrp = "KUL" Then
			ArkivSetFieldValue "ES", "Kultur"
		ElseIf ugrp = "DID" Then
			ArkivSetFieldValue "ES", "Dagen i dag"
		ElseIf ugrp = "FEA" Then
			ArkivSetFieldValue "ES", "Feature"
		End If
		ArkivSetFieldValue "EO", "Spesialtjeneste"
	End Select
	
End Sub



'---------------------------------------------------------------------
'-- Function SjekkArkivFelt
'-- Erstatter komma, punktum osv. med semikolon. Sjekker case.
'-- Inn:	opprinnnelig streng
'-- Ut:		ny streng
'---------------------------------------------------------------------
Function SjekkArkivFelt(oldstr)
	Dim newstr, parts, prt, i

	newstr = oldstr
	newstr = Replace(newstr, ",", ";")
	newstr = Replace(newstr, ":", ";")
	newstr = Replace(newstr, ".", ";")

	'Spesielle emneord
	newstr = Replace(newstr, "8; mars", "8. mars")
	newstr = Replace(newstr, "1; mai", "1. mai")
	newstr = Replace(newstr, "17; mai", "17. mai")

	parts = Split(newstr, ";")
	newstr = ""
	i = 0
	For Each prt In parts
		If prt <> "" Then
			If i > 0 Then
				newstr = newstr & ";"
			Else
				i = 1
				newstr = ""
			End If
			newstr = newstr & UCase(Left(prt, 1)) & Right(prt, Len(prt)-1)
		End If
	Next

	SjekkArkivFelt = newstr
End Function

%>

