<%  
'-------------------------------------------
'tekstarkiv.asp
'Funksjoner for tekstarkivet.
'6/12 1999 (jet)
'-------------------------------------------
%>
<!-- #include file="headers.asp" -->
<!-- #include file="functions.asp" -->
<!-- #include file="tekstarkiv-functions.asp" -->
<!-- #include file="tekstarkiv-savefile.asp" -->
<%
Dim folder, file, files, myInfo(), myFolder, fullPath, showFolder
Dim preStr, postStr, myRef, myDate
Dim sorted(), tmp
''Dim gAvdeling
''Dim gSign
''Dim gOrg
Dim kunde
Dim sendtDir, utDir
Dim gArkivFelt, gArkivVerdi()

gArkivFelt = Array("NR", "DA", "UT", "SG", "SI", "SO", "GE", "ES", "EO", "SA", "RE", "TI", "IN", "TE", "next")

Redim Preserve gArkivVerdi(UBound(gArkivFelt))

sendtDir = gBaseDir & "/sendt"
utDir = gBaseDir & "/ut"

''Set auth = Server.CreateObject("AUTHXOCX.AuthXOCXCtrl.1")
''Set fso = CreateObject("Scripting.FileSystemObject")
''gSign = GetSign()
''gAvdeling = GetAvdeling()
InitGlobals

'S�rger for at oversikten alltid oppdateres
Response.Expires = 0

If Request.ServerVariables("REQUEST_METHOD") = "POST" Then
	action = Request.Form("action")
	mydate = Request.Form("dato")
	myfile = Request.Form("filename")
	nextmsg = Request.Form("next")

Else  'if request method = get
	action = Request.QueryString("action")
	mydate = Request.QueryString("dato")
	myfile = Request.QueryString("filename")
	nextmsg = Request.QueryString("next")
End If

Select Case LCase(action)
Case "indekser"
	ArkivParseFile Server.MapPath(myfile)
	ArkivWriteForm myfile

Case "samle"
	SamleForIndeksering(mydate)

Case "samlefeature"
	SamleFeatureMeldinger

Case "tilaftenposten"
	SendTilAftenposten

Case "sendme"
	For Each fld in gArkivFelt
		ArkivSetFieldValue fld, Request.Form(fld)
	Next
	go = Request.Form("Go")
	If InStr(1, LCase(go), "slett") Then
		destDir = "stryk"
	Else
		destDir = "aftenposten"
	End If
	ArkivSendFile Server.MapPath(gBaseDir & "/tekstarkiv/" & destDir & "/" & fso.GetBaseName(myfile) & ".htm")
	'Sletter hvis ikke destinasjonen er den samme
	
	i = InStr(1, LCase(myfile), destdir)
	If i = 0 Then
		'response.write myfile & "#" & destdir
		fso.DeleteFile(Server.MapPath(AddBaseDir(myfile)))
	End If

	nextMsg = Request.Form("next")
	myFile = gBaseDir & "/tekstarkiv/indeksering/" & fso.GetBaseName(nextMsg) & ".htm"
	If fso.FileExists(Server.MapPath(myfile)) Then	
		ArkivParseFile Server.MapPath(myFile)
		ArkivWriteForm myFile
	Else
		Response.Write "<h3><font color=red>Finner ikke neste melding...</font></h3>"
		response.write "<form>"
		response.write "<input type=button onclick='window.close();' value='Lukk'>"
		response.write "</form>"
	End If
End Select

%>
</FORM>
</BODY>
</HTML>
    
