<!-- #include file="functions.asp" -->

<%
'------------------------------------------------------------
' teleavis.asp
' Skjema og utsendelse av Teleavis.
' 15/12 1999 (jet)
'------------------------------------------------------------

Const ForReading = 1, ForWriting = 2

Dim gAction
Dim newsFile
Dim mytext
Dim fso, fs

newsfile = "d:\fretex\data\teleavis\news.txt"

If Request.ServerVariables("REQUEST_METHOD") = "POST" Then
	gAction = Request.Form("action")

	If gAction = "Send" Then
		mytext = Request.Form("tekst")

		Set fso = Server.CreateObject("Scripting.FileSystemObject")		
		Set fs = fso.OpenTextFile(newsFile, ForWriting, True)
		fs.Write mytext
		fs.Close
%>
<h3><font color=red>Teleavisen er lagret</font></h3>
<form><input type=button onclick='window.close()' value="Lukk"></form>
<%
	End If

Else
	gAction = Request.QueryString("action")

	If gAction = "ny" Then
		If Hour(Now()) > 16 Then 
			tid = DateSerial(Year(Now()), Month(Now()), Day(Now())+1)
		Else
			tid = Now()
		End If

		dato = FinnUkedag(DatePart("w", tid, vbMonday, vbFirstFourDays))
		dato = dato & " " & Day(tid) & "."
		dato = dato & " " & FinnMaaned(DatePart("m", tid, vbMonday, vbFirstFourDays))
		dato = dato & " " & Year(tid)
		dato = UCase(dato)
%>
<html>
<head>
<title>Teleavisen</title>
</head>
<body bgcolor=#ffeedd>
<form action=teleavis.asp method=POST>
<h3>Teleavisen</h3>
<p>Lim inn teksten mellom topp- og bunnteksten og trykk 'Send'. Husk � sjekke datoen i overskriften!</p>
<p><input name="action" type="submit" value="Send"></p>
<textarea name="tekst" rows="25" cols="65" wrap="physical">NTB TELEAVISEN <%=dato%>



NTB Teleavisen - norske nyheter for nordmenn i utlandet.
Copyright: Norsk Telegrambyr� AS, Holbergsgt. 1, N-0166 Oslo
Tel.: (+47) 22 03 44 00 Fax.: (+47) 22 03 45 75
</textarea>
</form>
<body>
</html>
<%
	End If

End If
%>

