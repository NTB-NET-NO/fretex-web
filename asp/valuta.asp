<%  
	'-------------------------------------------
	'katalog.asp
	'Lister stikkordet p� meldinger i en katalog
	'9/8 1999 (jet)
	'-------------------------------------------
%>

<!-- #include file="headers.asp" -->
<!-- #include file="functions.asp" -->
<!-- #include file="savefile.asp" -->

<%
	Dim folder, file, files, myInfo(), myFolder, fullPath, showFolder, i, j
	Dim preStr, postStr, myRef
''	Dim gAvdeling, gSign, gOrg

	myFolder = gBaseDir & "/valuta"

	''Set fso = CreateObject("Scripting.FileSystemObject")
	''gSign = GetSign()
	InitGlobals

	'
	If Request.ServerVariables("REQUEST_METHOD") = "POST" Then
		If Request.Form("lagmelding") = "Lag melding" Then
			file = Request.Form("filename")
			fullPath = Server.MapPath(AddBaseDir(file))
			
			ParseFile(fullPath)
			myStr = GetFieldValue("BrodTekst")

			myLines = split(myStr, "<BR>")

			dat = Day(Now()) & "." & Month(Now())
			myStr = "Representative markedskurser for valuta fra Norges Bank " & dat

			cnt = 0
			For Each line In myLines
				cnt = cnt + 1
				If cnt = 1 Then
					i = InStr(1, line, "(")
					If i > 0 Then
						j = i - 2
						c = Mid(line, j, 1)
						Do While (IsNumeric(c) Or c = ".")
							j = j - 1
							c = Mid(line, j, 1)
						Loop
						oldDat = " (" & Mid(line, j+1, i-j-2) & ")"
						myStr = myStr & oldDat & ":<BR>"
					End If	
				Else				
					i = InStr(1, line, ",")
					If i > 0 Then
						j = 1
						Do While Not IsNumeric(Mid(line, j, 1))
							j = j + 1
						Loop
						myStr = myStr & Left(line, j - 1) & " ("

						i = j
						c = Mid(line, i, 1)
						Do While (IsNumeric(c) Or c = ",")
							i = i + 1
							c = Mid(line, i, 1)
						Loop
						myStr = myStr & Mid(line, j, i-j) & "), <BR>"
					End If
				End If
			Next
			SetFieldValue "BrodTekst", myStr

			SetFieldValue "ST", "valuta-" & dat
			SetFieldValue "Tittel", "Valuta " & dat & olddat

			SaveFile(Server.MapPath(gBaseDir & "/brukere/" & gSign & "/" & GetUniqueName))
			response.write "Melding med gamle kurser er lagt i din personlige k�."
		End If
	
	Else
		'S�rger for at oversikten alltid oppdateres
		Response.Expires = 0

		
		showFolder = myFolder

		If myFolder <> "" Then
			myFolder = AddBaseDir(myFolder)   
%>
			<HTML>
			<HEAD>
				<TITLE>Valuta</TITLE>
			</HEAD>
			<BODY bgcolor=#ffeedd>
				<H2>Valuta</H2>
				Sett kryss ved filen som inneholder sammenlignings-kursene og trykk 'Lag melding'.
			<FORM target="data" action="valuta.asp" method="POST">
				<INPUT type="Submit" name="lagmelding" value="Lag melding">
<%
			fullPath = Server.MapPath(myFolder)

			If Not fso.FolderExists(fullPath) Then
				Response.Write "<FONT size=4><I>Finner ikke katalogen..</I></FONT>"
			Else
				Set folder = fso.GetFolder(fullPath)
				Set files = folder.Files

				i = files.Count
				ReDim myInfo(i, 4)

				For Each file in files

					'Overser l�se-filer
					If Right(file.Name, 4) <> ".lck" Then

						myRef = file.Name
						myInfo(i, 0) = file.DateLastModified
						myInfo(i, 1) = file.Name

						Set fs = fso.OpenTextFile(file, ForReading)
						myLine = fs.ReadAll
						fs.Close
		
						preStr = "<!--pre-ST-->"
						postStr = "<!--post-ST-->"
						myStart = InStr(1, myLine, preStr)
						If myStart > 0 Then
							myEnd = InStr(1, myLine, postStr)
							If myEnd > 0 Then
								myRef = Mid(myLine, myStart + Len(preStr), myEnd - myStart - Len(PreStr))
							End If
						End If
						myInfo(i, 2) = myRef

						preStr = "<!--pre-SI-->"
						postStr = "<!--post-SI-->"
						myStart = InStr(1, myLine, preStr)
						If myStart > 0 Then
							myEnd = InStr(1, myLine, postStr)
							If myEnd > 0 Then
								myInfo(i, 3) = Mid(myLine, myStart + Len(preStr), myEnd - myStart - Len(PreStr))
							End If
						End If
								
					End If

					i = i - 1
				Next

				For i = 1 To files.count
			
					If myInfo(i, 2) <> "" Then
						dato = FormatDateTime(myInfo(i, 0), vbShortDate)
				
						If dato <> LastDate Then
							Response.Write "<H4>" & dato & "</H4>" & vbCrLf
							LastDate = dato
						End If

						Response.Write "<INPUT type='checkbox' name='filename' value='" & myFolder & "/" & myInfo(i, 1) & "'>"
						Response.Write FormatDateTime(myInfo(i, 0), vbShortTime)
						Response.Write "<A href='" & myFolder & "/" & myInfo(i, 1) & "' target='data'>" & vbCrLf
						Response.Write myInfo(i, 2) & "</A> (" & myInfo(i, 3) & ")</P>" & vbCrLf
					End If			
	
				Next

				Response.Write "</BODY>"
				Response.Write "</HTML>"

			End If 'FolderExists
        
		End If' myFolder <> ""
		
	End If 'Method = post
%>

    
