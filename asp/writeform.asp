<%
'---------------------------------------------------------------------
'-- Sub WriteForm
'-- Skriver form til brukeren. Feltverdier m� v�re satt.
'-- Inn:	n/a
'-- Ut:	n/a
'---------------------------------------------------------------------

Sub WriteForm

	Dim i, myVal, myStr, ent
	
	If gSendTil = "" Then
		gSendTil = FinnDefaultDestinasjon
	End If

%>
	<FORM name="redform" method="POST" action="rediger.asp" onsubmit="return SjekkFelt()">
	<INPUT type="hidden" name="filename" value="<%=gFileName%>" size="1">
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	<INPUT type="submit" name="Go" value="Lagre">
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	<INPUT type="submit" name="Go" value="Lagre og lukk">

<!--	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	<INPUT type="submit" name="Go" value="<%=sendCaption%>">
-->
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	<INPUT type="submit" name="Go" value="Send til:">
	<INPUT type="text" size="15" name="sendtil" value="<%=gSendTil%>">
	<A href="javascript:VisDestListe()" title="Liste over kataloger du kan sende til">[Vis liste]</A>
	<HR>
	<TABLE>
	<TR>
	<!-- SIGNATUR -->
<%
	myVal = GetFieldValue("SI")

	If myVal = "" Then
		myVal = gSign
	End If     
%>
	<TD><!-- &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-->
		&nbsp;<FONT color="navy">SI:</FONT>&nbsp;
		<INPUT type='text' name='SI' size='12' value='<%=myVal%>'>
	</TD>

	<!--GRUPPE-->
	<TD>
		<FONT color="navy">GR:</FONT>&nbsp;
		<SELECT name="GR" alt="Gruppe">
<%
	myVal = GetFieldValue("GR")
	For Each ent In gKategorier
		If ent = myVal Then
			Response.Write "<OPTION SELECTED>" & ent & vbCrLf
		Else
			Response.Write "<OPTION>" & ent & vbCrLf
		End If
	Next
%>
		</SELECT>
	</TD>

	<!--UNDERGRUPPE-->
<%
    myVal = GetFieldValue("UG") 
%>
    <TD>
        <FONT color="navy">UG:</FONT>&nbsp;
        <INPUT type="text" name="UG" size="5" value="<%=myVal%>">
    </TD>

	<!--PRIORITET-->
<%  
    myVal = GetFieldValue("PR") 
%>
	<TD>
		<FONT color="navy">PR:</FONT>&nbsp;
		<INPUT type="text" name="PR" size="3" value="<%=myVal%>">
	</TD>

	<!--KANAL-->
	<TD>
		<FONT color="navy">KA:</FONT>&nbsp;
		<SELECT name="KA">
<%
	myVal = GetFieldValue("KA") 
	For Each ent in gKanal
		If ent = myVal Then
			Response.Write "<OPTION SELECTED>" & ent
		Else
			Response.Write "<OPTION>" & ent
		End If
	Next
%>
		</SELECT>
	</TD>

	<!--DISTKODE-->
<%
	myVal = GetFieldValue("DI")
%>
	<TD>
		<FONT color="navy">DI:</FONT>&nbsp;
		<INPUT type="text" name="DI" size="5" value="<%=myVal%>" align="middle">
	</TD>
	
	<!-- Ut-tid -->
<%
	myVal = GetFieldValue("TI")
%>
	<TD>
		<A href="javascript:VisKalender()"><FONT color="navy">[Tid:]</FONT></A>&nbsp;
		<INPUT type="text" name="TI" size="16" value="<%=myval%>" align="middle">	
	</TD>
	</TR>

	<!-- STIKKORD -->
<%
	myVal = GetFieldValue("ST")
%>
	<TR>
	<TD colspan="2"><!-- &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; -->
		<FONT color="navy">ST:</FONT>&nbsp;
		<INPUT type='text' name='ST' size='25' value='<%=myVal%>'>
	</TD>

  
	<!-- TILDESK -->
<%
	myVal = GetFieldValue("TilDesk")
%>
	<TD colspan="5">
		<FONT color="navy">Til desk:</FONT>&nbsp;
		<INPUT type='text' name='TilDesk' size='40' value='<%=myVal%>'>

	<!-- TICKER -->
<%
	myVal = GetFieldValue("Ticker")
	if myVal = "ja" Then myVal = "checked"
%>
		&nbsp;&nbsp;
		<INPUT type='checkbox' name='Ticker' value='ja' <%=myVal%>>
		<FONT color="navy">Ticker</FONT>
	</TD>


	</TR>
	</TABLE>
    
    <HR>
	<TABLE>
	<!--TITTEL-->
<%
	myVal = GetFieldValue("Tittel")
%>
	<TD align="right">
		<FONT color="navy" size="4">Infolinje:</FONT>
	</TD>
	<TD>
		<INPUT type='text' name='Tittel' size='70' value='<%=myVal%>'>
	</TD>
	</TR>

	<!-- TILRED -->
	<TR>
<%
	myVal = GetFieldValue("TilRed")
%>
	<TD align="right">
		<FONT color="navy" size="4">Til red.:</FONT>
	</TD>
	<TD>
		<TEXTAREA name="TilRed" rows="3" cols="65" wrap="physical"><%=myVal%></TEXTAREA>
	</TD>
    </TR>

	<!--TITTEL-->
	<TR>
<%
	myVal = GetFieldValue("Byline")
%>
	<TD align="right">
		<A href="javascript:SettInnByline(1)" title="Trykk her for � legge inn byline"><FONT color="navy" size="4">[Byline:]</FONT></A>
	</TD>
	<TD>
		<INPUT type='text' name='Byline' size='70' value='<%=myVal%>'>
	</TD>
	</TR>


    <TR>
	<!-- INGRESS -->
<%
	myVal = GetFieldValue("Ingress")
%>
	<TD align="right">
		<A href="javascript:SettInnByline(0)" title="Trykk her for � legge inn fullt navn"><FONT color="navy" size="4">[Ingress:]</FONT></A>
	</TD>
	<TD>
		<TEXTAREA name="Ingress" rows="5" cols="65" wrap="physical"><%=myVal%></TEXTAREA>
	</TD>
    </TR>

    <TR>
	<!-- BRODTEKST -->
<%
	myVal = GetFieldValue("BrodTekst")
	myVal = myVal & GetFieldValue("Sign")
	myVal = Replace(myVal, "<BR>&nbsp;&nbsp;&nbsp; ", vbCrLf & "  ")
	myVal = Replace(myVal, "<BR>&nbsp;&nbsp;&nbsp;", vbCrLf & "  ")
	myVal = Replace(myVal, "<BR>", vbCrLf)
	myVal = Replace(myVal, "</P><P>", vbCrLf & vbCrLf)
%>
	<TD align="right">
   		<FONT color="navy">
   		<A href="javascript:TellLinjer()" title="Trykk her for � telle antall tegn i meldingen"><FONT size="4">[Br�dtekst:]</FONT></A>
		<BR><INPUT type="submit" name="Go" value="Lagre">
<!--            <BR>(Ved nytt avsnitt m�<BR>linjen begynne med<BR>ordmellomrom.) -->
		</FONT>
	<TD>
		<TEXTAREA name="BrodTekst" rows="25" cols="65" wrap="physical"><%=myVal%></TEXTAREA>
	</TD>
	</TR>
	
	    <TR>
	<!-- BILDER -->
<%
	myVal = GetFieldValue("Bilder")
	myVal = Replace(myVal, "<BR>&nbsp;&nbsp;&nbsp; ", vbCrLf & "  ")
	myVal = Replace(myVal, "<BR>&nbsp;&nbsp;&nbsp;", vbCrLf & "  ")
	myVal = Replace(myVal, "<BR>", vbCrLf)
	myVal = Replace(myVal, "</P><P>", vbCrLf & vbCrLf)
	If Left(myVal, 10) = "Bildetekst" Then
		i = InStr(1, myVal, vbCrLf)
		If i < 18 Then
			myVal = Right(myval, Len(myVal) - i + 1)
		End If
	End If
%>
	<TD align="right">
   		<FONT color="navy">
   		<FONT size="4">Bilder:</FONT>
<!--            <BR>(Ved nytt avsnitt m�<BR>linjen begynne med<BR>ordmellomrom.) -->
		</FONT>
	<TD>
		<TEXTAREA name="Bilder" rows="10" cols="65" wrap="physical"><%=myVal%></TEXTAREA>
	</TD>
	</TR>

	</TABLE>

	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	<INPUT type="submit" name="Go" value="Lagre">
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	<INPUT type="submit" name="Go" value="Lagre og lukk">

	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	<INPUT type="submit" name="Go" value="Send">
	<!--<INPUT type="text" size="12" name="sendtil" value="<%=GetMyDesk%>">
	<A href="javascript:VisDestListe()">[Vis liste]</A>-->

	</FORM>
<%
End Sub
%>





























































































